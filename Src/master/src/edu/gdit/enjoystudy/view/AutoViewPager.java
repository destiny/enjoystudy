package edu.gdit.enjoystudy.view;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.config.UniversalImgConfig;
import edu.gdit.enjoystudy.config.UniversalImgConfig.AnimateFirstDisplayListener;
import edu.gdit.enjoystudy.entity.EntityBanner;

/**
 * 自定义广告轮播View
 * 
 * @author skytoup
 *
 */
public class AutoViewPager extends RelativeLayout implements OnClickListener {

	private final static float SIZE_SELF = (9.0f / 16.0f);// 控件高宽比
	private AutoViewPager self;
	private Context mContext;
	private ImageView mImageView;// 没有图片显示时，显示的图片
	private ViewPager mViewPager;// 控制轮播图片的控件
	private LinearLayout mLinearLayout;// 指示器容器
	private TextView mTitle;// 标题
	private Timer mTimer;// 自动轮播的计时器
	private AutoViewPagerAdapter mAdapter;
	private int mCount;// 轮播条数
	private boolean mIsUserPager;// 是否用户操作
	private AutoViewPagerAdapter adapter;
	private OnPagerClick mOnPagerClick;

	public AutoViewPager(Context context) {
		super(context);
	}

	public AutoViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		self = this;
		this.mContext = context;
		this.mCount = 0;
		// 没有数据时显示的图片
		this.mImageView = new ImageView(context);
		mImageView.setScaleType(ScaleType.FIT_CENTER);
		mImageView.setImageResource(R.drawable.ic_launcher);
		this.addView(mImageView, LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		// 图片滚动的View
		this.mViewPager = new ViewPager(context);
		mViewPager.setVisibility(View.GONE);
		mViewPager.setOnPageChangeListener(new MyOnPageChangeListener(this));
		this.addView(mViewPager, LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		// 标题
		this.mTitle = new TextView(mContext);
		LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
				RelativeLayout.TRUE);
		mTitle.setLayoutParams(layoutParams);
		mTitle.setTextSize(16);
		mTitle.setTextColor(Color.WHITE);
		mTitle.setBackgroundColor(Color.argb(0x33, 0, 0, 0));
		mTitle.setPadding(15, 15, 15, 15);
		mTitle.setSingleLine();
		mTitle.setId(0x1);
		this.addView(mTitle);
		// 指示器的容器
		mLinearLayout = new LinearLayout(context);
		mLinearLayout.setGravity(Gravity.CENTER);
		mLinearLayout.setPadding(0, 0, 0, 5);
		layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		this.addView(mLinearLayout, layoutParams);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		// 改变控件大小
		int w = this.getMeasuredWidth();
		int h = (int) (w * SIZE_SELF);
		android.view.ViewGroup.LayoutParams params = this.getLayoutParams();
		params.width = w;
		params.height = h;
		this.setLayoutParams(params);
		super.onLayout(changed, l, t, r, b);
	}

	public AutoViewPager(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	private void setTitle(int index) {
		this.mTitle.setText(mAdapter.getBanner(index).getData());
	}

	/**
	 * 
	 * 设置轮播的图片(默认不自动轮播)
	 *
	 * skytoup
	 *
	 * 日期：2014年8月13日-下午1:48:32
	 *
	 * @param banner
	 *            需要轮播的图片UR
	 */
	public void setBanners(ArrayList<EntityBanner> banner) {
		this.setBanners(banner, null);
	}

	/**
	 * 
	 * 设置轮播的图片(默认不自动轮播)
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-上午9:39:22
	 *
	 * @param banners
	 *            需要轮播的图片UR
	 * @param pagerClick
	 *            pager点击监听器
	 */
	public void setBanners(ArrayList<EntityBanner> banners,
			OnPagerClick pagerClick) {
		this.setBanners(banners, false, pagerClick);
	}

	/**
	 * 
	 * 设置轮播的图片(默认不自动轮播)
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-上午9:39:22
	 *
	 * @param banners
	 *            需要轮播的图片UR
	 * @param isAutoPlay
	 *            是否自动轮播
	 */
	public void setBanners(ArrayList<EntityBanner> banners, boolean isAutoPlay) {
		this.setBanners(banners, isAutoPlay, null);
	}

	/**
	 * 
	 * 设置轮播的图片
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-上午9:39:22
	 *
	 * @param banners
	 *            需要轮播的图片UR
	 * @param pagerClick
	 *            pager点击监听器
	 * @param isAutoPlay
	 *            是否自动轮播
	 */
	public void setBanners(ArrayList<EntityBanner> banners, boolean isAutoPlay,
			OnPagerClick pagerClick) {
		if (banners == null || banners.size() == 0) {
			return;
		}
		if (this.mTimer != null) {
			mTimer.cancel();
			mTimer = null;
		}
		if (pagerClick != null) {
			this.mOnPagerClick = pagerClick;
		}
		if (adapter == null) {
			this.mAdapter = new AutoViewPagerAdapter(mContext, banners, this);
		} else {
			adapter.setBanner(banners);
		}
		mViewPager.setAdapter(mAdapter);

		this.mCount = banners.size();
		mLinearLayout.removeAllViews();
		mViewPager.setVisibility(View.VISIBLE);
		mImageView.setVisibility(View.GONE);
		if (mCount == 1) {
			return;
		}

		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		for (int i = 0; i < mCount; i++) {
			ImageView imageView = new ImageView(mContext);
			imageView.setImageResource(R.drawable.ad_point);
			imageView.setPadding(5, 0, 5, 0);
			mLinearLayout.addView(imageView, i, layoutParams);
		}
		this.setCurrentSelect(0);

		if (isAutoPlay) {
			this.startAutoPlay();
		}
		this.setTitle(0);
	}

	/**
	 * 
	 * 设置播放的图片
	 *
	 * skytoup
	 *
	 * 日期：2014年8月13日-下午1:49:59
	 *
	 * @param index
	 *            需要播放的位置
	 */
	public void setCurrentSelect(int index) {
		if (index < 0 || index >= mCount || mCount == 1) {
			return;
		}
		this.mViewPager.setCurrentItem(index);
		for (int i = 0; i < mCount; i++) {
			this.mLinearLayout.getChildAt(i).setSelected(false);
		}
		mLinearLayout.getChildAt(index).setSelected(true);
		this.setTitle(index);
	}

	/**
	 * 
	 * 设置是否用户操作
	 *
	 * skytoup
	 *
	 * 日期：2014年8月13日-下午1:50:27
	 *
	 * @param is
	 *            是否
	 */
	public void setIsUserPager(boolean is) {
		this.mIsUserPager = is;
	}

	public boolean isUserPager() {
		return mIsUserPager;
	}

	/**
	 * 
	 * 获取条数
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-上午10:57:16
	 *
	 * @return
	 */
	public int getItemCount() {
		return this.mCount;
	}

	/**
	 * 
	 * 设置监听pager点击
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-上午10:57:28
	 *
	 * @param onPagerClick
	 */
	public void setOnPagerClickListener(OnPagerClick onPagerClick) {
		this.mOnPagerClick = onPagerClick;
	}

	@Override
	public void onClick(View v) {
		if (this.mOnPagerClick != null) {
			int i = this.mViewPager.getCurrentItem();
			mOnPagerClick.onPagerClick(i, mAdapter.getBanner(i));
		}
	}

	/**
	 * 
	 * 获取正在显示的item
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-上午10:57:49
	 *
	 * @return
	 */
	public int getCurrentItem() {
		return this.mViewPager.getCurrentItem();
	}

	/**
	 * 
	 * 开始轮播
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-上午11:02:07
	 *
	 */
	public void startAutoPlay() {
		if (this.mTimer != null) {
			mTimer.cancel();
		}
		if (this.mCount == 0 || mCount == 1) {
			return;
		}
		mTimer = new Timer();
		mTimer.schedule(new AutoPlayTimerTask(self), 5000, 2500);
	}

	/**
	 * 
	 * 暂停自动轮播
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-上午10:58:08
	 *
	 */
	public void pauseAutoPlay() {
		if (mTimer != null) {
			mTimer.cancel();
		}
	}

	/**
	 * 自动轮播条被点击时回调的接口
	 * 
	 * @author skytoup
	 *
	 */
	public interface OnPagerClick {
		public void onPagerClick(int index, EntityBanner banner);
	}
}

/**
 * 自动轮播任务
 * 
 * @author skytoup
 *
 */
class AutoPlayTimerTask extends TimerTask {

	private static final int HANDLE_AUTO_PLAY_AD = 0x1001;// Handle Msg处理标识
	private AutoViewPager mAutoViewPager;
	private int mCount;
	private Handler mHandler;

	public AutoPlayTimerTask(AutoViewPager autoViewPager) {
		this.mAutoViewPager = autoViewPager;
		this.mCount = autoViewPager.getItemCount();
		mHandler = new Handler(Looper.getMainLooper()) {

			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				if (msg.what == HANDLE_AUTO_PLAY_AD) {
					mAutoViewPager.setCurrentSelect((mAutoViewPager
							.getCurrentItem() + 1) % mCount);
				}
			}
		};
	}

	@Override
	public void run() {
		if (mAutoViewPager.isUserPager()) {
			mAutoViewPager.setIsUserPager(false);
			return;
		}
		mHandler.sendEmptyMessage(HANDLE_AUTO_PLAY_AD);
	}
}

/**
 * 自定义的VIewPager监听器
 * 
 * @author skytoup
 *
 */
class MyOnPageChangeListener implements OnPageChangeListener {

	private AutoViewPager mAutoViewPager;

	public MyOnPageChangeListener(AutoViewPager autoViewPager) {
		this.mAutoViewPager = autoViewPager;
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		mAutoViewPager.setIsUserPager(true);
	}

	@Override
	public void onPageSelected(int arg0) {
		this.mAutoViewPager.setCurrentSelect(arg0);
	}

}

/**
 * 自动轮播器的Adapter
 * 
 * @author skytoup
 *
 */
class AutoViewPagerAdapter extends PagerAdapter {

	private ArrayList<EntityBanner> mBanners;
	private ArrayList<View> mImageViews;

	public AutoViewPagerAdapter(Context context,
			ArrayList<EntityBanner> banners, OnClickListener clickListener) {
		this.mBanners = new ArrayList<EntityBanner>(banners);
		this.mImageViews = new ArrayList<View>();
		ImageLoader imageLoader = ImageLoader.getInstance();
		AnimateFirstDisplayListener listener = new AnimateFirstDisplayListener();
		for (EntityBanner banner : mBanners) {
			ImageView imageView = new ImageView(context);
			imageView.setScaleType(ScaleType.FIT_CENTER);
			mImageViews.add(imageView);
			imageLoader.displayImage(banner.getPath(), imageView,
					UniversalImgConfig.displayImageOptions, listener);
			imageView.setOnClickListener(clickListener);
		}
	}

	public void setBanner(ArrayList<EntityBanner> banners) {
		this.mBanners = banners;
		this.notifyDataSetChanged();
	}

	public EntityBanner getBanner(int index) {
		return mBanners.get(index);
	}

	@Override
	public int getCount() {
		return mBanners == null ? 0 : mBanners.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView(mImageViews.get(position));
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		container.addView(mImageViews.get(position));
		return mImageViews.get(position);
	}

}
