package edu.gdit.enjoystudy.view;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ChooseWeekView extends LinearLayout implements OnClickListener {

	private static final int CHOOSE_COLOR = Color.GREEN;// 选中的颜色
	private static final int CHOOSE_NO_COLOR = Color.argb(0, 0, 0, 0);// 没选中的颜色
	private static final String[] WEEKS = new String[] { "日", "一", "二", "三",
			"四", "五", "六" };
	private boolean[] chooseItems;// 记录哪一个星期选中
	private TextView[] textViews;
	private int[] viewIds;
	private boolean selectable = true;// 是否可选择

	public ChooseWeekView(Context context) {
		super(context);
	}

	public ChooseWeekView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.setOrientation(LinearLayout.HORIZONTAL);
		this.setGravity(Gravity.CENTER);
		this.setPadding(15, 15, 0, 0);
		chooseItems = new boolean[] { false, false, false, false, false, false,
				false };
		textViews = new TextView[7];
		viewIds = new int[7];
		for (int i = 0; i != WEEKS.length; i++) {
			viewIds[i] = 19940 + i;
			textViews[i] = new TextView(context);
			textViews[i].setText(WEEKS[i]);
			textViews[i].setTextSize(16);
			textViews[i].setTextColor(Color.argb(0xFF, 0x41, 0x33, 0x90));
			textViews[i].setId(viewIds[i]);
			textViews[i].setOnClickListener(this);
			textViews[i].setPadding(15, 0, 15, 0);
			addView(textViews[i]);
		}
	}

	@Override
	public void onClick(View v) {
		if (!selectable) {
			return;
		}
		int id = v.getId();
		for (int i = 0; i != viewIds.length; i++) {
			if (viewIds[i] == id) {
				chooseItems[i] = !chooseItems[i];
				textViews[i].setBackgroundColor(chooseItems[i] ? CHOOSE_COLOR
						: CHOOSE_NO_COLOR);
				break;
			}
		}
	}

	public boolean[] getChooseItems() {
		return chooseItems.clone();
	}

	public void setChooseItems(boolean[] chooseItems) {
		this.chooseItems = chooseItems.clone();
		for (int i = 0; i != textViews.length; i++) {
			textViews[i].setBackgroundColor(chooseItems[i] ? CHOOSE_COLOR
					: CHOOSE_NO_COLOR);
		}
	}

	public boolean isSelectable() {
		return selectable;
	}

	public void setSelectable(boolean selectable) {
		this.selectable = selectable;
	}

}
