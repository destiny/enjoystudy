/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-8   Create
 * 
 **/
package edu.gdit.enjoystudy.view;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import edu.gdit.enjoystudy.app.App;

/**
 * Description:
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class CustomDrawerLayout extends DrawerLayout {

	private boolean isIntercept = true;

	public CustomDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public CustomDrawerLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomDrawerLayout(Context context) {
		super(context);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		// always false and event will be send to each view
		if (ev.getAction() == MotionEvent.ACTION_DOWN) {
			if (ev.getX() > 50 && !this.isDrawerOpen(Gravity.LEFT)) {
				isIntercept = false;
			} else if (isDrawerOpen(Gravity.LEFT)
					&& ev.getX() < App.getSceenWidth() - 150) {
				isIntercept = false;
			} else {
				isIntercept = true;
			}
			System.out.println("" + isIntercept);
		}
		return isIntercept;
	}
}