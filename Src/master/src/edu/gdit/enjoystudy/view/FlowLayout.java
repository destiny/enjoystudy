package edu.gdit.enjoystudy.view;

import java.util.ArrayList;

import edu.gdit.enjoystudy.entity.EntityHotSearch;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * ������
 * 
 * @author skytoup·��
 * 
 *         2014-10-30����9:01:35
 * 
 */
public class FlowLayout extends LinearLayout implements OnClickListener {

	private boolean isLayout = false;
	private ArrayList<EntityHotSearch> datas;
	private final static int[] COLORS = { 0xFF1765b0, 0xFFeb6b1b, 0xFFdc3115,
			0xFF232222, 0xFFfbd542, 0xFFb8d44b, 0xFF058590, 0xFF1f65ae };
	private int cp = 0;
	private OnItemClickListener onClickListener;

	public FlowLayout(Context context) {
		super(context);
	}

	public FlowLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOrientation(LinearLayout.VERTICAL);

	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		isLayout = true;
		if (datas != null) {
			addData(datas);
		}
	}

	public void setOnItemClickListener(OnItemClickListener listener) {
		onClickListener = listener;
	}

	public void addData(ArrayList<EntityHotSearch> datas) {
		if (!isLayout) {
			this.datas = datas;
			return;
		}

		int lw = getMeasuredWidth();
		int size = datas.size();
		int tw = lw;
		LinearLayout layout = createLinearLayout(getContext());
		for (int i = 0; i != size; ++i) {
			TextView tv = createTextView(getContext(), datas.get(i).getText());
			int w = tv.getMeasuredWidth() + 80;
			tv.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1));
			tv.setTag("" + i);
			tv.setOnClickListener(this);
			if (tw >= w && layout.getChildCount() != 4) {// ������
				tw -= w;
			} else {// �½�һ��
				addView(layout);
				layout = createLinearLayout(getContext());
				tw = lw;
			}
			layout.addView(tv);
		}
		addView(layout);
		datas = null;
	}

	private LinearLayout createLinearLayout(Context context) {
		LinearLayout layout = new LinearLayout(getContext());
		layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));
		layout.setPadding(0, 10, 0, 10);
		layout.setOrientation(LinearLayout.HORIZONTAL);
		return layout;
	}

	private TextView createTextView(Context context, String string) {
		TextView tv = new TextView(context);
		tv.setText(string);
		tv.setTextSize(24);
		tv.setSingleLine();
		tv.setTextColor(COLORS[cp++ % COLORS.length]);
		tv.setGravity(Gravity.CENTER);
		tv.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		tv.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		return tv;
	}

	@Override
	public void onClick(View v) {
		if (onClickListener != null) {
			onClickListener.onitemclick(Integer.parseInt((String) v.getTag()),
					(TextView) v);
		}
	}

	public interface OnItemClickListener {
		public void onitemclick(int position, TextView textView);
	}

}
