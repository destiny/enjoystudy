package edu.gdit.enjoystudy.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	/**
	 * 
	 * 获取现在的时分 --- HH:mm
	 * 
	 * skytoup
	 *
	 * 日期：2014年9月23日-下午10:26:54
	 *
	 * @return
	 */
	public static String getTimeNow() {
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		return format.format(new Date());
	}

	/**
	 * 
	 * 获取现在的时
	 * 
	 * skytoup
	 *
	 * 日期：2014年9月23日-下午10:31:10
	 *
	 * @return
	 */
	public static String getHourNow() {
		SimpleDateFormat format = new SimpleDateFormat("HH");
		return format.format(new Date());
	}

	/**
	 * 
	 * 获取现在的分
	 * 
	 * skytoup
	 *
	 * 日期：2014年9月23日-下午10:30:50
	 *
	 * @return
	 */
	public static String getMinuteNow() {
		SimpleDateFormat format = new SimpleDateFormat("mm");
		return format.format(new Date());
	}

	/**
	 * 
	 * 获取今天的日期 yyyy/MM/dd
	 * 
	 * skytoup
	 *
	 * 日期：2014年9月26日-上午7:04:57
	 *
	 * @return
	 */
	public static String getDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		return format.format(new Date());
	}

	/**
	 * 计算得到前几天的日期
	 * 
	 * @param specifiedDay
	 * @return
	 * @throws Exception
	 */
	public static String getSpecifiedDayBefore(String specifiedDay, int days) {
		// SimpleDateFormat simpleDateFormat = new
		// SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy/MM/dd").parse(specifiedDay);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		c.setTime(date);
		int day = c.get(Calendar.DATE);
		c.set(Calendar.DATE, day - days);

		String dayBefore = new SimpleDateFormat("yyyy/MM/dd").format(c
				.getTime());
		return dayBefore;
	}
	
	
	/**
	 * 根据time转换到>>id(length:10)
	 * 
	 * @return
	 */
	public static int getimeid() {// 时间转成借书id 9位数
		Date date = new Date();
		String a = "" + date.getHours() + date.getMinutes() + date.getSeconds()
				+ date.getMonth() + date.getDay() + date.getYear() % 100;
		double b = Double.valueOf(a);
		double keytime = 0;
		if (a.length() == 7) {
			keytime = 1300 * b;
		} else if (a.length() == 8) {
			keytime = 130 * b;
		} else if (a.length() == 9) {
			keytime = 13 * b;
		} else if (a.length() == 10) {
			keytime = b;
		} else if (a.length() == 11) {
			keytime = 0.13 * b;
		} else if (a.length() == 12) {
			keytime = 0.013 * b;
		} else {
			keytime = 0.0013 * b;
		}

		String str = ((keytime + "").replace(".", "").replace("E", "")
				.substring(0, 9));
		return Integer.parseInt(str);
	}
	
	
	/**
	 * date转到String
	 * 
	 * @author ping
	 * @create 2014-6-13 上午11:26:06
	 * @param dateDate
	 * @return
	 */
	public static String dateToStr(java.util.Date dateDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(dateDate);
		return dateString;
	}

	/**
	 * date转成String
	 * 
	 * @author lwj
	 * @create 2014-6-18 9:54:10
	 * @param dateDate
	 *            日期
	 * @param format
	 *            格式
	 * @return 规定格式的日期
	 */

	public static String dateToStr(java.util.Date dateDate, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		String dateString = formatter.format(dateDate);
		return dateString;
	}

	/**
	 * 把一个long类型的时间格式化为format格式,如yyyy年MM月dd日 HH时mm分ss秒
	 * 
	 * @param milliseconds
	 *            long类型的时间
	 * @param format
	 *            要转换的格式
	 * @return
	 */
	public static String dateFormat(long milliseconds, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		Date date = new Date(milliseconds);
		String time = formatter.format(date);
		return time;
	}
}
