package edu.gdit.enjoystudy.utils;

import java.util.ArrayList;

import edu.gdit.enjoystudy.entity.EntityCourse;
import edu.gdit.enjoystudy.ui.home.FragmentHome;
import edu.gdit.enjoystudy.ui.home.WebActivity;
import edu.gdit.enjoystudy.vitamio.VP;
import edu.gdit.enjoystudy.vitamio.VideoActivity;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;

public class AppUtil {
	/**
	 * 获取软件自身版本号
	 * 
	 * @return 当前应用的版本号
	 */
	public static int getselfVersionCode(Context context) {
		String packname = context.getPackageName();
		return getappVersionCode(context, packname);
	}

	/**
	 * 获取软件自身版本名
	 * 
	 * @return 当前应用的版本号
	 */
	public static String getselfVersionName(Context context) {
		String packname = context.getPackageName();
		return getappVersionName(context, packname);
	}

	public static int getappVersionCode(Context context, String packageName) {
		try {
			return context.getPackageManager().getPackageInfo(packageName, 0).versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static String getappVersionName(Context context, String packageName) {
		try {
			return context.getPackageManager().getPackageInfo(packageName, 0).versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 获取APP崩溃异常报告
	 * 
	 * @param ex
	 * @return
	 */
	public static String getCrashReport(Context context, Throwable ex) {
		// PackageInfo pinfo = getPackageInfo(context);
		StringBuffer exceptionStr = new StringBuffer();
		exceptionStr.append("Exception:" + ex.getMessage() + "\n");
		StackTraceElement[] elements = ex.getStackTrace();
		for (int i = 0; i < elements.length; i++) {
			exceptionStr.append(elements[i].toString() + "\n");
		}
		return exceptionStr.toString();
	}

	/**
	 * 获取App安装包信息
	 * 
	 * @return
	 */
	public static PackageInfo getPackageInfo(Context context) {
		PackageInfo info = null;
		try {
			info = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
		}
		if (info == null)
			info = new PackageInfo();
		return info;
	}

	/**
	 * 提出APP自己结束进程 ）
	 * 
	 * @return
	 */
	public static void exit() {
		killProcess(android.os.Process.myPid());
	}

	/**
	 * 根据进程id结束进程
	 * 
	 * @param Processid
	 */
	public static void killProcess(int Processid) {
		android.os.Process.killProcess(Processid);
	}

	/**
	 * 获取Meta值
	 * 
	 * @param context
	 * @param metaKey
	 * @return
	 */
	public static String getMetaValue(Context context, String metaKey) {
		Bundle metaData = null;
		String apiKey = null;
		if (context == null || metaKey == null) {
			return null;
		}
		try {
			ApplicationInfo ai = context.getPackageManager()
					.getApplicationInfo(context.getPackageName(),
							PackageManager.GET_META_DATA);
			if (null != ai) {
				metaData = ai.metaData;
			}
			if (null != metaData) {
				apiKey = metaData.getString(metaKey);
			}
		} catch (NameNotFoundException e) {

		}
		return apiKey;
	}

	/**
	 * 判断service是否运行
	 * 
	 * @param context
	 * @param serviceIntentFilter
	 * @return
	 */
	public static boolean serviceIsWorked(Context context,
			String serviceIntentFilter) {
		ActivityManager myManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		ArrayList<RunningServiceInfo> runningService = (ArrayList<RunningServiceInfo>) myManager
				.getRunningServices(30);
		for (int i = 0; i < runningService.size(); i++) {
			if (runningService.get(i).service.getClassName().toString()
					.equals(serviceIntentFilter)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断是否登陆
	 * 
	 * @param context
	 * @return
	 */
	public static boolean getIsLogin(Context context) {
		return !SPUtils.getPrefString(context, SPkey.TOKEN, "").equals("");
	}

	/**
	 * 打开课程
	 * 
	 * @param context
	 * @param course
	 */
	public static void openCourse(Context context, EntityCourse course) {
		Intent intent = new Intent(context, VideoActivity.class);
		intent.setData(Uri.parse("http://cococ.aliapp.com/1234.mp4"));
		intent.putExtra(VP.displayName, "网络视频");
		intent.putExtra(FragmentHome.INTENT_VIDEO, course);
		context.startActivity(intent);
	}
	
	public static void openCourse(Context context, int cid) {
		EntityCourse c= new EntityCourse();
		c.setId(cid);
		Intent intent = new Intent(context, VideoActivity.class);
		intent.setData(Uri.parse("http://cococ.aliapp.com/1234.mp4"));
		intent.putExtra(VP.displayName, "网络视频");
		intent.putExtra(FragmentHome.INTENT_VIDEO,c);
		context.startActivity(intent);
	}
	
	public static void openWebView(Context context, String url) {
	 
		Intent intent = new Intent(context, WebActivity.class);
	 
		intent.putExtra("url", url);
 
		context.startActivity(intent);
	}
	
	

}
