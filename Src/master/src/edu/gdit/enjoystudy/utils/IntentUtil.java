package edu.gdit.enjoystudy.utils;

import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;

/**
 * Description: 启动Activity的工具类
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class IntentUtil {

	public static void startActivity(Activity activity, Class<?> cls) {
		startActivity(activity, cls, null);
	}

	/**
	 * 描述：开启另一个activity ，如需设置切换动画，请调用该函数后设置  activity.overridePendingTransition
	 * 
	 * @param activity 当前Activity
	 * @param cls 目标Activity
	 * @param param 传递参数，（键值对）
	 * @return void
	 */
	public static void startActivity(Activity activity, Class<?> cls,
			BasicNameValuePair... param) {
		L.v("startActiviy:" + cls.getName());
		Intent intent = new Intent();
		intent.setClass(activity, cls);
		if (param != null) {
			for (int i = 0; i < param.length; i++) {
				intent.putExtra(param[i].getName(), param[i].getValue());
			}
		}
		activity.startActivity(intent);
		// activity.overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
	}

}
