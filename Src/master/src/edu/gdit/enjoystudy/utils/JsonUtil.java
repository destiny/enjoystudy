package edu.gdit.enjoystudy.utils;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

/**
 * Json工具类
 * 
 * @author skytoup
 *
 */
public class JsonUtil {

	/**
	 * 
	 * 把一个JsonArray转换成ArrayList
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-下午10:11:55
	 *
	 * @param jsonArray
	 *            需要转换的数据
	 * @param entityClass
	 *            转换成的实体类
	 * @return 实体类集合
	 */
	public static <Entity> ArrayList<Entity> jsonArrayT1oArray(
			JSONArray jsonArray, Class<Entity> entityClass) {
		int size = jsonArray.size();
		ArrayList<Entity> list = new ArrayList<Entity>();
		for (int i = 0; i < size; i++) {
			String obj = jsonArray.get(i).toString();
			Entity entity = JSON.parseObject(obj, entityClass);
			list.add(entity);
		}
		return list;
	}
}
