package edu.gdit.enjoystudy.utils;

import android.util.SparseArray;
import android.view.View;

/**
 * Holder优化工具类
 * 
 * @author skytoup
 *
 */
public class HolderUtil {
	@SuppressWarnings({ "unchecked", "hiding" })
	public static final <T extends View> T getView(View view, int id) {
		SparseArray<View> holder = (SparseArray<View>) view.getTag();
		View childeView = null;
		if (holder == null) {
			holder = new SparseArray<View>();
			view.setTag(holder);
		}

		childeView = holder.get(id);
		if (childeView == null) {
			childeView = view.findViewById(id);
			holder.put(id, childeView);
		}

		return (T) childeView;
	}
}
