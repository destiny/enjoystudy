package edu.gdit.enjoystudy.utils;

import java.io.File;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.text.format.Formatter;

@SuppressWarnings("deprecation")
/**
 * 获取容量的工具类
 * @author skytoup
 *
 */
public class MomentSizeUtil {

	/**
	 * 
	 * 获取内存卡容量
	 * 
	 * skytoup
	 *
	 * 日期：2014年9月2日-下午4:54:36
	 *
	 * @return
	 */
	private static long getSDTotal() {
		File path = Environment.getExternalStorageDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long totalBlocks = stat.getBlockCount();
		return blockSize * totalBlocks;
	}

	/**
	 * 获得SD卡总大小
	 * 
	 * @return
	 */
	private static String getSDTotalSize(Context context) {
		return Formatter.formatFileSize(context, getSDTotal());
	}

	/**
	 * 
	 * 获取内存卡剩余容量
	 * 
	 * skytoup
	 *
	 * 日期：2014年9月2日-下午4:55:20
	 *
	 * @return
	 */
	private static long getSDAvailable() {
		File path = Environment.getExternalStorageDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		return blockSize * availableBlocks;
	}

	/**
	 * 获得sd卡剩余容量，即可用大小
	 * 
	 * @return
	 */
	private static String getSDAvailableSize(Context context) {
		return Formatter.formatFileSize(context, getSDAvailable());
	}

	/**
	 * 
	 * 获取机身内存
	 * 
	 * skytoup
	 *
	 * 日期：2014年9月2日-下午4:56:31
	 *
	 * @return
	 */
	private static long getRomTotal() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long totalBlocks = stat.getBlockCount();
		return blockSize * totalBlocks;
	}

	/**
	 * 获得机身内存总大小
	 * 
	 * @return
	 */
	private static String getRomTotalSize(Context context) {
		return Formatter.formatFileSize(context, getRomTotal());
	}

	/**
	 * 
	 * 获取机身可用容量
	 * 
	 * skytoup
	 *
	 * 日期：2014年9月2日-下午4:57:22
	 *
	 * @return
	 */
	private static long getRomAvailable() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		return blockSize * availableBlocks;
	}

	/**
	 * 获得机身可用内存
	 * 
	 * @return
	 */
	private static String getRomAvailableSize(Context context) {
		return Formatter.formatFileSize(context, getRomAvailable());
	}

	/**
	 * 
	 * 获取总内存
	 * 
	 * skytoup
	 *
	 * 日期：2014年9月2日-下午4:27:59
	 *
	 * @param context
	 * @return
	 */
	public static String getMomentSize(Context context) {
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {// 有内存卡
			return getSDTotalSize(context);
		} else {
			return getRomTotalSize(context);
		}
	}

	/**
	 * 
	 * 获取可用内存
	 * 
	 * skytoup
	 *
	 * 日期：2014年9月2日-下午4:28:13
	 *
	 * @param context
	 * @return
	 */
	public static String getEnableMomentSize(Context context) {
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {// 有内存卡
			return getSDAvailableSize(context);
		} else {
			return getRomAvailableSize(context);
		}
	}

	/**
	 * 
	 * 获取内存可用和总容量百分比
	 * 
	 * skytoup
	 *
	 * 日期：2014年9月2日-下午4:59:12
	 *
	 * @return
	 */
	public static int getMonentProgress() {
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {// 有内存卡
			return (int) (getSDAvailable() * 100 / getSDTotal());
		} else {
			return (int) (getRomAvailable() * 100 / getRomTotal());
		}
	}

}
