package edu.gdit.enjoystudy.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.app.App;

public class DialogUtil {

	/**
	 * 显示对话框
	 * 
	 * @param dialog
	 */
	public static void show(Dialog dialog) {
		if (dialog != null && !dialog.isShowing()) {
			dialog.show();
		}
	}

	/**
	 * 关闭对话框
	 * 
	 * @param dialog
	 */
	public static void disShow(Dialog dialog) {
		if (dialog != null && dialog.isShowing()) {
			dialog.dismiss();
		}
	}

	/**
	 * 
	 * 显示一个单项选择对话框
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月19日-上午11:10:28
	 * 
	 * @param context
	 * @param title
	 *            标题
	 * @param items
	 *            选项
	 * @param chooseItem
	 *            选中项
	 * @param listener
	 *            选择监听器
	 */
	public static void showSingleItemDialog(Context context, String title,
			String[] items, int chooseItem,
			DialogInterface.OnClickListener listener) {
		new AlertDialog.Builder(context).setTitle(title)
				.setSingleChoiceItems(items, chooseItem, listener).create()
				.show();
	}

	/**
	 * 
	 * 创建一个等待对话框
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月19日-下午4:05:59
	 * 
	 * @param context
	 * @param message
	 *            提示内容
	 * @return
	 */
	public static ProgressDialog createProgressDialog(Context context,
			String message) {
		ProgressDialog dialog = new ProgressDialog(context);
		dialog.setMessage(message);
		return dialog;
	}

	/**
	 * 
	 * 创建一个无边框对话框
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月23日-下午9:21:25
	 * 
	 * @param context
	 * @return
	 */
	public static Dialog createDialog(Context context) {
		return new Dialog(context, R.style.DialogStyle);
	}

	/**
	 * 
	 * 创建设置的更新对话框
	 * 
	 * skytoup
	 * 
	 * 日期：2014年9月16日-上午10:52:11
	 * 
	 * @param context
	 * @param resLogoId
	 *            对话框的LogoId
	 * @param msg
	 *            对话框的文字
	 * @param btnTitle
	 *            按钮的标题
	 * @param listener
	 *            按钮点击的监听器
	 * @return
	 */
	public static Dialog createSettingDialog(Context context, int resLogoId,
			String msg, String btnTitle, OnClickListener listener) {
		Dialog dialog = createDialog(context);
		View view = LayoutInflater.from(context).inflate(
				R.layout.setting_dialog, null);
		((ImageView) view.findViewById(R.id.imgDialogLogo))
				.setImageResource(resLogoId);
		((TextView) view.findViewById(R.id.tvDialogMsg)).setText(msg);
		((TextView) view.findViewById(R.id.tvDialogBtnTitle)).setText(btnTitle);
		view.findViewById(R.id.viewDialogBtn).setOnClickListener(listener);
		dialog.setContentView(view);
		dialog.getWindow().setLayout(App.getSceenWidth() / 4 * 3,
				App.getSceenHeight() / 2);
		return dialog;
	}

	/**
	 * 
	 * 创建选项对话框
	 * 
	 * skytoup
	 * 
	 * 日期：2014年9月16日-上午10:53:36
	 * 
	 * @param context
	 * @param resDivider
	 *            分割线资源ID
	 * @param resBtnBg
	 *            按钮背景资源ID
	 * @param resTextView
	 *            文本资源ID
	 * @param items
	 *            选项
	 * @param listener
	 *            选项点击监听
	 * @return
	 */
	public static Dialog createItemDialog(Context context, int resDivider,
			int resBtnBg, int resTextView, String[] items,
			OnItemClickListener listener) {
		final Dialog dialog = createDialog(context);
		View view = LayoutInflater.from(context).inflate(R.layout.dialog_items,
				null);
		view.findViewById(R.id.btnCancle).setBackgroundResource(resBtnBg);
		view.findViewById(R.id.btnCancle).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
		ListView listView = (ListView) view.findViewById(R.id.listView);
		listView.setDivider(context.getResources().getDrawable(resDivider));
		listView.setAdapter(new ArrayAdapter<String>(context, resTextView,
				R.id.textView, items));
		listView.setOnItemClickListener(listener);
		dialog.setContentView(view);
		dialog.getWindow().setLayout(App.getSceenWidth() / 4 * 3,
				LayoutParams.WRAP_CONTENT);
		return dialog;
	}

	/**
	 * 
	 * 创建一个日期选择对话框
	 * 
	 * skytoup
	 * 
	 * 日期：2014年9月23日-下午10:14:28
	 * 
	 * @param context
	 * @param hour
	 *            初始小时
	 * @param minute
	 *            初始分钟
	 * @return
	 */
	public static Dialog createDateSelectDialog(Context context, int hour,
			int minute, final OnDateSelectListener onDateSelectListener) {
		final Dialog dialog = createDialog(context);
		final View view = LayoutInflater.from(context).inflate(
				R.layout.dialog_date_select, null);
		final TextView tvHour = (TextView) view.findViewById(R.id.tvHour);
		final TextView tvMinute = (TextView) view.findViewById(R.id.tvMinute);
		// 初始化时间
		tvHour.setText(StringUtils.intToDateString(hour));
		tvMinute.setText(StringUtils.intToDateString(minute));

		OnClickListener listener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				int id = v.getId();
				int hour = -1, minute = -1;
				switch (id) {
				case R.id.imgAddLeft:
					hour = Integer.parseInt(tvHour.getText().toString());
					hour = hour + 1 < 24 ? hour + 1 : 0;
					break;

				case R.id.imgReduceLeft:
					hour = Integer.parseInt(tvHour.getText().toString());
					hour = hour - 1 >= 0 ? hour - 1 : 23;
					break;

				case R.id.imgAddRight:
					minute = Integer.parseInt(tvMinute.getText().toString());
					minute = minute + 1 < 60 ? minute + 1 : 0;
					break;

				case R.id.imgReduceRight:
					minute = Integer.parseInt(tvMinute.getText().toString());
					minute = minute - 1 >= 0 ? minute - 1 : 0;
					break;

				case R.id.btnNo:// 取消
					dialog.dismiss();
					return;

				case R.id.btnYes:// 确认
					hour = Integer.parseInt(tvHour.getText().toString());
					minute = Integer.parseInt(tvMinute.getText().toString());
					onDateSelectListener.onSelectDate(hour, minute);
					dialog.dismiss();
					return;
				}

				// 设置时间
				if (hour != -1) {
					tvHour.setText(StringUtils.intToDateString(hour));
				} else if (minute != -1) {
					tvMinute.setText(StringUtils.intToDateString(minute));
				}
			}
		};

		view.findViewById(R.id.imgAddLeft).setOnClickListener(listener);
		view.findViewById(R.id.imgReduceLeft).setOnClickListener(listener);
		view.findViewById(R.id.imgAddRight).setOnClickListener(listener);
		view.findViewById(R.id.imgReduceRight).setOnClickListener(listener);
		view.findViewById(R.id.btnNo).setOnClickListener(listener);
		view.findViewById(R.id.btnYes).setOnClickListener(listener);

		dialog.setContentView(view);
		dialog.getWindow().setLayout(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		return dialog;
	}

	/**
	 * 日期选择对话框选择时间监听
	 * 
	 * @author skytoup
	 * 
	 */
	public interface OnDateSelectListener {
		/**
		 * 
		 * 选择时间后的回调
		 * 
		 * skytoup
		 * 
		 * 日期：2014年9月23日-下午10:17:21
		 * 
		 * @param hour
		 *            选择的小时
		 * @param minute
		 *            选择的分钟
		 */
		public void onSelectDate(int hour, int minute);
	}

}
