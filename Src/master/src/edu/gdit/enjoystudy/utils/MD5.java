package edu.gdit.enjoystudy.utils;

import java.security.MessageDigest;

public class MD5 {

	public static String getMD5(String content) {
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(content.getBytes());
			return getHashString(digest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// MD5（userName+MD5（pwd））
	public static String getPWDMD5(String userName, String pwd) {
		return getMD5(userName + getMD5(pwd));
	}

	private static String getHashString(MessageDigest digest) {
		StringBuilder builder = new StringBuilder();
		for (byte b : digest.digest()) {
			builder.append(Integer.toHexString((b >> 4) & 0xf));
			builder.append(Integer.toHexString(b & 0xf));
		}
		return builder.toString();
	}
}