/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-7-30   Create
 * 
 **/
package edu.gdit.enjoystudy;

import io.vov.vitamio.Vitamio;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;

import edu.gdit.enjoystudy.app.Constant;
import edu.gdit.enjoystudy.base.ABaseActivity;
import edu.gdit.enjoystudy.config.AppConfig;
import edu.gdit.enjoystudy.receiver.PushMessageReceiver;
import edu.gdit.enjoystudy.ui.guide.GuideActivity;
import edu.gdit.enjoystudy.utils.IntentUtil;
import edu.gdit.enjoystudy.utils.T;

/**
 * Description:启动页面
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
@SuppressLint("NewApi")
public class SplashActivity extends ABaseActivity {
	private boolean isInitSuccess = false;
	private final int MSG_INIT_ED = 0x1;
	private final int MSG_INIT_FAILD = 0X2;

	public static final String FROM_ME = "fromVitamioInitActivity";
	private boolean isAnimateEnd = false;
	private boolean isInitEnd = false;

	private Handler Handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_INIT_ED:
				if (isAnimateEnd && isInitEnd) {
					if (!AppConfig.isFristRun(getBaseContext())
							&& !AppConfig.isDebug) {
						startMainActivity();
					} else {
						startMainActivity();
//						startGuiActivity();
					}
					SplashActivity.this.finish();
					isAnimateEnd = isInitEnd = false;
				}
				break;
			case MSG_INIT_FAILD:
				T.show(SplashActivity.this, "视频解码库初始化失败，请重新安装", 1);
				break;
			default:
				break;
			}
		}

//		private void startGuiActivity() {
//			IntentUtil.startActivity(SplashActivity.this, GuideActivity.class);
//			SplashActivity.this.overridePendingTransition(R.anim.fade_in,
//					R.anim.fade_out);
//
//		}

		/**
		 * 描述：
		 * 
		 * @return void
		 */
		private void startMainActivity() {
			IntentUtil.startActivity(SplashActivity.this, MainActivity.class);
			SplashActivity.this.overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);

		};
	};

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		View view = View.inflate(this, R.layout.activity_splash, null);
		setContentView(view);
		// 启动页面影藏标题
		getActionBar().hide();
		// 渐变展示启动屏
		setAnimation(view);

		// 已登录，开始百度推送
		if (!PushMessageReceiver.isbind) {
			PushManager.startWork(getApplicationContext(),
					PushConstants.LOGIN_TYPE_API_KEY, Constant.PUSHKEY);
			PushManager.enableLbs(getApplicationContext());
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Handler.removeCallbacksAndMessages(null);
	}

	/**
	 * 描述： 设置过渡动画，并且设置
	 * 
	 * @param view
	 * @return void
	 */
	private void setAnimation(View view) {
		// AlphaAnimation alphaAnimation = new AlphaAnimation(0.3f, 1.0f);
		// alphaAnimation.setDuration(2000);

		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.move_logo);
		view.findViewById(R.id.logo_splash).startAnimation(animation);
		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationEnd(Animation arg0) {
				isAnimateEnd = true;
				Handler.sendEmptyMessage(MSG_INIT_ED);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationStart(Animation animation) {
				new Thread() {

					@Override
					public void run() {
						// 初始化播放器
						if (!Vitamio.isInitialized(SplashActivity.this)
								&& !SplashActivity.this.getIntent()
										.getBooleanExtra(FROM_ME, false)) {

							isInitSuccess = Vitamio.initialize(
									SplashActivity.this,
									getResources().getIdentifier("libarm",
											"raw", getPackageName()));
							if (!isInitSuccess) {
								Handler.sendEmptyMessage(MSG_INIT_FAILD);
							}

						}
						isInitEnd = true;
						Handler.sendEmptyMessage(MSG_INIT_ED);
					}
				}.start();
			}

		});

	}
}
