package edu.gdit.enjoystudy.receiver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.alibaba.fastjson.JSONObject;
import com.baidu.frontia.api.FrontiaPushMessageReceiver;

import edu.gdit.enjoystudy.MainActivity;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.entity.EntityCourse;
import edu.gdit.enjoystudy.entity.PushMsg;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.ui.home.FragmentHome;
import edu.gdit.enjoystudy.utils.AppUtil;
import edu.gdit.enjoystudy.utils.DateUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.vitamio.VP;
import edu.gdit.enjoystudy.vitamio.VideoActivity;

/**
 * Push消息处理receiver。请编写您需要的回调函数， 一般来说： onBind是必须的，用来处理startWork返回值；
 * onMessage用来接收透传消息； onSetTags、onDelTags、onListTags是tag相关操作的回调；
 * onNotificationClicked在通知被点击时回调； onUnbind是stopWork接口的返回值回调
 * 
 * 返回值中的errorCode，解释如下： 0 - Success 10001 - Network Problem 30600 - Internal
 * Server Error 30601 - Method Not Allowed 30602 - Request Params Not Valid
 * 30603 - Authentication Failed 30604 - Quota Use Up Payment Required 30605 -
 * Data Required Not Found 30606 - Request Time Expires Timeout 30607 - Channel
 * Token Timeout 30608 - Bind Relation Not Found 30609 - Bind Number Too Many
 * 
 * 当您遇到以上返回错误时，如果解释不了您的问题，请用同一请求的返回值requestId和errorCode联系我们追查问题。
 * 
 */
public class PushMessageReceiver extends FrontiaPushMessageReceiver {
	/** TAG to Log */
	public static boolean isbind;

	/**
	 * 调用PushManager.startWork后，sdk将对push
	 * server发起绑定请求，这个过程是异步的。绑定请求的结果通过onBind返回。 如果您需要用单播推送，需要把这里获取的channel
	 * id和user id上传到应用server中，再调用server接口用channel id和user id给单个手机或者用户推送。
	 * 
	 * @param context
	 *            BroadcastReceiver的执行Context
	 * @param errorCode
	 *            绑定接口返回值，0 - 成功
	 * @param appid
	 *            应用id。errorCode非0时为null
	 * @param userId
	 *            应用user id。errorCode非0时为null
	 * @param channelId
	 *            应用channel id。errorCode非0时为null
	 * @param requestId
	 *            向服务端发起的请求id。在追查问题时有用；
	 * @return none
	 */
	@Override
	public void onBind(final Context context, int errorCode, String appid,
			String userId, String channelId, String requestId) {
		String responseString = "onbind errorCode=" + errorCode + " appid="
				+ appid + " userId=" + userId + " channelId=" + channelId
				+ " requestId=" + requestId;
		L.d(responseString);

		// 绑定成功，设置已绑定flag，可以有效的减少不必要的绑定请求
		if (errorCode == 0) {
			isbind = true;
		}
		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
		MyVoller voll = new MyVoller(context, new OnGetDataed() {
			@Override
			public void onSuccess(String tag, JSONObject jsonObject,
					int requestCount) {
			}

			@Override
			public void onFail(String tag, int errorCode, String message,
					int requestCount) {
			}
		});
		Map<String, String> params = new HashMap<String, String>();
		params.put("dbid", userId);
		params.put("accesstoken",
				SPUtils.getPrefString(context, SPkey.TOKEN, ""));
		voll.urlToJsonObject("tag", Urls.POSTDBID, params, false);
		// "http://pingkgs3pc:8080/zuiaixue/postdbid?accesstoken=b44d19f4a909c2732f7f004135b0bb97&dbid=123"
	}

	/**
	 * 接收透传消息的函数。
	 * 
	 * @param context
	 *            上下文
	 * @param message
	 *            推送的消息
	 * @param customContentString
	 *            自定义内容,为空或者json字符串
	 */
	@Override
	public void onMessage(Context context, String message,
			String customContentString) {
		String messageString = "透传消息 message=\"" + message
				+ "\" customContentString=" + customContentString;
		L.d(messageString);

		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
		PushMsg msg = JSONObject.parseObject(message, PushMsg.class);

		if (msg == null) {
			return;
		}

		Intent intent = new Intent(context, MainActivity.class);
		String title = "未知推送";
		if (msg.getType() == 0) {
			title = "课程推送 ";
			EntityCourse c = new EntityCourse();
			try {
				c.setId(Integer.parseInt(msg.getData()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			intent = new Intent(context, VideoActivity.class);
			intent.setData(Uri.parse("http://cococ.aliapp.com/1234.mp4"));
			intent.putExtra(VP.displayName, "网络视频");
			intent.putExtra(FragmentHome.INTENT_VIDEO, c);
		} else if (msg.getType() == 1) {
			title = "视频推送 ";
			try {
//				AppUtil.openCourse(context, Integer.parseInt(msg.getData()));
			} catch (NumberFormatException e) {
			}
		} else if (msg.getType() == 2) {
			title = "广告推送 ";
		} else if (msg.getType() == 3) {
			title = "系统通知 ";
		}

		title = title
				+ DateUtil.dateFormat(System.currentTimeMillis(), "yyyy-MM-dd");
		String content = msg.getContent();

		Notification notification = new Notification(R.drawable.logo, title,
				System.currentTimeMillis());

		PendingIntent pintent = PendingIntent.getActivity(context, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		notification.defaults = Notification.DEFAULT_SOUND
				| Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS;
		notification.setLatestEventInfo(context, title, content, pintent);
		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(0, notification);
	}

	/**
	 * 接收通知点击的函数。注：推送通知被用户点击前，应用无法通过接口获取通知的内容。
	 * 
	 * @param context
	 *            上下文
	 * @param title
	 *            推送的通知的标题
	 * @param description
	 *            推送的通知的描述
	 * @param customContentString
	 *            自定义内容，为空或者json字符串
	 */
	@Override
	public void onNotificationClicked(Context context, String title,
			String description, String customContentString) {
		String notifyString = "通知点击 title=\"" + title + "\" description=\""
				+ description + "\" customContent=" + customContentString;
		L.d(notifyString);

		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
	}

	/**
	 * setTags() 的回调函数。
	 * 
	 * @param context
	 *            上下文
	 * @param errorCode
	 *            错误码。0表示某些tag已经设置成功；非0表示所有tag的设置均失败。
	 * @param successTags
	 *            设置成功的tag
	 * @param failTags
	 *            设置失败的tag
	 * @param requestId
	 *            分配给对云推送的请求的id
	 */
	@Override
	public void onSetTags(Context context, int errorCode,
			List<String> sucessTags, List<String> failTags, String requestId) {
		String responseString = "onSetTags errorCode=" + errorCode
				+ " sucessTags=" + sucessTags + " failTags=" + failTags
				+ " requestId=" + requestId;
		L.d(responseString);

		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
	}

	/**
	 * delTags() 的回调函数。
	 * 
	 * @param context
	 *            上下文
	 * @param errorCode
	 *            错误码。0表示某些tag已经删除成功；非0表示所有tag均删除失败。
	 * @param successTags
	 *            成功删除的tag
	 * @param failTags
	 *            删除失败的tag
	 * @param requestId
	 *            分配给对云推送的请求的id
	 */
	@Override
	public void onDelTags(Context context, int errorCode,
			List<String> sucessTags, List<String> failTags, String requestId) {
		String responseString = "onDelTags errorCode=" + errorCode
				+ " sucessTags=" + sucessTags + " failTags=" + failTags
				+ " requestId=" + requestId;
		L.d(responseString);

		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
	}

	/**
	 * listTags() 的回调函数。
	 * 
	 * @param context
	 *            上下文
	 * @param errorCode
	 *            错误码。0表示列举tag成功；非0表示失败。
	 * @param tags
	 *            当前应用设置的所有tag。
	 * @param requestId
	 *            分配给对云推送的请求的id
	 */
	@Override
	public void onListTags(Context context, int errorCode, List<String> tags,
			String requestId) {
		String responseString = "onListTags errorCode=" + errorCode + " tags="
				+ tags;
		L.d(responseString);

		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
	}

	/**
	 * PushManager.stopWork() 的回调函数。
	 * 
	 * @param context
	 *            上下文
	 * @param errorCode
	 *            错误码。0表示从云推送解绑定成功；非0表示失败。
	 * @param requestId
	 *            分配给对云推送的请求的id
	 */
	@Override
	public void onUnbind(Context context, int errorCode, String requestId) {
		String responseString = "onUnbind errorCode=" + errorCode
				+ " requestId = " + requestId;
		L.d(responseString);

		// 解绑定成功，设置未绑定flag，
		if (errorCode == 0) {
			isbind = false;
		}
		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
	}

	// private void updateContent(Context context, String content) {
	// Log.d(TAG, "updateContent");
	// String logText = "";
	//
	// if (!logText.equals("")) {
	// logText += "\n";
	// }
	//
	// SimpleDateFormat sDateFormat = new SimpleDateFormat("HH-mm-ss");
	// logText += sDateFormat.format(new Date()) + ": ";
	// logText += content;
	//
	//
	// Intent intent = new Intent();
	// intent.setClass(context.getApplicationContext(),
	// PushMessageActivity.class);
	// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	// context.getApplicationContext().startActivity(intent);
	// }
}
