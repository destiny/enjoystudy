package edu.gdit.enjoystudy.ui.guide;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import edu.gdit.enjoystudy.LoginActivity;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.utils.IntentUtil;
 

public class GuideActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_guide);
       initView();
    }

	 /**
	 * 描述： 
	 * @return void
	 */
	private void initView() {
		 findViewById(R.id.lastView3).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				IntentUtil.startActivity(GuideActivity.this,
						LoginActivity.class);
				GuideActivity.this.overridePendingTransition(
						R.anim.fade_in, R.anim.fade_out);
				GuideActivity.this.finish();
			}
		});
		
	}
}
