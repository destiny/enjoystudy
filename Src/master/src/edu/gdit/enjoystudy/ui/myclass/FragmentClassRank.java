package edu.gdit.enjoystudy.ui.myclass;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterClassRank;

/**
 * 兴趣小组-排名榜
 * 
 * @author skytoup
 *
 */
public class FragmentClassRank extends Fragment implements OnItemClickListener {

	public final static String TAG = "FragmentClassTeam";
	private AdapterClassRank adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_class_table, container,
				false);
		((TextView) view.findViewById(R.id.tvTitle)).setText("排名榜");
		((TextView) view.findViewById(R.id.tvSubTitle1)).setText("排名");
		((TextView) view.findViewById(R.id.tvSubTitle2)).setText("昵称");
		((TextView) view.findViewById(R.id.tvSubTitle3)).setText("积分");
		ListView listView = (ListView) view.findViewById(R.id.listView);
		listView.setOnItemClickListener(this);
		listView.setAdapter(adapter = new AdapterClassRank(inflater));
		return view;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

	}

}
