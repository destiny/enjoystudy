package edu.gdit.enjoystudy.ui.myclass;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;

/**
 * 兴趣小组
 * 
 * @author skytoup
 *
 */
public class ClassTeamActivity extends BaseActionBarActivity {

	private FragmentManager manager = getSupportFragmentManager();
	private FragmentTransaction transaction = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.color_blue));
		actionBar.setTitle("PS兴趣小组");
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_class_team);

		transaction = manager.beginTransaction();
		transaction.add(R.id.content, new FragmentClassItemChoose(),
				FragmentClassItemChoose.TAG);
		transaction.commit();
	}

	@Override
	public void onBackPressed() {
		if (manager.getBackStackEntryCount() == 0) {
			super.onBackPressed();
		} else {
			manager.popBackStack();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
