/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-8   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.myclass;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterClass;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.utils.IntentUtil;

/**
 * Description:我的班级
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentClass extends BaseFragment implements OnItemClickListener {

	private AdapterClass adapter;

	public static FragmentClass newInstance() {
		return new FragmentClass();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_class, container, false);
		ListView listView = (ListView) view.findViewById(R.id.listView);
		listView.setOnItemClickListener(this);
		listView.setAdapter(adapter = new AdapterClass(inflater));
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onShow(ActionBarActivity activity) {
		activity.getSupportActionBar().setBackgroundDrawable(
				activity.getResources().getDrawable(R.drawable.color_blue));
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		IntentUtil.startActivity(getActivity(), ClassTeamActivity.class);
	}

}
