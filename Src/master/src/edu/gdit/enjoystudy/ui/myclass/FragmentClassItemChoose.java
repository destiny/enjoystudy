package edu.gdit.enjoystudy.ui.myclass;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import edu.gdit.enjoystudy.R;

/**
 * 小组选项选择
 * 
 * @author skytoup
 *
 */
public class FragmentClassItemChoose extends Fragment implements
		OnClickListener {

	public final static String TAG = "FragmentClassItemChoose";
	private FragmentManager manager;
	private FragmentTransaction transaction = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		manager = getFragmentManager();
		View view = inflater.inflate(R.layout.fragment_class_item_choose,
				container, false);
		view.findViewById(R.id.viewMember).setOnClickListener(this);
		view.findViewById(R.id.viewRank).setOnClickListener(this);
		view.findViewById(R.id.viewMsg).setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		transaction = manager.beginTransaction();
		transaction.addToBackStack(TAG);

		String fragmentName = null;
		String tag = null;
		switch (id) {
		case R.id.viewMember:// 小组成员
			fragmentName = FragmentClassTeam.class.getName();
			tag = FragmentClassTeam.TAG;
			break;

		case R.id.viewRank:// 排名
			fragmentName = FragmentClassRank.class.getName();
			tag = FragmentClassRank.TAG;
			break;

		case R.id.viewMsg:// 问题讨论
			return;
		}

		transaction.replace(R.id.content,
				Fragment.instantiate(getActivity(), fragmentName), tag);
		transaction.commit();
	}

}
