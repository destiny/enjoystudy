package edu.gdit.enjoystudy.ui.friends;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;

public class FriendsTeacherInfoActivity extends BaseActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.color_blue_greed));
		actionBar.setTitle("老师XXX");
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_friends_teacher_info);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
