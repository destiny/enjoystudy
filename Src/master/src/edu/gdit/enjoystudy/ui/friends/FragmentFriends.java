/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-8   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.friends;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterFriends;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.entity.EntityAttention;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.utils.IntentUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;

/**
 * Description:首页
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentFriends extends BaseFragment implements
		OnChildClickListener, OnGetDataed {
	private MyVoller mv;
	private ExpandableListView listView;
	private ArrayList<EntityAttention> list;
	private LayoutInflater minflater;
	private ArrayList<EntityAttention> datas;
	private Activity activity;

	public static FragmentFriends newInstance() {
		return new FragmentFriends();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		this.setHasOptionsMenu(true);// 设置显示菜单
		View view = inflater.inflate(R.layout.fragment_friends, container,
				false);
		minflater = inflater;

		listView = (ExpandableListView) view
				.findViewById(R.id.expandableListView);
		listView.setOnChildClickListener(this);
		listView.setGroupIndicator(null);
		// JSONObject jo = CacheDB.getCacheJsonObject( Urls.LISTATTENTION +
		// "get");

		mv = new MyVoller(activity, this);
		loadCache();
		return view;
	}

	private void loadCache() {
		list = CacheDB.getCacheArray(Urls.LISTATTENTION + "get",
				EntityAttention.class);
		listView.setAdapter(new AdapterFriends(activity.getLayoutInflater(),
				list));
		getData();
	}

	private void getData() {
		mv = new MyVoller(activity, this);
		HashMap<String, String> params = new HashMap<String, String>();
		if (L.isDebug) {
			params.put("accesstoken",
					"899489f74a6947ad3e32d960f2df506490e436342d4cb9c1");
		} else {
			params.put("accesstoken",
					SPUtils.getPrefString(activity, SPkey.TOKEN, ""));
		}
		params.put("page", "1");
		mv.urlToJsonObject("get", Urls.LISTATTENTION, params, true);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onShow(ActionBarActivity activity) {
		this.activity = activity;
		if (mv == null) {
			mv = new MyVoller(activity, this);
		}
		activity.getSupportActionBar().setBackgroundDrawable(
				activity.getResources()
						.getDrawable(R.drawable.color_blue_greed));
		if (listView != null) {
			getData();
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		menu.add(0, 0, 0, "").setIcon(R.drawable.home_actionbar_search)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:// 搜索

			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		if (groupPosition == 1) {// 学生
			Intent intent = new Intent(activity, FriendsInfoActivity.class);
			intent.putExtra(FriendsInfoActivity.INTENT_FID,
					"" + datas.get(childPosition).getUserid());
			activity.startActivity(intent);
		} else {// 老师
			IntentUtil
					.startActivity(activity, FriendsTeacherInfoActivity.class);
		}
		return false;
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		if ("get".equals(tag)) {
			datas = (ArrayList<EntityAttention>) JSONArray.parseArray(
					jsonObject.getString("data"), EntityAttention.class);
			listView.setAdapter(new AdapterFriends(minflater, datas));
		}
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		T.showShort(activity, message);
	}

}
