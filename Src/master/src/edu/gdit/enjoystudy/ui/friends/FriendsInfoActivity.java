package edu.gdit.enjoystudy.ui.friends;

import java.util.HashMap;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.nostra13.universalimageloader.core.ImageLoader;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;
import edu.gdit.enjoystudy.entity.EntityFriend;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.utils.DialogUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;

/**
 * 好友信息
 * 
 * @author skytoup
 * 
 */
public class FriendsInfoActivity extends BaseActionBarActivity implements
		OnGetDataed, OnClickListener {

	private final static String TAG_INFO = "info";
	private final static String TAG_ATT = "att";

	public final static String INTENT_FID = "friendID";
	private MyVoller voller;
	private EntityFriend friend;
	Dialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.color_blue_greed));
		actionBar.setTitle("好友XXX");
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_friends_info);
		dialog = DialogUtil.createProgressDialog(this, "数据获取中...");
		int fId = Integer.parseInt(getIntent().getStringExtra(INTENT_FID));
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("uid", "" + fId);
		if (L.isDebug) {
			params.put("accesstoken",
					"899489f74a6947ad3e32d960f2df506490e436342d4cb9c1");
		} else {
			params.put("accesstoken",
					SPUtils.getPrefString(this, SPkey.TOKEN, ""));
		}
		voller = new MyVoller(this, this);
		voller.urlToJsonObject(TAG_INFO, Urls.GET_USER_INFO, params, false);
		dialog.show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		TextView tvAttOrCal = (TextView) findViewById(R.id.tvAttOrCal);
		if (tag.equals(TAG_INFO)) {
			JSONObject obj = jsonObject.getJSONObject("data");
			friend = JSON.parseObject(obj.toJSONString(), EntityFriend.class);
			tvAttOrCal.setOnClickListener(this);
			tvAttOrCal.setText(friend.getIsaffent() == 0 ? "关注" : "取消关注");
			((TextView) findViewById(R.id.tvName)).setText("好友昵称："
					+ friend.getNick());
			((TextView) findViewById(R.id.tvUserTime)).setText("好友使用年龄："
					+ friend.getUserYear());
			((TextView) findViewById(R.id.tvContent)).setText(friend
					.getContent());
			ImageLoader.getInstance().displayImage(friend.getPic(),
					(ImageView) findViewById(R.id.img));
			((TextView) findViewById(R.id.tvCollectCount)).setText(""
					+ friend.getCollectcourse());
			((TextView) findViewById(R.id.tvDoneCount)).setText(""
					+ friend.getDonecourse());
			((TextView) findViewById(R.id.tvMedalCount)).setText(""
					+ friend.getMedalsum());
			((TextView) findViewById(R.id.tvSignCount)).setText(""
					+ friend.getSignincount());
		} else if (tag.equals(TAG_ATT)) {
			if (friend.getIsaffent() == 0) {
				tvAttOrCal.setText("取消关注");
				friend.setIsaffent(1);
			} else {
				tvAttOrCal.setText("关注");
				friend.setIsaffent(0);
			}
			T.showShort(this, jsonObject.getString("message"));
		}
		DialogUtil.disShow(dialog);
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		T.showShort(this, message);
		DialogUtil.disShow(dialog);
		if (tag.equals(TAG_INFO)) {
			finish();
		}
	}

	@Override
	public void onClick(View v) {
		HashMap<String, String> params = new HashMap<String, String>();
		if (L.isDebug) {
			params.put("accesstoken",
					"899489f74a6947ad3e32d960f2df506490e436342d4cb9c1");
		} else {
			params.put("accesstoken",
					SPUtils.getPrefString(this, SPkey.TOKEN, ""));
		}
		params.put("id", "" + friend.getUserid());
		voller.urlToJsonObject(TAG_ATT, friend.getIsaffent() == 0 ? Urls.ATT
				: Urls.CANCLE_ATT, params, false);
		DialogUtil.show(dialog);
	}
}
