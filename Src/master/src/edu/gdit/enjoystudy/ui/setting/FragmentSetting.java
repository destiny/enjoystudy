/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-8   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.setting;

import others.fadingactionbar.view.FadingActionBarHelper;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.app.BroadcastController;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.utils.AppUtil;
import edu.gdit.enjoystudy.utils.DialogUtil;
import edu.gdit.enjoystudy.utils.IntentUtil;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;
import edu.gdit.enjoystudy.view.RippleView;

/**
 * Description:设置界面
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentSetting extends BaseFragment implements OnClickListener {

	private Dialog dialog;
	private RippleView rippView;
	private FadingActionBarHelper mFadingHelper;
	private Button btnLogout;

	public static FragmentSetting newInstance() {
		return new FragmentSetting();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_setting, container,
				false);
		// View view = mFadingHelper.createView(inflater);
		rippView = (RippleView) view.findViewById(R.id.rv);
		RelativeLayout relatRefreash = (RelativeLayout) view
				.findViewById(R.id.relatRefreash);
		RelativeLayout relatSuggest = (RelativeLayout) view
				.findViewById(R.id.relatSuggest);
		RelativeLayout relatRemind = (RelativeLayout) view
				.findViewById(R.id.relatRemind);
		RelativeLayout relatClear = (RelativeLayout) view
				.findViewById(R.id.relatClear);
		RelativeLayout relatGuide = (RelativeLayout) view
				.findViewById(R.id.relatGuide);
		RelativeLayout relatAbout = (RelativeLayout) view
				.findViewById(R.id.relatAbout);
		((TextView) view.findViewById(R.id.tvVesion)).setText("(当前版本为"
				+ AppUtil.getselfVersionName(getActivity()) + ")");
		btnLogout = (Button) view.findViewById(R.id.btnLogout);
		btnLogout.setOnClickListener(this);
		if (!AppUtil.getIsLogin(getActivity())) {
			btnLogout.setVisibility(View.GONE);
		}
		relatRefreash.setOnClickListener(this);
		relatSuggest.setOnClickListener(this);
		relatRemind.setOnClickListener(this);
		relatClear.setOnClickListener(this);
		relatGuide.setOnClickListener(this);
		relatAbout.setOnClickListener(this);
		rippView.setRippleColor(Color.parseColor("#1F7840"), 1);
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		statrAnimation();
	}

	/**
	 * 描述：
	 * 
	 * @return void
	 */
	private void statrAnimation() {
		rippView.postDelayed(new Runnable() {

			@Override
			public void run() {
				rippView.startAnimation(rippView.getWidth(),
						rippView.getHeight());
			}
		}, 500);
	}

	@Override
	public void onShow(ActionBarActivity activity) {
		activity.getSupportActionBar().setBackgroundDrawable(
				activity.getResources().getDrawable(R.drawable.color_brown));
		if (rippView != null) {
			rippView.setRippleColor(Color.parseColor("#1F7840"), 1);
			statrAnimation();
		}
		if (AppUtil.getIsLogin(activity) && btnLogout != null) {
			btnLogout.setVisibility(View.VISIBLE);
		} else if (btnLogout != null) {
			btnLogout.setVisibility(View.GONE);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// initActionbar(activity);
	}

	/**
	 * 描述：
	 * 
	 * @param activity
	 * @return void
	 */
	private void initActionbar(Activity activity) {
		mFadingHelper = new FadingActionBarHelper()
				.actionBarBackground(R.drawable.color_brown)
				.headerLayout(R.layout.include_zax_view)
				.contentLayout(R.layout.fragment_setting).lightActionBar(true);
		// .lightActionBar(actionBarBg == R.drawable.ab_background_light);
		mFadingHelper.initActionBar(activity);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.relatRefreash:// 检测更新
			dialog = DialogUtil
					.createSettingDialog(getActivity(),
							R.drawable.setting_logo_dialog_refreash, "暂无更新",
							"关闭", this);
			dialog.show();
			break;
		case R.id.relatSuggest:// 意见反馈
			IntentUtil.startActivity(getActivity(), SuggestActivity.class);
			break;
		case R.id.relatRemind:// 提醒开关

			break;
		case R.id.relatClear:// 清除缓存

			break;
		case R.id.relatGuide:// 功能引导

			break;
		case R.id.relatAbout:// 关于
			IntentUtil.startActivity(getActivity(), AboutActivity.class);
			break;
		case R.id.btnLogout:// 注销
			SPUtils.setPrefString(getActivity(), SPkey.TOKEN, "");
			btnLogout.setVisibility(View.GONE);
			Intent intent = new Intent();
			intent.setAction(BroadcastController.ACTION_USERCHANGE);
			getActivity().sendBroadcast(intent);
			T.showShort(getActivity(), "已成功注销");
			break;
		case R.id.viewDialogBtn:// 对话框确认按钮
			dialog.dismiss();
			break;
		}
	}
}
