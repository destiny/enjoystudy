/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-8   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.task;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterTask;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.entity.EntityTask;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.utils.DialogUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;

/**
 * Description:今日任务
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentTask extends BaseFragment implements OnGetDataed {

	private MyVoller voller;
	private final static String TAG = "taskToday";
	private Dialog dialog;
	private View view;
	private View hearView;
	private ListView listView;
	private Activity activity;

	public static FragmentTask newInstance() {
		return new FragmentTask();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		this.setHasOptionsMenu(true);// 设置显示菜单
		view = inflater.inflate(R.layout.fragment_task, container, false);
		listView = (ListView) view.findViewById(R.id.listView);
		hearView = activity.getLayoutInflater().inflate(
				R.layout.task_listview_hear, null);
		listView.addHeaderView(hearView);
		listView.setAdapter(new AdapterTask(inflater, null));
		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		menu.add(0, 0, 0, "").setIcon(R.drawable.home_actionbar_signin)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:// Actionbar上的图标
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	private void getData() {
		HashMap<String, String> params = new HashMap<String, String>();
		if (L.isDebug) {
			params.put("accesstoken", "75728c062ecb67bda3d3029ec807b03f");
		} else {
			params.put("accesstoken",
					SPUtils.getPrefString(activity, SPkey.TOKEN, ""));
		}
		voller.urlToJsonObject(TAG, Urls.TASK_TODAY, params, false);
		DialogUtil.show(dialog);
	}

	@Override
	public void onShow(ActionBarActivity activity) {
		this.activity = activity;
		if (voller == null) {
			voller = new MyVoller(activity, this);
		}
		if (dialog == null) {
			dialog = DialogUtil.createProgressDialog(activity, "获取数据中...");
		}
		activity.getSupportActionBar().setBackgroundDrawable(
				activity.getResources().getDrawable(R.drawable.color_orange));
		getData();
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		DialogUtil.disShow(dialog);
		JSONObject obj = jsonObject.getJSONObject("data");
		EntityTask task = (EntityTask) JSON.parseObject(obj.toJSONString(),
				EntityTask.class);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long day = 0;
		try {
			day = (new Date().getTime() - format.parse(task.getStarttime())
					.getTime()) / (24 * 60 * 60 * 1000);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		DecimalFormat df = new DecimalFormat("#.0");
		TextView tvCountDown = (TextView) hearView
				.findViewById(R.id.count_down_time);
		TextView tvStartTime = (TextView) hearView
				.findViewById(R.id.tvStartTime);
		TextView tvPercen = (TextView) hearView.findViewById(R.id.tvPercen);
		TextView tvTimed = (TextView) hearView.findViewById(R.id.tvTimed);
		TextView tvTimeTip = (TextView) hearView.findViewById(R.id.tvTimeTip);
		tvCountDown.setText(task.getCountdown() + "");
		tvStartTime.setText("开始时间" + task.getStarttime());
		tvTimed.setText("任务进行到第" + day + "天");
		tvPercen.setText(df.format(task.getProgress() * 100) + "%");
		tvTimeTip.setText("今天已完成任务的" + tvPercen.getText() + "，还需继续努力");
		listView.setAdapter(new AdapterTask(activity.getLayoutInflater(), task
				.getTodaytask()));
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		DialogUtil.disShow(dialog);
		T.showShort(activity, message);
	}

}
