/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-5   Create
 * 
 **/
package edu.gdit.enjoystudy.ui;

import others.scrollview.pullscrollview.PullScrollView;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import edu.gdit.enjoystudy.LoginActivity;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.app.App;
import edu.gdit.enjoystudy.app.BroadcastController;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.config.AppConfig;
import edu.gdit.enjoystudy.entity.EntityUserInfo;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.interfaces.DrawerMenuCallBack;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.ui.personal.FragmentPersonal;
import edu.gdit.enjoystudy.utils.AppUtil;
import edu.gdit.enjoystudy.utils.IntentUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;

/**
 * Description:Drawer菜单
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentDrawerMenu extends BaseFragment implements OnClickListener {

	public static FragmentDrawerMenu newInstance() {
		return new FragmentDrawerMenu();
	}

	DrawerMenuCallBack mCallBack;
	private App mApplication;
	private PullScrollView mPullScrollView;
	private ImageView mIvhead;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof DrawerMenuCallBack) {
			mCallBack = (DrawerMenuCallBack) activity;
		}
		// 注册一个用户发生变化的广播
		BroadcastController.registerUserChangeReceiver(activity,
				mUserChangeReceiver);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallBack = null;
		L.d("is activity is null?" + (getActivity() == null));
		BroadcastController.unregisterReceiver(getActivity(),
				mUserChangeReceiver);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mApplication = getApp();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_drawer_menu, null);
		img = (ImageView) view.findViewById(R.id.user_avatar);
		img.setOnClickListener(this);
		tvName = (TextView) view.findViewById(R.id.user_name);
		setupUserView();
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mIvhead = (ImageView) view.findViewById(R.id.drawer_bg_img);
		mPullScrollView = (PullScrollView) view
				.findViewById(R.id.drawer_scrollview);
		mPullScrollView.setHeader(mIvhead);
		view.findViewById(R.id.drawer_home).setOnClickListener(this);
		view.findViewById(R.id.drawer_class).setOnClickListener(this);
		view.findViewById(R.id.drawer_cousers).setOnClickListener(this);
		view.findViewById(R.id.drawer_demo).setOnClickListener(this);
		view.findViewById(R.id.drawer_friends).setOnClickListener(this);
		view.findViewById(R.id.drawer_personal).setOnClickListener(this);
		view.findViewById(R.id.drawer_download).setOnClickListener(this);
		view.findViewById(R.id.drawer_setting).setOnClickListener(this);
		view.findViewById(R.id.drawer_task).setOnClickListener(this);
		setupUserView();
	}

	private void setupUserView() {
		// 判断是否已经登录，如果已登录则显示用户的头像与信息
		boolean b = !SPUtils.getPrefString(getActivity(), SPkey.TOKEN, "")
				.equals("");
		if (b) {
			if(AppConfig.isDebug){
				tvName.setText("林凌灵");
				ImageLoader.getInstance().displayImage("http://cococ.qiniudn.com/image/avatar.gif", img);
				return;
			}
			userInfo = CacheDB.getCacheObject(Urls.USER_INFO
					+ FragmentPersonal.TAG, EntityUserInfo.class);
			if (userInfo == null) {
				return;
			}
			tvName.setText(userInfo.getUsername());
			ImageLoader.getInstance().displayImage(userInfo.getPic(), img);
		} else {
			tvName.setText("点击头像登陆");
		}
	}

	private BroadcastReceiver mUserChangeReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			L.d("收到用户账号变化的广播。。。");
			// 接收到变化后，更新用户资料
			setupUserView();
		}

	};
	private EntityUserInfo userInfo;
	private TextView tvName;
	private ImageView img;

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.user_avatar:// 头像
			if (!AppUtil.getIsLogin(getActivity())) {
				IntentUtil.startActivity(getActivity(), LoginActivity.class);
			} else {
				if (mCallBack != null) {
					mCallBack.onClickPresonal();
				}
			}
			break;

		case R.id.drawer_home:// 首页
			if (mCallBack != null) {
				mCallBack.onClickHome();
			}
			break;
		/* ---------------需要登陆 --------------- */
		case R.id.drawer_cousers:// 我的课程
			if (!AppUtil.getIsLogin(getActivity())) {
				T.showShort(getActivity(), "请先登录");
				return;
			}
			if (mCallBack != null) {
				mCallBack.onClickCoursers();
			}
			break;

		case R.id.drawer_task:// 今日任务
			if (!AppUtil.getIsLogin(getActivity())) {
				T.showShort(getActivity(), "请先登录");
				return;
			}
			if (mCallBack != null) {
				mCallBack.onClickTask();
			}
			break;

		case R.id.drawer_class:// 我的班级
			if (!AppUtil.getIsLogin(getActivity())) {
				T.showShort(getActivity(), "请先登录");
				return;
			}
			if (mCallBack != null) {
				mCallBack.onClickClass();
			}
			break;

		case R.id.drawer_friends:// 良师益友
			if (!AppUtil.getIsLogin(getActivity())) {
				T.showShort(getActivity(), "请先登录");
				return;
			}
			if (mCallBack != null) {
				mCallBack.onClickFriends();
			}
			break;

		case R.id.drawer_personal:// 个人中心
			if (!AppUtil.getIsLogin(getActivity())) {
				T.showShort(getActivity(), "请先登录");
				return;
			}
			if (mCallBack != null) {
				mCallBack.onClickPresonal();
			}
			break;
		/*--------------------------------------*/
		/* ---------------需要登陆 --------------- */
		case R.id.drawer_setting:// 设置
			if (mCallBack != null) {
				mCallBack.onClickSettring();
			}
			break;

		case R.id.drawer_download:// 下载
			if (mCallBack != null) {
				mCallBack.onClickDownload();
			}
			break;

		case R.id.drawer_demo:// 退出
			if (mCallBack != null) {
				mCallBack.onClickDemo();
			}
			break;
		}

	}

	@Override
	public void onShow(ActionBarActivity activity) {

	}
}
