/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-10-27   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.video;

import java.util.HashMap;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSONObject;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.entity.EntityCourse;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.ui.couser.MyCollectActivity;
import edu.gdit.enjoystudy.ui.couser.PlanActivity;
import edu.gdit.enjoystudy.ui.home.FragmentHome;
import edu.gdit.enjoystudy.utils.AppUtil;
import edu.gdit.enjoystudy.utils.DialogUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;

/**
 * Description:视频播放更多
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentVMore extends BaseFragment implements OnClickListener,
		OnGetDataed {

	private static final String TAG_COLLECTION = "tag_collection";

	private EntityCourse video;
	private MyVoller voller;
	private Dialog dialog;

	@Override
	public void onShow(ActionBarActivity activity) {

	}

	/**
	 * 描述：实例化自身
	 * 
	 * @return
	 * @return FragmentChapter
	 */
	public static FragmentVMore newInstance() {
		return new FragmentVMore();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater
				.inflate(R.layout.fragment_v_more, container, false);
		view.findViewById(R.id.viewCollection).setOnClickListener(this);
		view.findViewById(R.id.viewAdd).setOnClickListener(this);
		view.findViewById(R.id.viewDownload).setOnClickListener(this);
		view.findViewById(R.id.viewTerm).setOnClickListener(this);
		view.findViewById(R.id.viewShare).setOnClickListener(this);
		video = (EntityCourse) getActivity().getIntent()
				.getSerializableExtra(FragmentHome.INTENT_VIDEO);
		voller = new MyVoller(getActivity(), this);
		dialog = DialogUtil.createProgressDialog(getActivity(), "数据加载中...");
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.viewCollection:// 收藏
			if (!AppUtil.getIsLogin(getActivity())) {
				T.showShort(getActivity(), "请先登陆");
				return;
			}
			HashMap<String, String> map = new HashMap<String, String>();
			if (L.isDebug) {
				map.put("accesstoken",
						"899489f74a6947ad3e32d960f2df506490e436342d4cb9c1");
			} else {
				map.put("accesstoken",
						SPUtils.getPrefString(getActivity(), SPkey.TOKEN, ""));
			}
			map.put("id", "" + video.getId());
			voller.urlToJsonObject(TAG_COLLECTION, Urls.ADD_COLLECTION, map,
					false);
			dialog.show();
			break;
		case R.id.viewAdd:// 加入计划
			if (AppUtil.getIsLogin(getActivity())) {
				Intent intent = new Intent(getActivity(), PlanActivity.class);
				intent.putExtra(MyCollectActivity.INTENT_ID, video.getId());
				intent.putExtra(MyCollectActivity.INTENT_NAME, video.getName());
				startActivity(intent);
			} else {
				T.showShort(getActivity(), "请先登陆");
			}
			break;
		case R.id.viewTerm:// 组队学习

			break;
		case R.id.viewDownload:// 下载

			break;
		case R.id.viewShare:// 分享

			break;
		}
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		if (tag.equals(TAG_COLLECTION)) {
			dialog.dismiss();
			T.showShort(getActivity(), jsonObject.getString("message"));
		}
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		T.showShort(getActivity(), message);
	}

}
