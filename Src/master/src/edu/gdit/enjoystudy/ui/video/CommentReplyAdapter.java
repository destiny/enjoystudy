/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-10-28   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.video;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.entity.EntityUserInfo;
import edu.gdit.enjoystudy.entity.EntityVCommentSub;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.ui.friends.FriendsInfoActivity;
import edu.gdit.enjoystudy.ui.personal.FragmentPersonal;
import edu.gdit.enjoystudy.utils.AppUtil;
import edu.gdit.enjoystudy.utils.HolderUtil;
import edu.gdit.enjoystudy.utils.T;

/**
 * Description: 评论回复
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class CommentReplyAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater mInflater;
	private ArrayList<EntityVCommentSub> list;
	private onRecommentListener listener;

	/**
	 * 
	 */
	public CommentReplyAdapter(Context context, onRecommentListener listener) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
		this.listener = listener;
	}

	public void setdata(ArrayList<EntityVCommentSub> list) {
		this.list = list;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_reply, null);
		}
		final EntityVCommentSub commsub = list.get(position);

		ImageView img = HolderUtil.getView(convertView, R.id.avatar);
		ImageLoader.getInstance().displayImage(commsub.getHead(), img);
		img.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int id = commsub.getUserid();
				int uid = CacheDB.getCacheObject(
						Urls.USER_INFO + FragmentPersonal.TAG,
						EntityUserInfo.class).getUserid();
				if (uid == id) {
					return;
				}
				Context context = mInflater.getContext();
				if (!AppUtil.getIsLogin(context)) {
					T.showShort(context, "请先登陆");
					return;
				}
				Intent intent = new Intent(context, FriendsInfoActivity.class);
				intent.putExtra(FriendsInfoActivity.INTENT_FID, "" + id);
				context.startActivity(intent);
			}
		});

		TextView tv = HolderUtil.getView(convertView, R.id.name);
		tv.setText(commsub.getUsername());

		TextView content = HolderUtil.getView(convertView,
				R.id.blogcomments_item_text);
		if (commsub.getTouserid() != -1) {
			commsub.setTousername(commsub.getTousername()==null?"吴同学":commsub.getTousername());
			commsub.setContent(commsub.getContent()==null?"无内容":commsub.getContent());
			
			content.setText("回复:" + commsub.getTousername() + "\n"
					+ commsub.getContent());
		} else {
			content.setText(commsub.getContent());
		}

		HolderUtil.getView(convertView, R.id.blogcomments_item_reply)
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						if (listener != null) {
							listener.onRecomment(position);
						}
					}
				});

		return convertView;
	}

	interface onRecommentListener {
		public void onRecomment(int id);
	}

}
