/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-10-27   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.video;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.entity.EntityCouserDetail;
import edu.gdit.enjoystudy.entity.EntitySections;
import edu.gdit.enjoystudy.vitamio.VideoActivity;

/**
 * Description:视频播放-章节
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentVChapter extends BaseFragment {

	private ExpandableListView mExpandableListView;
	private ChapterAdapter mAdapter;
	private VideoActivity mActivity;

	@Override
	public void onShow(ActionBarActivity activity) {

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mActivity = (VideoActivity) activity;
	}

	/**
	 * 描述：实例化自身
	 * 
	 * @return
	 * @return FragmentChapter
	 */
	public static FragmentVChapter newInstance() {
		return new FragmentVChapter();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_v_chapter, container,
				false);
		mExpandableListView = (ExpandableListView) view
				.findViewById(R.id.expandableListView);
		mAdapter = new ChapterAdapter(getActivity());
		mExpandableListView.setAdapter(mAdapter);
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mExpandableListView.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				ArrayList<EntitySections> list = mAdapter.getData();
				mActivity.openVideo(list.get(groupPosition).getVideos()
						.get(childPosition));

				return false;
			}
		});

		expandAll();
	}

	/**
	 * 描述：
	 * 
	 * @return void
	 */
	private void expandAll() {
		// 展开所有group
		for (int i = 0, count = mExpandableListView.getCount(); i < count; i++) {
			mExpandableListView.expandGroup(i);
		}
	}

	public void binddata(EntityCouserDetail couserDetail) {
		ArrayList<EntitySections> mEntitySections = couserDetail.getSections();
		mAdapter.setData(mEntitySections);
		mAdapter.notifyDataSetChanged();
		expandAll();
	}

}
