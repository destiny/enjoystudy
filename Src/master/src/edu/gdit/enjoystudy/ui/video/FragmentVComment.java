/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-10-27   Create
 * 
**/
package edu.gdit.enjoystudy.ui.video;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.entity.EntityVComment;
import edu.gdit.enjoystudy.greenDAO.Cache;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.utils.KeyboradUtil;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;

/** 
 * Description:视频播放-评论
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentVComment extends BaseFragment implements OnGetDataed, OnClickListener {
	private CommentAdapter commentAdpter;
	private MyVoller mv;
	private int courseid = 0;//课程id
	private EditText edit;
	private ArrayList<EntityVComment> list;

	@Override
	public void onShow(ActionBarActivity activity) {
		
	}
	
	 /**
	 * 描述：实例化自身
	 * @return 
	 * @return FragmentChapter
	 */
	public static FragmentVComment newInstance(){
		return new FragmentVComment();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_v_comment, container,false);
		ListView lv=(ListView) view.findViewById(R.id.lv_video);
		commentAdpter=new CommentAdapter(getActivity());
		lv.setAdapter(commentAdpter);
		lv.setOnItemClickListener(commentAdpter);
		
		list = (ArrayList<EntityVComment>) CacheDB.getCacheArray( Urls.GETCOMMENT + "get", EntityVComment.class);
		commentAdpter.setdata(list);
		
		mv = new MyVoller(getActivity(), this);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("id", ""+courseid);
		mv.urlToJsonObject("get", Urls.GETCOMMENT, params, true);
		
		view.findViewById(R.id.btn_send).setOnClickListener(this);
		edit = (EditText) view.findViewById(R.id.edit);
		return view;
	}
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		if ("get".equals(tag)) {
			list = (ArrayList<EntityVComment>) JSONArray.parseArray(jsonObject.getString("data"), EntityVComment.class);
			commentAdpter.setdata(list);			
		} else if ("send".equals(tag)) {
			edit.setText("");
			T.showLong(getActivity(), "评论成功!");
			KeyboradUtil.closeKeyboard(getActivity());
		}
	}

	@Override
	public void onFail(String tag, int errorCode, String message,int requestCount) {
		T.showShort(getActivity(),message);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btn_send:
			String str  = edit.getText().toString();
			if (TextUtils.isEmpty(str)) {
				T.showLong(getActivity(), "请输入评论内容");
			} else {
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("cid", courseid+"");
				params.put("accesstoken", SPUtils.getPrefString(getActivity(), SPkey.TOKEN, ""));
				params.put("content", str);
				mv.urlToJsonObject("send", Urls.DISCUSSCOURSE, params, false);				
			}
			break;
		default:
			break;
		}
		
	}
	
	

}
