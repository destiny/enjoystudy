/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-10-27   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.video;

import java.util.ArrayList;

import com.gc.materialdesign.views.ButtonFlat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.entity.EntitySections;
import edu.gdit.enjoystudy.utils.HolderUtil;

/**
 * Description:
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class ChapterAdapter extends BaseExpandableListAdapter {

	private LayoutInflater mInflater;
	private ArrayList<EntitySections> mList = new ArrayList<EntitySections>();

	public ChapterAdapter(Context context) {
		mInflater = LayoutInflater.from(context);
	}

	public void setData(ArrayList<EntitySections> list) {
		mList = list;
	}

	public ArrayList<EntitySections> getData() {
		return mList;
	}

	@Override
	public int getGroupCount() {
		return mList.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return mList.get(groupPosition).getVideos().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return mList.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return mList.get(groupPosition).getVideos().get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_ipview_group, null);
		}
		ImageView indicator = HolderUtil.getView(convertView,	R.id.group_indicator);
		TextView textView = HolderUtil.getView(convertView, R.id.parent_name);
		// (ImageView) convertView.findViewById(R.id.group_indicator);
		textView.setText(mList.get(groupPosition).getName());
		if (isExpanded) {
			convertView.setBackgroundResource(R.drawable.card_white_top);
			indicator.setImageResource(R.drawable.triangle_down);
		} else {
			convertView.setBackgroundResource(R.drawable.task_bg_item);
			indicator.setImageResource(R.drawable.triangle_left);
		}
		// 必须使用资源Id当key（不是资源id会出现运行时异常），android本意应该是想用tag来保存资源id对应组件。
		// 将groupPosition，childPosition通过setTag保存,在onItemLongClick方法中就可以通过view参数直接拿到了
		convertView.setTag(R.id.ImageView01, groupPosition);
		convertView.setTag(R.id.ImageView02, -1);
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_ipview_child, null);
		}
		TextView btn = HolderUtil.getView(convertView, R.id.expand_child);
		btn.setText(mList.get(groupPosition).getVideos().get(childPosition).getName());
		if (isLastChild) {
			convertView.setBackgroundResource(R.drawable.card_white_bottom);
		} else {
			convertView.setBackgroundResource(R.drawable.card_white_mid);
		}
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
