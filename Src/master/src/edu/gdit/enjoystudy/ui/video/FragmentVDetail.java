/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-10-27   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.video;

import java.util.HashMap;

import others.imageview.customiamge.widget.CircleImageView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.nostra13.universalimageloader.core.ImageLoader;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.entity.EntityCourse;
import edu.gdit.enjoystudy.entity.EntityCouserDetail;
import edu.gdit.enjoystudy.entity.EntityUserInfo;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.ui.friends.FriendsInfoActivity;
import edu.gdit.enjoystudy.ui.personal.FragmentPersonal;
import edu.gdit.enjoystudy.utils.AppUtil;
import edu.gdit.enjoystudy.utils.T;
import edu.gdit.enjoystudy.vitamio.VideoActivity;

/**
 * Description:视频播放-详细
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentVDetail extends BaseFragment implements OnGetDataed {

	private VideoActivity mActivity;
	private EntityCourse mCousers;
	private MyVoller mVoller;
	private EntityCouserDetail mCouserDetail;
	private CircleImageView mCiv;
	private TextView mTv;
	private TextView mtv_lv;
	private TextView mTv_1;
	private TextView mTv_2;

	@Override
	public void onShow(ActionBarActivity activity) {

	}

	/**
	 * 描述：实例化自身
	 * 
	 * @return
	 * @return FragmentChapter
	 */
	public static FragmentVDetail newInstance() {
		return new FragmentVDetail();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_v_detail, container,
				false);
		mCiv = (CircleImageView) view.findViewById(R.id.icon);
		mCiv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mCouserDetail == null) {
					return;
				}
				int id = mCouserDetail.getTeacherid();
				int uid = CacheDB.getCacheObject(
						Urls.USER_INFO + FragmentPersonal.TAG,
						EntityUserInfo.class).getUserid();
				if (uid == id) {
					return;
				}
				Context context = getActivity();
				if (!AppUtil.getIsLogin(context)) {
					T.showShort(context, "请先登陆");
					return;
				}
				Intent intent = new Intent(context, FriendsInfoActivity.class);
				intent.putExtra(FriendsInfoActivity.INTENT_FID, "" + id);
				context.startActivity(intent);
			}
		});
		mTv = (TextView) view.findViewById(R.id.teacher_name);
		mtv_lv = (TextView) view.findViewById(R.id.lv);
		mTv_1 = (TextView) view.findViewById(R.id.introduce);
		mTv_2 = (TextView) view.findViewById(R.id.introduce_cousers);
		getDataFromeNetById(mCousers.getId());
		return view;
	}

	/**
	 * 描述：
	 * 
	 * @return void
	 */
	private void getDataFromeNetById(int id) {
		if (id == 0) {
			// linglingling
		}
		// if (AppConfig.isDebug) {
		// id = 29;
		// }
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("id", id + "");
		mVoller.urlToJsonObject("tag", Urls.COUSERS, map, true);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mVoller = new MyVoller(activity, this);
		mActivity = (VideoActivity) activity;
		mCousers = mActivity.getmCoursers();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		mCouserDetail = JSON.parseObject(jsonObject.getString("data"),
				EntityCouserDetail.class);
		FragmentVChapter vChapter = (FragmentVChapter) getActivity()
				.getSupportFragmentManager().findFragmentByTag(
						FragmentVChapter.class.getName());
		vChapter.binddata(mCouserDetail);
		mTv.setText(mCouserDetail.getContent());
		mtv_lv.setText("LV:" + mCouserDetail.getTlevel());
		mTv_1.setText("简介:" + mCouserDetail.getUserintroduction());
		mTv_2.setText(mCouserDetail.getCourseintroduction());
		ImageLoader.getInstance().displayImage(mCouserDetail.getHead(), mCiv);

	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		T.showShort(getActivity(), message);
	}

}
