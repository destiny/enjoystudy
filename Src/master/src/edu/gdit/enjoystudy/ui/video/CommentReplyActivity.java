/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-10-28   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.video;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nostra13.universalimageloader.core.ImageLoader;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;
import edu.gdit.enjoystudy.entity.EntityVComment;
import edu.gdit.enjoystudy.entity.EntityVCommentSub;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.ui.video.CommentReplyAdapter.onRecommentListener;
import edu.gdit.enjoystudy.utils.AppUtil;
import edu.gdit.enjoystudy.utils.KeyboradUtil;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;

/**
 * Description:评论详情
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class CommentReplyActivity extends BaseActionBarActivity implements
		OnGetDataed, OnClickListener, onRecommentListener {
	private EntityVComment commentdata;
	private CommentReplyAdapter adapter;
	private ArrayList<EntityVCommentSub> list;
	private EditText edit;
	private MyVoller mv;
	private int touserid = -1;// 回复某个用户

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.color_blue_greed));
		actionBar.setTitle("评论详情");
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_comment_reply);
		ListView listView = (ListView) findViewById(R.id.lv_reply);
		listView.setAdapter((adapter = new CommentReplyAdapter(this, this)));

		commentdata = (EntityVComment) getIntent().getSerializableExtra("data");

		ImageView img = (ImageView) findViewById(R.id.blogcomments_head_avatar);
		ImageLoader.getInstance().displayImage(commentdata.getHead(), img);

		TextView name = (TextView) findViewById(R.id.blogcomments_head_name);
		name.setText(commentdata.getUsername());

		TextView content = (TextView) findViewById(R.id.blogcomments_head_title);
		content.setText(commentdata.getContent());

		TextView count = (TextView) findViewById(R.id.count);
		count.setText(commentdata.getSubdata().size() + "");

		mv = new MyVoller(this, this);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("cid", commentdata.getId() + "");
		params.put("page", "1");
		mv.urlToJsonObject("get", Urls.MORECOMMENT, params, true);

		findViewById(R.id.btn_send).setOnClickListener(this);
		edit = (EditText) findViewById(R.id.edit);
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		if ("get".equals(tag)) {
			list = (ArrayList<EntityVCommentSub>) JSONArray.parseArray(
					jsonObject.getString("data"), EntityVCommentSub.class);
			TextView count = (TextView) findViewById(R.id.count);
			count.setText(list.size() + "");
			adapter.setdata(list);
		} else if ("send".equals(tag)) {
			edit.setText("");
			T.showLong(this, "评论成功!");
			KeyboradUtil.closeKeyboard(this);
		}
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		T.showLong(this, message);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btn_send:
			if (!AppUtil.getIsLogin(getActivity())) {
				T.showShort(getActivity(), "请先登陆");
				return;
			}
			String str = edit.getText().toString();
			if (TextUtils.isEmpty(str)) {
				T.showLong(getActivity(), "请输入评论内容");
			} else {
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("cid", commentdata.getId() + "");
				params.put("accesstoken",
						SPUtils.getPrefString(getActivity(), SPkey.TOKEN, ""));
				params.put("content", str);
				params.put("uid", touserid + "");
				mv.urlToJsonObject("send", Urls.COMMENTDISCUS, params, false);
			}
			break;
		default:
			break;
		}

	}

	@Override
	public void onRecomment(int id) {
		if (edit != null) {
			edit.performClick();
			edit.setHint("回复:" + list.get(id).getUsername());
		}
		touserid = list.get(id).getId();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
