/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-10-28   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.video;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.CommentSubAdapter;
import edu.gdit.enjoystudy.entity.EntityUserInfo;
import edu.gdit.enjoystudy.entity.EntityVComment;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.ui.friends.FriendsInfoActivity;
import edu.gdit.enjoystudy.ui.personal.FragmentPersonal;
import edu.gdit.enjoystudy.utils.AppUtil;
import edu.gdit.enjoystudy.utils.HolderUtil;
import edu.gdit.enjoystudy.utils.T;

/**
 * Description:
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class CommentAdapter extends BaseAdapter implements OnItemClickListener {

	private Context mContext;
	private LayoutInflater mInflater;
	private ArrayList<EntityVComment> list;

	/**
	 * 
	 */
	public CommentAdapter(Context context) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
	}

	public void setdata(ArrayList<EntityVComment> list) {
		this.list = list;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_comment, null);
		}

		final EntityVComment comm = list.get(position);
		ImageView img = HolderUtil.getView(convertView,
				R.id.blogcomments_head_avatar);
		img.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int id = comm.getUserid();
				int uid = CacheDB.getCacheObject(
						Urls.USER_INFO + FragmentPersonal.TAG,
						EntityUserInfo.class).getUserid();
				if (uid == id) {
					return;
				}
				Context context = mInflater.getContext();
				if (!AppUtil.getIsLogin(context)) {
					T.showShort(context, "请先登陆");
					return;
				}
				Intent intent = new Intent(context, FriendsInfoActivity.class);
				intent.putExtra(FriendsInfoActivity.INTENT_FID, "" + id);
				context.startActivity(intent);
			}
		});
		ImageLoader.getInstance().displayImage(comm.getHead(), img);
		TextView name = HolderUtil.getView(convertView,
				R.id.blogcomments_head_name);
		name.setText(comm.getUsername());
		TextView content = HolderUtil.getView(convertView,
				R.id.blogcomments_head_title);
		content.setText(comm.getContent());

		TextView count = HolderUtil.getView(convertView,
				R.id.blogcomments_count);
		count.setText("" + comm.getSubdata().size());

		ListView listview = HolderUtil.getView(convertView, R.id.lv_comment);
		CommentSubAdapter adapter = new CommentSubAdapter(mInflater);
		listview.setAdapter(adapter);
		adapter.setdata(comm.getSubdata());

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				onItemClick(null, view, position, position);
			}
		});

		return convertView;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Intent intent = new Intent(mContext, CommentReplyActivity.class);
		intent.putExtra("data", list.get(position));
		mContext.startActivity(intent);
	}

}
