/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-8   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.personal;

import java.util.HashMap;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.nostra13.universalimageloader.core.ImageLoader;

import edu.gdit.enjoystudy.MainActivity;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.entity.EntityUserInfo;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.ui.couser.MyCollectActivity;
import edu.gdit.enjoystudy.utils.DialogUtil;
import edu.gdit.enjoystudy.utils.IntentUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;

/**
 * Description:个人中心
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentPersonal extends BaseFragment implements OnClickListener,
		OnItemClickListener, OnGetDataed {

	public final static String TAG = "userInfo";
	private final String[] DIALOG_ITEMS_CHANGE = new String[] { "修改头像",
			"修改背景墙", "更换勋章" };
	private final String[] DIALOG_ITEMS_CHANGE_IMG = new String[] { "本地", "拍照" };
	private String[] currentItems;
	private int changeId;
	private Dialog dialog;
	private Dialog waitDialog;
	private MyVoller voller;
	private EntityUserInfo userInfo;
	private View view;
	private Activity activity;

	public static FragmentPersonal newInstance() {
		return new FragmentPersonal();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		if (view == null) {
			view = inflater.inflate(R.layout.fragment_personal, container,
					false);
		}
		view.findViewById(R.id.viewCollect).setOnClickListener(this);
		view.findViewById(R.id.viewStudyed).setOnClickListener(this);
		view.findViewById(R.id.viewClass).setOnClickListener(this);
		view.findViewById(R.id.viewAttention).setOnClickListener(this);
		view.findViewById(R.id.viewCourse).setOnClickListener(this);
		view.findViewById(R.id.viewComment).setOnClickListener(this);
		view.findViewById(R.id.viewMedal).setOnClickListener(this);
		view.findViewById(R.id.imgHearView).setOnClickListener(this);

		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onShow(ActionBarActivity activity) {
		this.activity = activity;
		if (voller == null) {
			voller = new MyVoller(activity, this);
		}
		if (waitDialog == null) {
			waitDialog = DialogUtil.createProgressDialog(activity, "获取数据中...");
		}
		activity.getSupportActionBar().setBackgroundDrawable(
				activity.getResources().getDrawable(R.drawable.color_greed));
		if (view == null) {
			view = activity.getLayoutInflater().inflate(
					R.layout.fragment_personal, null);
		}
		loadCache();
	}

	private void loadCache() {
		userInfo = CacheDB.getCacheObject(Urls.USER_INFO + TAG,
				EntityUserInfo.class);
		if (userInfo != null) {
			setViewData();
		} else {
			DialogUtil.show(waitDialog);
		}
		getData();
	}

	private void getData() {
		HashMap<String, String> map = new HashMap<String, String>();
		if (L.isDebug) {
			map.put("accesstoken", "75728c062ecb67bda3d3029ec807b03f");
		} else {
			map.put("accesstoken",
					SPUtils.getPrefString(activity, SPkey.TOKEN, ""));
		}
		voller.urlToJsonObject(TAG, Urls.USER_INFO, map, true);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.imgHearView:
			currentItems = DIALOG_ITEMS_CHANGE;
			dialog = DialogUtil.createItemDialog(activity,
					R.drawable.person_division, R.drawable.btn_dialog_person,
					R.layout.text_view_greed, currentItems, this);
			dialog.show();
			break;
		case R.id.viewCollect:// 我的收藏
			Intent intent = new Intent(activity, MyCollectActivity.class);
			intent.putExtra(MyCollectActivity.INTENT_COLOR,
					R.drawable.color_greed);
			startActivity(intent);
			break;
		case R.id.viewStudyed:// 学过课程

			break;
		case R.id.viewClass:// 我的班级
			((MainActivity) activity).showContent(3);
			break;
		case R.id.viewAttention:// 关注的人

			break;
		case R.id.viewCourse:// 历程
			IntentUtil.startActivity(activity, CourseTreeAvctivity.class);
			break;
		case R.id.viewComment:// 发表的评论

			break;
		case R.id.viewMedal:// 勋章榜
			IntentUtil.startActivity(activity, PersonMedalsActivity.class);
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		dialog.dismiss();
		if (currentItems == DIALOG_ITEMS_CHANGE) {// 修改对话框
			switch (position) {
			case 0:// 修改头像
			case 1:// 修改背景墙
				changeId = position;
				currentItems = DIALOG_ITEMS_CHANGE_IMG;
				dialog = DialogUtil.createItemDialog(activity,
						R.drawable.person_division,
						R.drawable.btn_dialog_person, R.layout.text_view_greed,
						currentItems, this);
				dialog.show();
				break;
			case 2:// 更换勋章
				T.showShort(activity, "开发中...");
				break;
			}
		} else if (currentItems == DIALOG_ITEMS_CHANGE_IMG) {// 图片选择方式
			switch (position) {
			case 0:// 本地
				if (changeId == 0) {// 头像

				} else {// 背景墙

				}
				T.showShort(activity, "开发中...");
				break;
			case 1:// 拍照
				if (changeId == 0) {// 头像

				} else {// 背景墙

				}
				T.showShort(activity, "开发中...");
				break;
			}
		}
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		JSONObject obj = jsonObject.getJSONObject("data");
		userInfo = (EntityUserInfo) JSON.parseObject(obj.toJSONString(),
				EntityUserInfo.class);
		setViewData();
		DialogUtil.disShow(waitDialog);
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		DialogUtil.disShow(waitDialog);
		T.showShort(activity, message);
	}

	private void setViewData() {
		((TextView) view.findViewById(R.id.tvCollectCount2)).setText("收藏课程数："
				+ userInfo.getCollectcourse());
		((TextView) view.findViewById(R.id.tvCollectCount)).setText(""
				+ userInfo.getCollectcourse());
		((TextView) view.findViewById(R.id.tvStudyCourse)).setText(""
				+ userInfo.getDonecourse());
		((TextView) view.findViewById(R.id.tvAttentCount)).setText(""
				+ userInfo.getAffsum());
		((TextView) view.findViewById(R.id.tvAttentCount2)).setText("关注人数："
				+ userInfo.getAffsum());
		((TextView) view.findViewById(R.id.tvSendCommentCount)).setText(""
				+ userInfo.getSendcommentsum());
		((TextView) view.findViewById(R.id.tvCommentCount)).setText("我的评论条数："
				+ userInfo.getSendcommentsum());
		((TextView) view.findViewById(R.id.tvMedalCount)).setText(""
				+ userInfo.getMedalsum());
		((TextView) view.findViewById(R.id.tvMsgCount)).setText("我的信息条数："
				+ userInfo.getCommentsum());
		((TextView) view.findViewById(R.id.tvName)).setText("我的昵称："
				+ userInfo.getUsername());
		((TextView) view.findViewById(R.id.tvAge)).setText("我使用的年龄："
				+ userInfo.getAge());
		ImageLoader.getInstance().displayImage(userInfo.getPic(),
				(ImageView) view.findViewById(R.id.imgHearView));
	}

}
