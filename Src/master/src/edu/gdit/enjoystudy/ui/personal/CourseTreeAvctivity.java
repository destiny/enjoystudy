package edu.gdit.enjoystudy.ui.personal;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterPersonCourseTree;
import edu.gdit.enjoystudy.app.App;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;

/**
 * 历程树
 * 
 * @author skytoup
 *
 */
public class CourseTreeAvctivity extends BaseActionBarActivity {

	private AdapterPersonCourseTree adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.color_greed));
		actionBar.setTitle("历程");
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_person_course_tree);
		ListView listView = (ListView) findViewById(R.id.listView);
		View view = getLayoutInflater().inflate(
				R.layout.person_course_tree_head, null);
		listView.addHeaderView(view);
		LinearLayout layout = new LinearLayout(this);
		layout.setLayoutParams(new AbsListView.LayoutParams(
				App.getSceenWidth(), App.getSceenHeight() / 5));
		layout.setGravity(Gravity.CENTER_HORIZONTAL);
		ImageView img = new ImageView(this);
		img.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.MATCH_PARENT));
		img.setBackgroundResource(R.drawable.person_course_tree_ver);
		layout.addView(img);
		listView.addFooterView(layout);
		listView.setAdapter(adapter = new AdapterPersonCourseTree(
				getLayoutInflater()));
		listView.setSelection(9);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
