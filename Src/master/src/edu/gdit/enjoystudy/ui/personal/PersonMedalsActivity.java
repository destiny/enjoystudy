package edu.gdit.enjoystudy.ui.personal;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.ListView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterPersonMedals;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;

/**
 * 个人中心-勋章榜
 * 
 * @author skytoup
 *
 */
public class PersonMedalsActivity extends BaseActionBarActivity {

	private AdapterPersonMedals adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.color_greed));
		actionBar.setTitle("勋章榜");
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_person_medals);
		ListView listView = (ListView) findViewById(R.id.listView);
		listView.setAdapter(adapter = new AdapterPersonMedals(
				getLayoutInflater()));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
