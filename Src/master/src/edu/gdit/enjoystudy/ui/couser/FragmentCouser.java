/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-8   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.couser;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.utils.IntentUtil;
import edu.gdit.enjoystudy.utils.T;

/**
 * Description:我的课程
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentCouser extends BaseFragment implements OnClickListener {

	private RelativeLayout mrelatPlan;
	private RelativeLayout mrelatAllPlan;
	private RelativeLayout mrelatProgress;
	private RelativeLayout mrelatRemind;
	private RelativeLayout mrelatCollect;

	public static FragmentCouser newInstance() {
		return new FragmentCouser();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		this.setHasOptionsMenu(false);// 设置不显示菜单
		View view = inflater.inflate(R.layout.fragment_cousers, container,
				false);
		mrelatPlan = (RelativeLayout) view.findViewById(R.id.relatPlan);
		mrelatAllPlan = (RelativeLayout) view.findViewById(R.id.relatAllPlan);
		mrelatProgress = (RelativeLayout) view.findViewById(R.id.relatProgress);
		mrelatRemind = (RelativeLayout) view.findViewById(R.id.relatRemind);
		mrelatCollect = (RelativeLayout) view.findViewById(R.id.relatCollect);
		mrelatPlan.setOnClickListener(this);
		mrelatAllPlan.setOnClickListener(this);
		mrelatProgress.setOnClickListener(this);
		mrelatRemind.setOnClickListener(this);
		mrelatCollect.setOnClickListener(this);
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onShow(ActionBarActivity activity) {
		activity.getActionBar().setBackgroundDrawable(
				activity.getResources().getDrawable(R.drawable.color_purple));
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.relatPlan:// 定制计划
			IntentUtil.startActivity(getActivity(), PlanActivity.class);
			break;
		case R.id.relatAllPlan:// 全部计划
			IntentUtil.startActivity(getActivity(), AllPlanActivity.class);
			break;
		case R.id.relatProgress:// 学习进度
			IntentUtil.startActivity(getActivity(),
					MyCourseProgressActivity.class);
			break;
		case R.id.relatRemind:// 设置提醒
			T.showShort(getActivity(), "模块开发中...");
			// IntentUtil
			// .startActivity(getActivity(), SettingRemindActivity.class);
			break;
		case R.id.relatCollect:// 我的收藏
			IntentUtil.startActivity(getActivity(), MyCollectActivity.class);
			break;
		}
	}

}
