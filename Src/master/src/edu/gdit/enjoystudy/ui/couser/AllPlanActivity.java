package edu.gdit.enjoystudy.ui.couser;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.ListView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterCourseAllPlan;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;
import edu.gdit.enjoystudy.entity.EntityPlan;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.utils.DialogUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;

/**
 * 所有计划
 * 
 * @author skytoup
 * 
 */
public class AllPlanActivity extends BaseActionBarActivity implements
		OnGetDataed {

	private ListView listView;
	private Dialog dialog;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.color_purple));
		actionBar.setTitle("全部计划");
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_couser_all_plan);
		ArrayList<EntityPlan> datas = CacheDB.getCacheArray(Urls.ALL_PLAN
				+ EntityPlan.TAG, EntityPlan.class);
		listView = (ListView) findViewById(R.id.listView);
		listView.setAdapter(new AdapterCourseAllPlan(getLayoutInflater(), datas));
		MyVoller voller = new MyVoller(this, this);
		HashMap<String, String> map = new HashMap<String, String>();
		if (L.isDebug) {
			map.put("accesstoken",
					"899489f74a6947ad3e32d960f2df506490e436342d4cb9c1");
		} else {
			map.put("accesstoken",
					SPUtils.getPrefString(getActivity(), SPkey.TOKEN, ""));
		}
		voller.urlToJsonObject(EntityPlan.TAG, Urls.ALL_PLAN, map, true);
		dialog = DialogUtil.createProgressDialog(this, "获取计划中...");
		dialog.show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		JSONArray jsonArray = jsonObject.getJSONArray("data");
		ArrayList<EntityPlan> plans = (ArrayList<EntityPlan>) JSON.parseArray(
				jsonArray.toJSONString(), EntityPlan.class);
		listView.setAdapter(new AdapterCourseAllPlan(getLayoutInflater(), plans));
		DialogUtil.disShow(dialog);
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		DialogUtil.disShow(dialog);
		T.showShort(this, message);
	}
}
