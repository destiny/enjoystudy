package edu.gdit.enjoystudy.ui.couser;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;
import edu.gdit.enjoystudy.utils.DateUtil;
import edu.gdit.enjoystudy.utils.DialogUtil;
import edu.gdit.enjoystudy.utils.DialogUtil.OnDateSelectListener;
import edu.gdit.enjoystudy.utils.StringUtils;

/**
 * 创建设置提醒
 * 
 * @author skytoup
 *
 */
public class SettingRemindCreateActivity extends BaseActionBarActivity
		implements OnClickListener, OnDateSelectListener {

	private TextView tvTime;
	private String hour;
	private String minute;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.color_purple));
		actionBar.setTitle("设置");
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_setting_remind_create);
		tvTime = (TextView) findViewById(R.id.tvTime);
		tvTime.setOnClickListener(this);
		// 获取时、分
		hour = DateUtil.getHourNow();
		minute = DateUtil.getMinuteNow();
		tvTime.setText(hour + ":" + minute);// 设置现在的时间

	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.tvTime:// 选择时间
			DialogUtil.createDateSelectDialog(this, Integer.parseInt(hour),
					Integer.parseInt(minute), this).show();
			break;

		}
	}

	@Override
	public void onSelectDate(int hour, int minute) {
		this.hour = StringUtils.intToDateString(hour);
		this.minute = StringUtils.intToDateString(minute);
		tvTime.setText(this.hour + ":" + this.minute);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
