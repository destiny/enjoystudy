package edu.gdit.enjoystudy.ui.couser;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterHome;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;
import edu.gdit.enjoystudy.entity.EntityCourse;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.utils.AppUtil;
import edu.gdit.enjoystudy.utils.DialogUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;

/**
 * 我的收藏
 * 
 * @author skytoup
 * 
 */
public class MyCollectActivity extends BaseActionBarActivity implements
		OnItemClickListener, OnGetDataed {

	public final static String INTENT_NAME = "name";
	public final static String INTENT_ID = "id";

	public final static String INTENT_COLOR = "intentColor";
	private final static String TAG_COLLECT = "tagCollect";
	private AdapterHome adapter;
	private MyVoller voller;
	private boolean isChoose;
	private ArrayList<EntityCourse> videos;
	private Dialog dialog;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		isChoose = getIntent().getBooleanExtra(PlanActivity.INTENT_ISCHOOSE,
				false);
		setResult(RESULT_CANCELED);
		ActionBar actionBar = getSupportActionBar();
		actionBar
				.setBackgroundDrawable(getResources().getDrawable(
						getIntent().getIntExtra(INTENT_COLOR,
								R.drawable.color_purple)));
		actionBar.setTitle("我的收藏");
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_couser_my_collect);
		voller = new MyVoller(this, this);
		ListView listView = (ListView) findViewById(R.id.listView);
		adapter = new AdapterHome(getLayoutInflater());
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
		dialog = DialogUtil.createProgressDialog(getActivity(), "获取收藏课程中...");
		loadCache();
	}

	private void loadCache() {
		ArrayList<EntityCourse> data = CacheDB.getCacheArray(Urls.COLLECT_CLASS
				+ TAG_COLLECT, EntityCourse.class);
		adapter.setDatas(data);
		getData();
	}

	private void getData() {
		HashMap<String, String> map = new HashMap<String, String>();
		if (L.isDebug) {
			map.put("accesstoken",
					"899489f74a6947ad3e32d960f2df506490e436342d4cb9c1");
		} else {
			map.put("accesstoken", SPUtils.getPrefString(this, SPkey.TOKEN, ""));
		}
		voller.urlToJsonObject(TAG_COLLECT, Urls.COLLECT_CLASS, map, true);
		DialogUtil.show(dialog);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		EntityCourse course = videos.get(position);
		if (isChoose) {
			Intent intent = new Intent();
			intent.putExtra(INTENT_ID, course.getId());
			intent.putExtra(INTENT_NAME, course.getName());
			setResult(RESULT_OK, intent);
			finish();
		} else {
			AppUtil.openCourse(this, course);
		}
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		JSONArray datas = jsonObject.getJSONArray("data");
		videos = (ArrayList<EntityCourse>) JSON.parseArray(
				datas.toJSONString(), EntityCourse.class);
		adapter.setDatas(videos);
		DialogUtil.disShow(dialog);
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		T.showShort(this, "获取收藏课程失败");
		DialogUtil.disShow(dialog);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
