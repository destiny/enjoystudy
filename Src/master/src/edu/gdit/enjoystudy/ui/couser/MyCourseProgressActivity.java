package edu.gdit.enjoystudy.ui.couser;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.ListView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterMyCourseProgress;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;

/**
 * 每日进度
 * 
 * @author skytoup
 *
 */
public class MyCourseProgressActivity extends BaseActionBarActivity {

	private AdapterMyCourseProgress adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.color_purple));
		actionBar.setTitle("每日进度");
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_mycourse_progress);
		ListView listView = (ListView) findViewById(R.id.listView);
		listView.setAdapter(adapter = new AdapterMyCourseProgress(
				getLayoutInflater()));
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
