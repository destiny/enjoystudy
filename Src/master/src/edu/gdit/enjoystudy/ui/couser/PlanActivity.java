package edu.gdit.enjoystudy.ui.couser;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;
import edu.gdit.enjoystudy.entity.EntityPlan;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.utils.DialogUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;

/**
 * 定制学习计划
 * 
 * @author skytoup
 * 
 */
public class PlanActivity extends BaseActionBarActivity implements
		OnClickListener, OnGetDataed, OnItemClickListener {

	public final static int REQUEST_CODE = 998;
	public final static String INTENT_ISCHOOSE = "isChoose";

	private final static String TAG = "PLAN";
	private String[] COURSES = new String[] { "收藏课程", "清除" };
	private String[] REMINDS = new String[] { "每次登陆时弹出提醒框", "我很自觉，不需要提醒" };
	// private String[] REMINDS = new String[] { "每次登陆时弹出提醒框", "添加闹钟，准时提醒我学习",
	// "我很自觉，不需要提醒" };
	private int dialogId;
	private TextView tvCourse;
	private TextView tvRemind;
	private Dialog dialog;
	private int pos = -1;
	private int id = -1;
	private MyVoller voller;
	private Dialog waitDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.color_purple));
		actionBar.setTitle("定制");
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_couser_plan);
		(tvCourse = (TextView) findViewById(R.id.tvCourse))
				.setOnClickListener(this);
		Intent intent = getIntent();
		tvCourse.setText(intent.getStringExtra(MyCollectActivity.INTENT_NAME));
		id = intent.getIntExtra(MyCollectActivity.INTENT_ID, -1);
		(tvRemind = (TextView) findViewById(R.id.tvRemind))
				.setOnClickListener(this);
		findViewById(R.id.btn_yes).setOnClickListener(this);
		voller = new MyVoller(this, this);
		waitDialog = DialogUtil.createProgressDialog(this, "添加计划中...");
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.btn_yes:// 确认
			String title = ((TextView) findViewById(R.id.edtTheme)).getText()
					.toString().trim();
			if (title.equals("")) {
				T.showShort(getActivity(), "请输入标题");
				return;
			}
			if (id == -1) {
				T.showShort(getActivity(), "请选择课程");
				return;
			}
			if (pos == -1) {
				T.showShort(getActivity(), "请选择提醒方式");
				return;
			}
			String strDay = ((TextView) findViewById(R.id.edtCountdown))
					.getText().toString().trim();
			if (strDay == null || strDay.equals("")) {
				T.showShort(getActivity(), "请输入计划完成时间");
				return;
			} else if (strDay.equals("0")) {
				T.showShort(getActivity(), "计划完成时间不能为0");
				return;
			}
			HashMap<String, String> map = new HashMap<String, String>();
			if (L.isDebug) {
				map.put("accesstoken",
						"899489f74a6947ad3e32d960f2df506490e436342d4cb9c1");
			} else {
				map.put("accesstoken",
						SPUtils.getPrefString(getActivity(), SPkey.TOKEN, ""));
			}
			map.put("theme", "" + title);
			map.put("id", "" + this.id);
			map.put("tiptype", "" + pos);
			map.put("plantype", "0");// 无意义
			map.put("plantime",
					""
							+ (System.currentTimeMillis() + 1000 * 60 * 60 * 24
									* Long.parseLong(strDay)));
			voller.urlToJsonObject(TAG, Urls.ADDP_LAN, map, false);
			DialogUtil.show(waitDialog);
			break;

		case R.id.tvCourse:// 课程选择
			dialog = DialogUtil.createItemDialog(this,
					R.drawable.setting_remind_division,
					R.drawable.setting_remind_btn, R.layout.text_view_purple,
					COURSES, this);
			dialog.show();
			dialogId = R.id.tvCourse;
			break;

		case R.id.tvRemind:// 提醒方式
			dialog = DialogUtil.createItemDialog(this,
					R.drawable.setting_remind_division,
					R.drawable.setting_remind_btn, R.layout.text_view_purple,
					REMINDS, this);
			dialog.show();
			dialogId = R.id.tvRemind;
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == PlanActivity.REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				id = data.getIntExtra(MyCollectActivity.INTENT_ID, -1);
				String name = data
						.getStringExtra(MyCollectActivity.INTENT_NAME);
				tvCourse.setText(name);
			} else {
				T.showShort(this, "没有选择课程");
			}
		}
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		String msg = jsonObject.getString("message");
		T.showShort(this, msg);
		EntityPlan plan = new EntityPlan();
		plan.setId(id);
		plan.setRemindType(pos);
		plan.setTheme(((TextView) findViewById(R.id.edtTheme)).getText()
				.toString().trim());
		plan.setStartTime(System.currentTimeMillis());
		plan.setEndTime(System.currentTimeMillis()
				+ 1000
				* 60
				* 60
				* 24
				* Long.parseLong(((TextView) findViewById(R.id.edtCountdown))
						.getText().toString().trim()));
		plan.setPlanid(jsonObject.getLongValue("planid"));
		ArrayList<EntityPlan> datas = CacheDB.getCacheArray(Urls.ALL_PLAN
				+ EntityPlan.TAG, EntityPlan.class);
		if (datas == null) {
			datas = new ArrayList<EntityPlan>();
		}
		datas.add(plan);
		CacheDB.inster(Urls.ALL_PLAN + EntityPlan.TAG,
				"{\"data\":" + JSON.toJSONString(datas) + "}");

		((TextView) findViewById(R.id.edtTheme)).setText("");
		((TextView) findViewById(R.id.edtCountdown)).setText("");
		tvCourse.setText("");
		tvRemind.setText("");
		id = -1;
		pos = -1;
		DialogUtil.disShow(waitDialog);
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		T.showShort(this, message);
		DialogUtil.disShow(waitDialog);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		dialog.dismiss();
		if (dialogId == R.id.tvCourse) {// 选择课程
			if (position == 0) {// 选择收藏课程
				Intent intent = new Intent(this, MyCollectActivity.class);
				intent.putExtra(PlanActivity.INTENT_ISCHOOSE, true);
				startActivityForResult(intent, REQUEST_CODE);
			} else {// 清除
				id = -1;
				tvCourse.setText("");
			}
		} else {// 选择提醒方式
			pos = position;
			tvRemind.setText(REMINDS[position]);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
