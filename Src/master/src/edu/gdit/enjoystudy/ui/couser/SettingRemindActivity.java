package edu.gdit.enjoystudy.ui.couser;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterSettingRemind;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;
import edu.gdit.enjoystudy.utils.IntentUtil;

/**
 * 设置提醒
 * 
 * @author skytoup
 *
 */
public class SettingRemindActivity extends BaseActionBarActivity implements
		OnClickListener, OnItemClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.color_purple));
		actionBar.setTitle("设置提醒");
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_setting_remind);
		findViewById(R.id.btnSettingRemind).setOnClickListener(this);
		ListView listView = (ListView) findViewById(R.id.listView);
		listView.setOnItemClickListener(this);
		listView.setAdapter(new AdapterSettingRemind(getLayoutInflater()));
	}

	@Override
	public void onClick(View v) {
		// 设置提醒
		IntentUtil.startActivity(this, SettingRemindCreateActivity.class);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
