package edu.gdit.enjoystudy.ui.download;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.HttpHandler;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;

import edu.gdit.enjoystudy.entity.EntityDownload;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.utils.FileUtil;

/**
 * 下载服务
 * 
 * @author skytoup路人
 * 
 *         2014-10-25下午7:19:21
 * 
 */
public class DownloadService extends Service {

	public static final String FILTER = "com.EnjoyStudy.DownloadService";
	private HttpUtils httpUtils;
	private ArrayList<EntityDownload> startQueue;// 开始的队列
	private ArrayList<EntityDownload> waitQueue;// 等待的队列
	private HashMap<EntityDownload, HttpHandler<File>> map;
	private ArrayList<EntityDownload> downloading;
	private ArrayList<EntityDownload> downloaded;

	@Override
	public void onCreate() {
		super.onCreate();
		httpUtils = new HttpUtils();
		startQueue = new ArrayList<EntityDownload>();
		waitQueue = new ArrayList<EntityDownload>();
		downloading = CacheDB.getCacheArray(EntityDownload.TAG_DOWNLOAD_ING,
				EntityDownload.class);
		downloaded = CacheDB.getCacheArray(EntityDownload.TAG_DOWNLOAD_ED,
				EntityDownload.class);
		if (downloading == null) {
			downloading = new ArrayList<EntityDownload>();
		}
		if (downloaded == null) {
			downloaded = new ArrayList<EntityDownload>();
		}
		map = new HashMap<EntityDownload, HttpHandler<File>>();
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return new DownloadBind();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return Service.START_STICKY;// 防止被杀
	}

	@Override
	public void onDestroy() {
		CacheDB.inster(EntityDownload.TAG_DOWNLOAD_ING,
				"{\"data\":" + JSON.toJSONString(downloading) + "}");
		CacheDB.inster(EntityDownload.TAG_DOWNLOAD_ED,
				"{\"data\":" + JSON.toJSONString(downloaded) + "}");
		super.onDestroy();
	}

	/**
	 * 下载服务连接
	 * 
	 * @author skytoup路人
	 * 
	 *         2014-10-25下午7:19:44
	 * 
	 */
	public class DownloadBind extends Binder {
		private OnDownload onDownload;

		public OnDownload getOnDownload() {
			return onDownload;
		}

		public void setOnDownload(OnDownload onDownload) {
			this.onDownload = onDownload;
		}

		/**
		 * 是否在下载
		 * 
		 * @param download
		 * @return
		 */
		public boolean getIsDownload(EntityDownload download) {
			if (map.get(download) != null) {
				return true;
			}
			return false;
		}

		/**
		 * 开始下载
		 * 
		 * @param download
		 *            需要下载的文件
		 */
		public void startDown(final EntityDownload download) {

			HttpHandler<File> handler = httpUtils.download(
					download.getUrl(),
					FileUtil.getCacheDir(DownloadService.this) + "/"
							+ download.getFileName(), true, true,
					new RequestCallBack<File>() {
						@Override
						public void onStart() {

						}

						@Override
						public void onLoading(long total, long current,
								boolean isUploading) {
							long speed = current - download.getDownSize();
							download.setDownSize(current);
							download.setSize(total);
							if (onDownload != null) {
								onDownload.onDownload(download, total, current,
										speed);
							}
						}

						@Override
						public void onSuccess(ResponseInfo<File> responseInfo) {
							if (waitQueue.size() != 0) {
								EntityDownload download2 = waitQueue.get(0);
								waitQueue.remove(0);
								startQueue.add(download2);
								map.get(download2).resume();
							}
							downloaded.add(download);
							downloading.remove(download);
							CacheDB.inster(
									EntityDownload.TAG_DOWNLOAD_ING,
									"{\"data\":"
											+ JSON.toJSONString(downloading)
											+ "}");
							CacheDB.inster(
									EntityDownload.TAG_DOWNLOAD_ED,
									"{\"data\":"
											+ JSON.toJSONString(downloaded)
											+ "}");
							if (onDownload != null) {
								onDownload.onSuccess(download);
							}
							startQueue.remove(download);
							map.remove(download);
						}

						@Override
						public void onFailure(HttpException error, String msg) {
							// map.remove(download);
							if (waitQueue.size() != 0) {
								EntityDownload download = waitQueue.get(0);
								waitQueue.remove(0);
								map.get(download).resume();
							}
							if (onDownload != null) {
								onDownload.onFail(download, error, msg);
							}
							startQueue.remove(download);
							map.remove(download);
						}
					});
			if (startQueue.size() < 3) {
				startQueue.add(download);
			} else {
				handler.pause();
				waitQueue.add(download);
			}
			map.put(download, handler);
		}

		/**
		 * 暂停下载
		 * 
		 * @param download
		 *            需要下载的文件
		 */
		public void pauseDown(EntityDownload download) {
			map.get(download).cancel();
			map.remove(download);
			startQueue.remove(download);
			if (!waitQueue.isEmpty()) {
				EntityDownload download2 = waitQueue.get(0);
				waitQueue.remove(0);
				startQueue.add(download2);
				map.get(download2).resume();
			}
		}

		/**
		 * 恢复下载
		 * 
		 * @param download
		 *            需要下载的文件
		 */
		public void resumeDown(EntityDownload download) {
			if (startQueue.size() < 3) {
				map.get(download).resume();
				startQueue.add(download);
			} else {
				waitQueue.add(download);
			}
		}

		/**
		 * 取消下载
		 * 
		 * @param download
		 *            需要下载的文件
		 */
		public void cancleDown(EntityDownload download) {
			pauseDown(download);
			map.get(download).cancel();
			map.remove(download);
			new File(FileUtil.getCacheDir(DownloadService.this) + "/"
					+ download.getFileName()).delete();
			downloaded.add(download);
			downloading.remove(download);
			CacheDB.inster(EntityDownload.TAG_DOWNLOAD_ING,
					"{\"data\":" + JSON.toJSONString(downloading) + "}");
			CacheDB.inster(EntityDownload.TAG_DOWNLOAD_ED,
					"{\"data\":" + JSON.toJSONString(downloaded) + "}");
		}
	}

	/**
	 * 文件下载代理
	 * 
	 * @author skytoup路人
	 * 
	 *         2014-10-25下午9:32:13
	 * 
	 */
	public interface OnDownload {
		/**
		 * 成功
		 * 
		 * @param download
		 */
		public void onSuccess(EntityDownload download);

		/**
		 * 失败
		 * 
		 * @param download
		 * @param error
		 * @param msg
		 */
		public void onFail(EntityDownload download, HttpException error,
				String msg);

		/**
		 * 正在下载
		 * 
		 * @param download
		 * @param total
		 * @param current
		 * @param current
		 */
		public void onDownload(EntityDownload download, long total,
				long current, long speed);
	}
}