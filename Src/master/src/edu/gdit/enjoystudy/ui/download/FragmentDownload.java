/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-8   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.download;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterDownload;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.ui.download.DownloadService.DownloadBind;
import edu.gdit.enjoystudy.utils.AppUtil;
import edu.gdit.enjoystudy.utils.MomentSizeUtil;

/**
 * Description:下载管理
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentDownload extends BaseFragment {

	DownloadBind bind;

	public static FragmentDownload newInstance() {
		return new FragmentDownload();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_download, container,
				false);
		// String downing =
		// "{\"data\":[{\"fileName\":\"1.jpg\",\"url\":\"http://dlsw.baidu.com/sw-search-sp/soft/3a/12350/QQ6.4.12593.0.1412936023.exe\",\"size\":0,\"downSize\":0},{\"fileName\":\"2.jpg\",\"url\":\"http://dlsw.baidu.com/sw-search-sp/soft/3a/12350/QQ6.4.12593.0.1412936023.exe\",\"size\":0,\"downSize\":0},{\"fileName\":\"3.jpg\",\"url\":\"http://dlsw.baidu.com/sw-search-sp/soft/3a/12350/QQ6.4.12593.0.1412936023.exe\",\"size\":0,\"downSize\":0},{\"fileName\":\"4.jpg\",\"url\":\"http://dlsw.baidu.com/sw-search-sp/soft/3a/12350/QQ6.4.12593.0.1412936023.exe\",\"size\":0,\"downSize\":0}]}";
		// CacheDB.inster(EntityDownload.TAG_DOWNLOAD_ING, downing);

		ExpandableListView listView = (ExpandableListView) view
				.findViewById(R.id.expandableListView);
		listView.setGroupIndicator(null);
		final AdapterDownload adapter = new AdapterDownload(inflater, listView);
		listView.setAdapter(adapter);
		Intent intent = new Intent();
		intent.setAction(DownloadService.FILTER);
		if (!AppUtil.serviceIsWorked(getActivity(), DownloadService.FILTER)) {
			getActivity().startService(intent);
		}
		getActivity().bindService(intent, new ServiceConnection() {
			@Override
			public void onServiceDisconnected(ComponentName name) {
			}

			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				bind = (DownloadBind) service;
				adapter.setDownloadBind(bind);
			}
		}, Service.BIND_AUTO_CREATE);

		TextView tvMonent = (TextView) view.findViewById(R.id.tvMoment);
		ProgressBar progressMonent = (ProgressBar) view
				.findViewById(R.id.progressMoment);
		String enable = MomentSizeUtil.getEnableMomentSize(getActivity());
		String totle = MomentSizeUtil.getMomentSize(getActivity());
		tvMonent.setText("手机内存剩余  " + enable + "/" + totle);
		progressMonent.setProgress(MomentSizeUtil.getMonentProgress());
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onShow(ActionBarActivity activity) {
		activity.getSupportActionBar().setBackgroundDrawable(
				activity.getResources().getDrawable(R.drawable.color_red));
	}

}
