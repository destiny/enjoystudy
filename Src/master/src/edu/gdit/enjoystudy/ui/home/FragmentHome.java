/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-8   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.home;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import others.actionbar_pulltorefresh.ActionBarPullToRefresh;
import others.actionbar_pulltorefresh.Options;
import others.actionbar_pulltorefresh.PullToRefreshLayout;
import others.actionbar_pulltorefresh.listeners.OnRefreshListener;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.BaseSliderView.OnSliderClickListener;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterHome;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.config.AppConfig;
import edu.gdit.enjoystudy.entity.EntityBanner;
import edu.gdit.enjoystudy.entity.EntityCourse;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.utils.AppUtil;
import edu.gdit.enjoystudy.utils.IntentUtil;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;
import edu.gdit.enjoystudy.view.AutoViewPager.OnPagerClick;
import edu.gdit.enjoystudy.view.QuickReturnListView;

/**
 * Description: 首页
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentHome extends BaseFragment implements OnClickListener,
		OnPagerClick, OnGetDataed, OnRefreshListener, OnSliderClickListener {

	public final static String INTENT_VIDEO = "video";
	private final static String TAG_BANNER = "banner";
	private final static String TAG_SIGN_IN = "signIn";
	private final static String TAG_VIDEO_ITEM = "videoItem";
	private PullToRefreshLayout refreshLayout;// 下拉刷新
	private Button mBtnCurrentSelect;// 当前选中的按钮
	private int mIndex;// 选中项
	private SliderLayout mAutoViewPager;// 自动轮播条
	private MyVoller mVoller;
	private ArrayList<EntityCourse> mVideos_0;
	private ArrayList<EntityCourse> mVideos_1;
	private ArrayList<EntityCourse> mVideos_2;
	private AdapterHome adapterHome;
	private LinearLayout mQuickReturnView;
	private View mPlaceHolder;
	private View mHeaderView;
	private QuickReturnListView mListView;
	private int mQuickReturnHeight;

	private static final int STATE_ONSCREEN = 0;
	private static final int STATE_OFFSCREEN = 1;
	private static final int STATE_RETURNING = 2;
	private int mState = STATE_ONSCREEN;
	private int mScrollY;
	private int mMinRawY = 0;

	private long mClickTime = 0;

	public static FragmentHome newInstance() {
		return new FragmentHome();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		this.mVoller = new MyVoller(getActivity(), this);
		this.setHasOptionsMenu(true);// 设置显示菜单
		this.mIndex = 0;
		View view = inflater.inflate(R.layout.fragment_home, container, false);
		initReFreshLayout(view);
		mListView = (QuickReturnListView) view.findViewById(R.id.listView);
		mHeaderView = (View) inflater.inflate(R.layout.home_listview_header,
				null);
		mQuickReturnView = (LinearLayout) view.findViewById(R.id.sticky);
		mPlaceHolder = mHeaderView.findViewById(R.id.placeholder);
		mAutoViewPager = (SliderLayout) mHeaderView.findViewById(R.id.slider);
		// mAutoViewPager.setOnPagerClickListener(this);
		mListView.addHeaderView(mHeaderView);
		mListView.setAdapter(adapterHome = new AdapterHome(inflater));
		// 当全部视图构建好,就测量Lisiview的总长度.但是这里会莫名导致轮播图片不显示.
		// view.getViewTreeObserver().addOnGlobalLayoutListener(
		// new OnGlobalLayoutListener() {
		// @Override
		// public void onGlobalLayout() {
		// mQuickReturnHeight = mQuickReturnView.getHeight();
		// mListView.computeScrollY();
		// }
		// });
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		startComputeScrollY(view);

		loadData();

		(this.mBtnCurrentSelect = (Button) mQuickReturnView
				.findViewById(R.id.btnNew)).setOnClickListener(this);
		mBtnCurrentSelect.setSelected(true);
		((Button) mQuickReturnView.findViewById(R.id.btnHot))
				.setOnClickListener(this);
		((Button) mQuickReturnView.findViewById(R.id.btnRecommend))
				.setOnClickListener(this);

		mListView.setOnScrollListener(onScrollListener);
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				EntityCourse video = null;
				if(AppConfig.isDebug){
					video=new EntityCourse();
					video.setAnthorname("波老师");
					video.setCollectnum(123);
					video.setId(123);
					video.setCoursedynamic("更新中");
					video.setImageurl("http://cococ.qiniudn.com/image/Bantou.jpg");
					video.setIntroduction("这是21世纪最好的课程");
					video.setName("波粒二性");
					video.setNew(true);
					video.setSeesum("332");
					video.setTime("2015.01.22");
					AppUtil.openCourse(getActivity(), video);
					return;
				}
				switch (mIndex) {
				case 0:
					video = mVideos_0.get(position - 1);
					break;
				case 1:
					video = mVideos_1.get(position - 1);
					break;
				case 2:
					video = mVideos_2.get(position - 1);
					break;
				}
				AppUtil.openCourse(getActivity(), video);
			}
		});
	}

	/**
	 * 描述：采用setViewTreeObserver().addOnGlobalLayoutListener
	 * 的方式会导致轮播图片不显示.于是搞了这个方法,300是保守估计
	 * 
	 * @param view
	 * @return void
	 */
	private void startComputeScrollY(View view) {
		if (view != null) {
			view.postDelayed(new Runnable() {

				@Override
				public void run() {
					mQuickReturnHeight = mQuickReturnView.getHeight();
					mListView.computeScrollY();

				}
			}, 300);
		}
	}

	/**
	 * 描述：初始化下拉刷新
	 * 
	 * @param view
	 * @return void
	 */
	private void initReFreshLayout(View view) {
		refreshLayout = (PullToRefreshLayout) view
				.findViewById(R.id.swipeRefreshLayout);

		// Now setup the PullToRefreshLayout
		ActionBarPullToRefresh
				.from(getActivity())
				.options(
						Options.create()
								// Here we make the refresh scroll distance to
								// 75% of the refreshable
								// view's height
								.scrollDistance(.50f)
								.headerLayout(R.layout.default_header).build())
				// Mark All Children as pullable
				.allChildrenArePullable()

				// Set a OnRefreshListener
				.listener(this)
				// Finally commit the setup to our PullToRefreshLayout
				.setup(refreshLayout);
	}

	/**
	 * 描述：
	 * 
	 * @return void
	 */
	private void initSliderAndSetData(ArrayList<EntityBanner> banners) {
		mAutoViewPager.removeAllSliders();
		Iterator<EntityBanner> iterator = banners.iterator();
		while (iterator.hasNext()) {
			EntityBanner banner = iterator.next();
			TextSliderView textSliderView = new TextSliderView(getActivity());
			// initialize a SliderLayout
			textSliderView.description(banner.getTitle())
					.image(banner.getPath())
					.setScaleType(BaseSliderView.ScaleType.Fit)
					.setOnSliderClickListener(this);
			// add your extra information
			textSliderView.getBundle().putInt("extra", banner.getType());
			textSliderView.getBundle().putString("data", banner.getData() + "");
			mAutoViewPager.addSlider(textSliderView);
		}

		mAutoViewPager.setPresetTransformer(SliderLayout.Transformer.Tablet);
		mAutoViewPager
				.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
		mAutoViewPager.setCustomAnimation(new DescriptionAnimation());
		mAutoViewPager.setDuration(4000);
	}

	@Override
	public void onSliderClick(BaseSliderView slider) {
		// Toast.makeText(getActivity(), slider.getBundle().get("extra") + "",
		// Toast.LENGTH_SHORT).show();
		/*
		 * 
		 * type=0时是视频广告，data是放他的id type=2时是广告数据，data是存放他的广告地址
		 */
		int type = slider.getBundle().getInt("extra");
		String data = slider.getBundle().getString("data");
		// T.showLong(getActivity(), type);
		if (type == 0) {
			AppUtil.openCourse(getActivity(), Integer.parseInt(data));
		} else {
			AppUtil.openWebView(getActivity(), data);

		}
	}

	@Override
	public void onPause() {
		super.onPause();

		// this.mAutoViewPager.pauseAutoPlay();
	}

	public void onDestroyView() {
		this.mVoller.stopAllRequest();
	};

	@Override
	public void onShow(ActionBarActivity activity) {
		activity.getSupportActionBar().setBackgroundDrawable(
				activity.getResources().getDrawable(R.drawable.color_red));
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		menu.add(0, 0, 0, "").setIcon(R.drawable.home_actionbar_signin)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		menu.add(0, 1, 0, "").setIcon(R.drawable.home_actionbar_search)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:// 签到
			String tk = SPUtils.getPrefString(getActivity(), SPkey.TOKEN, "");
			if (tk.equals("")) {
				T.showShort(getActivity(), "请先登录");
			} else {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("accesstoken", tk);
				this.mVoller.urlToJsonObject(TAG_SIGN_IN, Urls.SIGN_IN, map,
						false);
			}
			break;

		case 1:// 搜索
			IntentUtil.startActivity(getActivity(), SearchActivity.class);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@SuppressLint("UseValueOf")
	@Override
	public void onClick(View v) {
		long time = System.currentTimeMillis();
		if (time - mClickTime < 50) {
			return;
		} else {
			mClickTime = time;
		}
		if (v == this.mBtnCurrentSelect) {
			return;
		}
		int id = v.getId();
		mBtnCurrentSelect.setSelected(false);
		mBtnCurrentSelect = (Button) v;
		mBtnCurrentSelect.setSelected(true);

		ArrayList<EntityCourse> videos = null;
		String type = "0";
		switch (id) {
		case R.id.btnNew:// 最新
			type = "0";
			videos = mVideos_0;
			break;
		case R.id.btnHot:// 最热
			type = "1";
			videos = mVideos_1;
			break;
		case R.id.btnRecommend:// 推荐
			type = "2";
			videos = mVideos_2;
			break;
		}
		this.mIndex = new Integer(type);

		// 数据为空，则获取网络上的数据
		if (videos == null) {
			this.getVideosFromNetWork(type);
		} else {
			adapterHome.setDatas(videos);
			startComputeScrollY(mListView);
		}
	}

	@Override
	public void onPagerClick(int index, EntityBanner banner) {
		T.showShort(getActivity(), "" + banner.getData());
	}

	// 加载数据
	private void loadData() {
		ArrayList<EntityBanner> bannersFromCache = loadDatefromCache();
		if (mVideos_0 != null) {
			if (adapterHome == null) {
				adapterHome = new AdapterHome(getActivity().getLayoutInflater());
				mListView.setAdapter(adapterHome);
			}
			adapterHome.setDatas(mVideos_0);
			startComputeScrollY(mListView);
		}
		if(AppConfig.isDebug){
			ArrayList<EntityBanner> banners= new ArrayList<EntityBanner>();
			EntityBanner b1=new EntityBanner();
			b1.setData("图一");
			b1.setId(1);
			b1.setPath("http://cococ.qiniudn.com/image/qiniang/01.jpg");
			b1.setType(0);
			EntityBanner b2=new EntityBanner();
			b2.setData("图2");
			b2.setId(2);
			b2.setPath("http://cococ.qiniudn.com/image/qiniang/02.jpg");
			b2.setType(0);
			EntityBanner b3=new EntityBanner();
			b2.setData("图3");
			b2.setId(3);
			b2.setPath("http://cococ.qiniudn.com/image/qiniang/03.jpg");
			b2.setType(0);
			banners.add(b1);
			banners.add(b2);
			banners.add(b3);
			initSliderAndSetData(banners);

		}
		if (bannersFromCache != null) {
			
			initSliderAndSetData(bannersFromCache);
			// mAutoViewPager.setBanners(bannersFromCache);
		}
		this.getBannersFromNetwork();
		this.getVideosFromNetWork("0");
	}

	/**
	 * 描述：从缓存加载数据
	 * 
	 * @return
	 * @return ArrayList<EntityBanner>
	 */
	private ArrayList<EntityBanner> loadDatefromCache() {
		ArrayList<EntityBanner> banners = CacheDB.getCacheArray(Urls.BANNER
				+ TAG_BANNER, EntityBanner.class);
		mVideos_0 = CacheDB.getCacheArray(Urls.VIDEO_INFO + TAG_VIDEO_ITEM
				+ "0", EntityCourse.class);
		mVideos_1 = CacheDB.getCacheArray(Urls.VIDEO_INFO + TAG_VIDEO_ITEM
				+ "1", EntityCourse.class);
		mVideos_2 = CacheDB.getCacheArray(Urls.VIDEO_INFO + TAG_VIDEO_ITEM
				+ "2", EntityCourse.class);
		return banners;
	}

	// 网络获取广告条
	private void getBannersFromNetwork() {
		this.mVoller.urlToJsonObject(TAG_BANNER, Urls.BANNER, null, true);
	}

	// 获取视屏列表
	private void getVideosFromNetWork(String type) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("type", type);
		mVoller.urlToJsonObject(TAG_VIDEO_ITEM + type, Urls.VIDEO_INFO, map,
				true);
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		if (tag.equals(TAG_BANNER)) {// 获取广告条
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			ArrayList<EntityBanner> banners = (ArrayList<EntityBanner>) JSON
					.parseArray(jsonArray.toJSONString(), EntityBanner.class);
			initSliderAndSetData(banners);
			// this.mAutoViewPager.setBanners(banners, true);
		} else if (tag.equals(TAG_SIGN_IN)) {// 签到
			T.showShort(getActivity(), "签到成功");
		} else if (tag.startsWith(TAG_VIDEO_ITEM)) {
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			ArrayList<EntityCourse> videos = (ArrayList<EntityCourse>) JSON
					.parseArray(jsonArray.toJSONString(), EntityCourse.class);
			if (tag.endsWith("0")) {// 获取课程列表,最新
				mVideos_0 = videos;
			} else if (tag.endsWith("1")) {// 获取课程列表,最热
				mVideos_1 = videos;
			} else if (tag.endsWith("2")) {// 获取课程列表,推荐
				mVideos_2 = videos;
			}
			// 获取到的数据为当前选中项时，设置Adapter
			if (tag.endsWith(mIndex + "")) {
				adapterHome.setDatas(videos);
				startComputeScrollY(mListView);
			}
		}

		if (requestCount == 0) {
			refreshLayout.setRefreshing(false);
		}
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		T.showShort(getActivity(), message);
		if (requestCount == 0) {
			refreshLayout.setRefreshing(false);
		}
	}

	int scrollY;
	AbsListView.OnScrollListener onScrollListener = new OnScrollListener() {

		private int mScrollY;

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {

		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {

			mScrollY = 0;
			int translationY = 0;

			if (mListView.scrollYIsComputed()) {
				mScrollY = mListView.getComputedScrollY();// 该视图内容相当于视图起始坐标的Y偏移量
			}

			// 做差，小于零表示屏幕已经滑动到了mPlaceHolder的位置，|rawY|就是mPlaceHolder与mScrollY的相对位移
			int rawY = mPlaceHolder.getTop() - mScrollY;

			// 状态机，首次进入的时候是 STATE_ONSCREEN
			switch (mState) {

			case STATE_OFFSCREEN:
				if (rawY <= mMinRawY) {
					mMinRawY = rawY;// 负数，更新最小值，当rawY
									// 变大说明正在回滚，回滚的时候mMinRawY马上又会被修改成临界值
				} else {
					mState = STATE_RETURNING;
				}
				translationY = rawY;
				break;

			case STATE_ONSCREEN:
				if (rawY < -mQuickReturnHeight) {// QuickReturn已超出 屏幕
					mState = STATE_OFFSCREEN;// 更新状态，同时得到超出屏幕时rawY的临界值
					mMinRawY = rawY;
				}
				translationY = rawY;
				break;

			case STATE_RETURNING:// 第一次进来 就是 0- mQuickReturnHeight。 rawY -
									// mMinRawY 做差，在新的位置得出新的位移
				translationY = (rawY - mMinRawY) - mQuickReturnHeight;
				if (translationY > 0) {// 大于零说明 QuickReturn
										// 已经完整出现，相对自己本身的位置是0就定住不动了。
					translationY = 0;
					mMinRawY = rawY - mQuickReturnHeight;// 要让mMinRawY 也更新。
				}

				if (rawY > 0) {//
					mState = STATE_ONSCREEN;
					translationY = rawY;
				}

				if (translationY < -mQuickReturnHeight) {
					mState = STATE_OFFSCREEN;
					mMinRawY = rawY;
				}
				break;
			}

			mQuickReturnView.setTranslationY(translationY);

		}

	};

	@Override
	public void onRefreshStarted(View view) {
		getBannersFromNetwork();
		getVideosFromNetWork("0");
		getVideosFromNetWork("1");
		getVideosFromNetWork("2");
	}

}