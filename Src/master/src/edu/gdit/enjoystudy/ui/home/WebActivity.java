/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-10-31   Create
 * 
 **/
package edu.gdit.enjoystudy.ui.home;

import android.os.Bundle;
import android.webkit.WebView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;

/**
 * Description:
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class WebActivity extends BaseActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web);
		getSupportActionBar().setBackgroundDrawable(
				getResources().getDrawable(R.drawable.color_red));
		String url = getIntent().getStringExtra("url");
		WebView webView = (WebView) findViewById(R.id.web);
		 webView.loadUrl(url);  
	}

}
