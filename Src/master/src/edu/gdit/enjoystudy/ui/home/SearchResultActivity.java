package edu.gdit.enjoystudy.ui.home;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterHome;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;
import edu.gdit.enjoystudy.entity.EntityCourse;
import edu.gdit.enjoystudy.utils.AppUtil;

public class SearchResultActivity extends BaseActionBarActivity implements
		OnItemClickListener {

	private ArrayList<EntityCourse> datas;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.color_red));
		actionBar.setTitle(getIntent().getStringExtra(
				SearchActivity.INTENT_TITLE));
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_couser_my_collect);
		ListView listView = (ListView) findViewById(R.id.listView);
		listView.setOnItemClickListener(this);
		datas = (ArrayList<EntityCourse>) getIntent().getSerializableExtra(
				SearchActivity.INTENT_DATAS);
		listView.setAdapter(new AdapterHome(getLayoutInflater(), datas));
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		AppUtil.openCourse(this, datas.get(position));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
