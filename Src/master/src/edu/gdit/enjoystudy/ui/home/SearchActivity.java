package edu.gdit.enjoystudy.ui.home;

import java.util.ArrayList;
import java.util.HashMap;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.adapter.AdapterSearch;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;
import edu.gdit.enjoystudy.entity.EntityCourse;
import edu.gdit.enjoystudy.entity.EntityHotSearch;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.utils.DialogUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.utils.T;
import edu.gdit.enjoystudy.view.FlowLayout;

public class SearchActivity extends BaseActionBarActivity implements
		OnItemClickListener, OnGetDataed, FlowLayout.OnItemClickListener,
		OnClickListener {

	public final static String INTENT_TITLE = "title";
	public final static String INTENT_DATAS = "datas";

	private final static String TAG_HOT_SEARCH = "hotSearch";
	private final static String TAG_SEARCH = "search";
	private FlowLayout layout;
	private MyVoller voller;
	private EditText editText;
	private Dialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.color_red));
		actionBar.setTitle("");
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowCustomEnabled(false);
		actionBar.setIcon(R.drawable.btn_back);
		setContentView(R.layout.activity_search);
		voller = new MyVoller(this, this);
		GridView gridView = (GridView) findViewById(R.id.gridView);
		if (L.isDebug) {
			ArrayList<String> list = new ArrayList<String>();
			for (int i = 0; i != 6; i++) {
				list.add("计算机");
			}
			gridView.setAdapter(new AdapterSearch(getLayoutInflater(), list));
		} else {
			gridView.setAdapter(new AdapterSearch(getLayoutInflater()));
		}
		dialog = DialogUtil.createProgressDialog(this, "搜索中...");

		layout = (FlowLayout) findViewById(R.id.flowLayout);
		layout.setOnItemClickListener(this);
		voller.urlToJsonObject(TAG_HOT_SEARCH, Urls.HOT_SEARCH, null, false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.search_menu, menu);
		View view = menu.findItem(R.id.search).getActionView();
		editText = ((EditText) view.findViewById(R.id.view_search));
		view.findViewById(R.id.imgSearch).setOnClickListener(this);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		if (tag.equals(TAG_HOT_SEARCH)) {
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			ArrayList<EntityHotSearch> datas = (ArrayList<EntityHotSearch>) JSON
					.parseArray(jsonArray.toJSONString(), EntityHotSearch.class);
			layout.removeAllViews();
			layout.addData(datas);
		} else if (tag.equals(TAG_SEARCH)) {
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			ArrayList<EntityCourse> datas = (ArrayList<EntityCourse>) JSON
					.parseArray(jsonArray.toJSONString(), EntityCourse.class);
			Intent intent = new Intent(this, SearchResultActivity.class);
			intent.putExtra(INTENT_DATAS, datas);
			intent.putExtra(INTENT_TITLE, editText.getText().toString().trim());
			startActivity(intent);
		}
		DialogUtil.disShow(dialog);
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		T.showShort(this, message);
		DialogUtil.disShow(dialog);
	}

	@Override
	public void onitemclick(int position, TextView textView) {
		editText.setText(textView.getText());
	}

	@Override
	public void onClick(View v) {
		// 搜索
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("key", editText.getText().toString().trim());
		voller.urlToJsonObject(TAG_SEARCH, Urls.SEARCH, map, false);
	}

}
