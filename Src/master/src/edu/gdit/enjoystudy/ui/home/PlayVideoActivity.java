package edu.gdit.enjoystudy.ui.home;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.MediaPlayer.OnCompletionListener;
import io.vov.vitamio.MediaPlayer.OnInfoListener;
import io.vov.vitamio.MediaPlayer.OnPreparedListener;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;
import android.content.Intent;
import android.view.Window;
import android.view.WindowManager;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;
import edu.gdit.enjoystudy.utils.FileUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.utils.T;

public class PlayVideoActivity extends BaseActionBarActivity implements
		OnPreparedListener, OnCompletionListener, OnInfoListener {

	private VideoView videoView;
	private long seekTime = 0;
	public final static String INTENT_VIDEO_URL = "videoUrl";
	public final static String INTENT_SEEK_TO = "seekTo";

	protected void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 全屏
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_play_video);
		Intent intent = getIntent();
		videoView = (VideoView) findViewById(R.id.videoView);
		// videoView.setVideoLayout(VideoView.VIDEO_LAYOUT_SCALE, 0);
		videoView
				.setVideoPath(L.isDebug ? "http://zuiaixue.jd-app.com/videos/android.mp4?type=0&accesstoken=2f6f68106220d9a2449b9b49eead15b2"
						: intent.getStringExtra(INTENT_VIDEO_URL));
		videoView.seekTo(intent.getLongExtra(INTENT_SEEK_TO, 0));
		videoView.setBufferSize(1024 * 1);// 1M缓存
		MediaController controller = new MediaController(this);
		videoView.setMediaController(controller);
		videoView.requestFocus();
		videoView.setOnPreparedListener(this);
		videoView.setOnCompletionListener(this);
		videoView.setOnInfoListener(this);
	}

	@Override
	public void onResume() {
		super.onResume();
		videoView.seekTo(seekTime);
	}

	@Override
	public void onPause() {
		super.onPause();
		seekTime = videoView.getCurrentPosition();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		videoView.stopPlayback();
	}

	@Override
	public void onPrepared(MediaPlayer mp) {// 每次缓存好开始播放时
		mp.setPlaybackSpeed(1.0f);
		mp.setCacheDirectory(FileUtil.getCacheDir(this));
		mp.setUseCache(true);
		// videoView.setVideoLayout(VideoView.VIDEO_LAYOUT_SCALE, 0);
	};

	@Override
	public void onCompletion(MediaPlayer mp) {
		seekTime = 0;
		mp.seekTo(seekTime);
	}

	@Override
	public boolean onInfo(MediaPlayer mp, int what, int extra) {
		if (what == MediaPlayer.MEDIA_INFO_BUFFERING_START) {// 开始缓冲
			T.showShort(this, "缓冲中...");
		} else if (what == MediaPlayer.MEDIA_INFO_BUFFERING_END) {// 缓冲结束
			mp.start();
		}
		return false;
	}

}
