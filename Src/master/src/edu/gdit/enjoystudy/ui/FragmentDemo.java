/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-5   Create
 * 
 **/
package edu.gdit.enjoystudy.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import edu.gdit.enjoystudy.LoginActivity;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.utils.IntentUtil;

/**
 * Description: 用于测试和写例子
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class FragmentDemo extends BaseFragment implements OnClickListener {

	public FragmentDemo() {
		// Empty constructor required for fragment subclasses
	}

	public static FragmentDemo newInstance() {
		return new FragmentDemo();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_demo, container,
				false);
		// ((ImageView)
		// rootView.findViewById(R.id.image)).setImageResource(imageId);
		getActivity().setTitle("demo");
		Button button1 = (Button) rootView.findViewById(R.id.test_universal_1);
		Button button2 = (Button) rootView.findViewById(R.id.test_universal_2);
		Button button3 = (Button) rootView.findViewById(R.id.test_universal_3);
		button1.setOnClickListener(this);
		button2.setOnClickListener(this);
		button3.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onShow(ActionBarActivity activity) {

	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.test_universal_1) {
			IntentUtil.startActivity(getActivity(), LoginActivity.class);
		} else if (id == R.id.test_universal_2) {
			// IntentUtil.startActivity(getActivity(),
			// ThemeVideoActivity.class);
		} else if (id == R.id.test_universal_3) {

		}
	}
}
