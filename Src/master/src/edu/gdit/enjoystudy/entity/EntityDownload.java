package edu.gdit.enjoystudy.entity;


/**
 * 下载的实体类
 * 
 * @author skytoup路人
 * 
 *         2014-10-25下午7:29:01
 * 
 */
public class EntityDownload {

	public static final String TAG_DOWNLOAD_ING = "entityDownloading";// 正在下载
	public static final String TAG_DOWNLOAD_ED = "entityDownloaded";// 已完成下载
	private String fileName;// 文件名
	private String url;// 下载地址
	private long size = 0;// 文件大小
	private long downSize = 0;// 已下载的大小

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getDownSize() {
		return downSize;
	}

	public void setDownSize(long downSize) {
		this.downSize = downSize;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public boolean equals(Object o) {
		EntityDownload download = (EntityDownload) o;
		return download.getFileName().equals(fileName)
				&& download.getUrl().equals(url);
	}
}
