package edu.gdit.enjoystudy.entity;

/**
 * 广告条
 * 
 * @author skytoup
 *
 */
public class EntityBanner {

	private String data;
	private int id;
	private String path;
	private String title="最爱学";
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	private int type;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
