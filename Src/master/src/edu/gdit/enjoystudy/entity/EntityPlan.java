package edu.gdit.enjoystudy.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 计划
 * 
 * @author skytoup路人
 * 
 *         2014-10-27下午8:22:01
 * 
 */
public class EntityPlan {

	public final static String TAG = "entityPlan";

	private String theme;// 主题
	private int id;// 课程id
	private int remindType;// 提醒类型
	private long startTime;// 开始时间
	private long endTime;// 结束时间
	private long planid;// 计划id

	/**
	 * 获取开始时间yyyy-MM-dd
	 * 
	 * @return
	 */
	public String getStartDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(new Date(startTime));
	}

	/**
	 * 获取剩余天数(天)
	 * 
	 * @return
	 */
	public String getCountDown() {
		long d = (endTime - System.currentTimeMillis()) / (1000 * 60 * 60 * 24);
		return d + "";
	}

	public long getPlanid() {
		return planid;
	}

	public void setPlanid(long planid) {
		this.planid = planid;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRemindType() {
		return remindType;
	}

	public void setRemindType(int remindType) {
		this.remindType = remindType;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

}
