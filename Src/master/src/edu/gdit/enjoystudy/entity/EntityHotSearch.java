package edu.gdit.enjoystudy.entity;

/**
 * 热门搜索
 * 
 * @author skytoup路人
 * 
 *         2014-10-30上午10:59:36
 * 
 */
public class EntityHotSearch {
	private int id;
	private String text;
	private int times;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getTimes() {
		return times;
	}

	public void setTimes(int times) {
		this.times = times;
	}

}
