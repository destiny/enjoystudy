/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-10-29   Create
 * 
 **/
package edu.gdit.enjoystudy.entity;

import java.util.ArrayList;

/**
 * Description: 章节
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class EntitySections {
	private int id;
	private String name;
	private ArrayList<EntityVDO> videos;
	public ArrayList<EntityVDO> getVideos() {
		return videos;
	}


	public void setVideos(ArrayList<EntityVDO> videos) {
		this.videos = videos;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
