package edu.gdit.enjoystudy.entity;

import java.io.Serializable;

/**
 * 视频Item
 * 
 * @author skytoup
 *
 */
public class EntityCourse implements Serializable{

	private int id;// 视频id
	private String anthorname;// 作者名字
	private int collectnum;// 收藏数
	private String coursedynamic;// 课程动态
	private String imageurl;// 影片图片地址
	private String introduction;// 介绍
	private String name;// 视频名字
	private String seesum;// 观看数
	private String time;// 时间
	private boolean isNew = false;// 是否显示New标签

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAnthorname() {
		return anthorname;
	}

	public void setAnthorname(String anthorname) {
		this.anthorname = anthorname;
	}

	public int getCollectnum() {
		return collectnum;
	}

	public void setCollectnum(int collectnum) {
		this.collectnum = collectnum;
	}

	public String getCoursedynamic() {
		return coursedynamic;
	}

	public void setCoursedynamic(String coursedynamic) {
		this.coursedynamic = coursedynamic;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSeesum() {
		return seesum;
	}

	public void setSeesum(String seesum) {
		this.seesum = seesum;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
