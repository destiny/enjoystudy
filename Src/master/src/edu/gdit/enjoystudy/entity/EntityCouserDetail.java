/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-10-29   Create
 * 
 **/
package edu.gdit.enjoystudy.entity;

import java.util.ArrayList;

import android.R.integer;

/**
 * Description:
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class EntityCouserDetail {

	private int teacherid = 137;
	
	
	public int getTeacherid() {
		return teacherid;
	}

	public void setTeacherid(int teacherid) {
		this.teacherid = teacherid;
	}

	private String Content;
	
	private int collectnum;
	
	private int commentsum;
	
	private String courseintroduction;
	
	private String coursename;
	
	private String head;
	
	private ArrayList<EntitySections> sections;
	
	private int seesum;
	
	private int tlevel;
	
	private String videointroduction;

	private String userintroduction;

	public String getUserintroduction() {
		return userintroduction;
	}

	public void setUserintroduction(String userintroduction) {
		this.userintroduction = userintroduction;
	}

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	public int getCollectnum() {
		return collectnum;
	}

	public void setCollectnum(int collectnum) {
		this.collectnum = collectnum;
	}

	public int getCommentsum() {
		return commentsum;
	}

	public void setCommentsum(int commentsum) {
		this.commentsum = commentsum;
	}

	public String getCourseintroduction() {
		return courseintroduction;
	}

	public void setCourseintroduction(String courseintroduction) {
		this.courseintroduction = courseintroduction;
	}

	public String getCoursename() {
		return coursename;
	}

	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public ArrayList<EntitySections> getSections() {
		return sections;
	}

	public void setSections(ArrayList<EntitySections> sections) {
		this.sections = sections;
	}

	public int getSeesum() {
		return seesum;
	}

	public void setSeesum(int seesum) {
		this.seesum = seesum;
	}

	public int getTlevel() {
		return tlevel;
	}

	public void setTlevel(int tlevel) {
		this.tlevel = tlevel;
	}

	public String getVideointroduction() {
		return videointroduction;
	}

	public void setVideointroduction(String videointroduction) {
		this.videointroduction = videointroduction;
	}

}
