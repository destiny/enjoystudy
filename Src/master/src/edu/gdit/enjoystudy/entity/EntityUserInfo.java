package edu.gdit.enjoystudy.entity;

/**
 * 个人信息
 * 
 * @author skytoup路人
 * 
 *         2014-10-29上午10:29:25
 * 
 */
public class EntityUserInfo {
	private int affsum;
	private int age;// 年龄
	private int collectcourse;// 收藏课程数
	private int commentsum;// 评论数
	private String content;// 心情
	private int donecourse;// 完成课程数
	private int dynamicsum;// 动态数
	private int fanssum;// 粉丝数
	private int medalsum;// 勋章数
	private String pic;// 头像
	private int sendcommentsum;// 发送评论数
	private int sex;// 性别
	private int signincount;// 签到次数
	private int userid;// 用户id
	private String username;// 用户名

	public int getAffsum() {
		return affsum;
	}

	public void setAffsum(int affsum) {
		this.affsum = affsum;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getCollectcourse() {
		return collectcourse;
	}

	public void setCollectcourse(int collectcourse) {
		this.collectcourse = collectcourse;
	}

	public int getCommentsum() {
		return commentsum;
	}

	public void setCommentsum(int commentsum) {
		this.commentsum = commentsum;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getDonecourse() {
		return donecourse;
	}

	public void setDonecourse(int donecourse) {
		this.donecourse = donecourse;
	}

	public int getDynamicsum() {
		return dynamicsum;
	}

	public void setDynamicsum(int dynamicsum) {
		this.dynamicsum = dynamicsum;
	}

	public int getFanssum() {
		return fanssum;
	}

	public void setFanssum(int fanssum) {
		this.fanssum = fanssum;
	}

	public int getMedalsum() {
		return medalsum;
	}

	public void setMedalsum(int medalsum) {
		this.medalsum = medalsum;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public int getSendcommentsum() {
		return sendcommentsum;
	}

	public void setSendcommentsum(int sendcommentsum) {
		this.sendcommentsum = sendcommentsum;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public int getSignincount() {
		return signincount;
	}

	public void setSignincount(int signincount) {
		this.signincount = signincount;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
