package edu.gdit.enjoystudy.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * 好友信息
 * 
 * @author skytoup路人
 * 
 *         2014-10-30下午7:38:47
 * 
 */
public class EntityFriend {

	private int affsum;
	private int age;
	private int collectcourse;
	private int commentsum;
	private String content;
	private int donecourse;
	private int dynamicsum;
	private String email;
	private int fanssum;
	private String introduction;
	private int isaffent;
	private String logintime;
	private int medalsum;
	private String nick;
	private String phone;
	private String pic;
	private String regtime;
	private int sex;
	private int signincount;
	private String truename;
	private int userid;
	private String username;

	public long getUserYear() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long y = 0;
		try {
			y = (format.parse(logintime).getTime() - format.parse(regtime)
					.getTime()) / (60 * 60 * 24 * 365);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return y;
	}

	public int getAffsum() {
		return affsum;
	}

	public void setAffsum(int affsum) {
		this.affsum = affsum;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getCollectcourse() {
		return collectcourse;
	}

	public void setCollectcourse(int collectcourse) {
		this.collectcourse = collectcourse;
	}

	public int getCommentsum() {
		return commentsum;
	}

	public void setCommentsum(int commentsum) {
		this.commentsum = commentsum;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getDonecourse() {
		return donecourse;
	}

	public void setDonecourse(int donecourse) {
		this.donecourse = donecourse;
	}

	public int getDynamicsum() {
		return dynamicsum;
	}

	public void setDynamicsum(int dynamicsum) {
		this.dynamicsum = dynamicsum;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getFanssum() {
		return fanssum;
	}

	public void setFanssum(int fanssum) {
		this.fanssum = fanssum;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public int getIsaffent() {
		return isaffent;
	}

	public void setIsaffent(int isaffent) {
		this.isaffent = isaffent;
	}

	public String getLogintime() {
		return logintime;
	}

	public void setLogintime(String logintime) {
		this.logintime = logintime;
	}

	public int getMedalsum() {
		return medalsum;
	}

	public void setMedalsum(int medalsum) {
		this.medalsum = medalsum;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getRegtime() {
		return regtime;
	}

	public void setRegtime(String regtime) {
		this.regtime = regtime;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public int getSignincount() {
		return signincount;
	}

	public void setSignincount(int signincount) {
		this.signincount = signincount;
	}

	public String getTruename() {
		return truename;
	}

	public void setTruename(String truename) {
		this.truename = truename;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
