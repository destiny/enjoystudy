package edu.gdit.enjoystudy.entity;

import java.util.ArrayList;

/**
 * 今日任务
 * 
 * @author skytoup路人
 * 
 *         2014-10-27上午11:09:49
 * 
 */
public class EntityTask {
	private int countdown;// 倒计时
	private float progress;// 进度
	private String starttime;// 开始时间
	private ArrayList<EntityTodayTask> todaytask;// 任务列表

	public int getCountdown() {
		return countdown;
	}

	public void setCountdown(int countdown) {
		this.countdown = countdown;
	}

	public float getProgress() {
		return progress;
	}

	public void setProgress(float progress) {
		this.progress = progress;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public ArrayList<EntityTodayTask> getTodaytask() {
		return todaytask;
	}

	public void setTodaytask(ArrayList<EntityTodayTask> todaytask) {
		this.todaytask = todaytask;
	}
}
