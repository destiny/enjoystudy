package edu.gdit.enjoystudy.entity;

import java.io.Serializable;
import java.util.Date;

import edu.gdit.enjoystudy.utils.DateUtil;

/**
 * 推送消息实体类
 * 
 * @author ping
 * @create 2014-6-12 下午6:33:10
 */
public class PushMsg implements Serializable {
	private int id;
	private String time;
	private boolean isRead;
	
	private int type;
	private String data;
	private String content;

	/**
	 * 获取isRead
	 * @return the isRead
	 */
	public boolean isRead() {
		return isRead;
	}

	/**
	 * 设置isRead
	 * @param isRead
	 */
	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	public PushMsg() {
		this.id = DateUtil.getimeid();
		this.time = DateUtil.dateToStr(new Date());
	}

	public PushMsg(String title, String content) {
		this.id = DateUtil.getimeid();
		this.content = content;
		this.content = content;
		this.time = DateUtil.dateToStr(new Date());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
