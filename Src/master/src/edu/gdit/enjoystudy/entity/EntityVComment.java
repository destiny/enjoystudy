package edu.gdit.enjoystudy.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 视频评论数据实体类
 * @author ping
 *
 */
public class EntityVComment implements Serializable {
	private String content;
	private int id;
	private int userid;
	private int videoid;
	private String time;
	private ArrayList<EntityVCommentSub> subdata;
	private String head;
	private String username;
	
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public int getVideoid() {
		return videoid;
	}
	public void setVideoid(int videoid) {
		this.videoid = videoid;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public ArrayList<EntityVCommentSub> getSubdata() {
		return subdata;
	}
	public void setSubdata(ArrayList<EntityVCommentSub> subdata) {
		this.subdata = subdata;
	}
	
}
