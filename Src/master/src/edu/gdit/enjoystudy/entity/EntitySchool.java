package edu.gdit.enjoystudy.entity;

/**
 * 学校
 * 
 * @author skytoup
 *
 */
public class EntitySchool {

	private int schoolid;// 学校id
	private String schoolname;// 学校名称

	public int getSchoolid() {
		return schoolid;
	}

	public void setSchoolid(int schoolid) {
		this.schoolid = schoolid;
	}

	public String getSchoolname() {
		return schoolname;
	}

	public void setSchoolname(String schoolname) {
		this.schoolname = schoolname;
	}

}
