package edu.gdit.enjoystudy.entity;

/**
 * 还需完成任务
 * 
 * @author skytoup路人
 * 
 *         2014-10-27上午11:09:38
 * 
 */
public class EntityTodayTask {
	private String name;// 名称
	private String pic;// 图片地址
	private float progress;// 进度

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public float getProgress() {
		return progress;
	}

	public void setProgress(float progress) {
		this.progress = progress;
	}
}
