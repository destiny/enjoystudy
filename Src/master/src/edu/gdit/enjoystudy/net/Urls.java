package edu.gdit.enjoystudy.net;

/**
 * 接口地址
 * 
 * @author skytoup
 * 
 */
public class Urls {
	// bug提交
	public final static String POST_ERROR = "http://ping4545.jd-app.com/postapperror";
	// 接口根目录
	private final static String ROOT = "http://192.168.191.1:8080/zuiaixue";
//	 private final static String ROOT = "http://zuiaixue.jd-app.com";
	// 登陆
	public final static String LOGIN = ROOT + "/login";
	// 获取学校列表
	public final static String SCHOOLS = ROOT + "/getallschool";
	// 广告条
	public final static String BANNER = ROOT + "/banner";
	// 热门搜索
	public final static String HOT_SEARCH = ROOT + "/gethotsearch";
	// 搜索
	public final static String SEARCH = ROOT + "/searchcourse";
	// 签到
	public final static String SIGN_IN = ROOT + "/signin";
	// 课程item信息
	public final static String VIDEO_INFO = ROOT + "/getindexclass";
	/**
	 * 课程详情 id
	 */
	public final static String COUSERS = ROOT + "/getclassdetails";
	// 收藏的课程
	public final static String COLLECT_CLASS = ROOT + "/getcollectlist";
	// 添加收藏
	public final static String ADD_COLLECTION = ROOT + "/addcollect";

	// public final static String fuckyou = "suping";
	// 今日任务
	public final static String TASK_TODAY = ROOT + "/tasktoday";
	// 定制计划
	public final static String ADDP_LAN = ROOT + "/addplan";
	// 获取用户信息
	public final static String USER_INFO = ROOT + "/userinfo";
	// 获取全部计划
	public final static String ALL_PLAN = ROOT + "/getallplan";
	// 删除计划
	public final static String DEL_PLAN = ROOT + "/delplan";
	// 提交推送
	public final static String POSTDBID = ROOT + "/postdbid";

	// 获取课程评论
	public final static String GETCOMMENT = ROOT + "/getcomment";

	// 获取评论的回复
	public final static String MORECOMMENT = ROOT + "/morecomment";
	// 获取用户信息
	public final static String GET_USER_INFO = ROOT + "/getuserinfo";
	// 关注
	public final static String ATT = ROOT + "/attention";
	// 取消关注
	public final static String CANCLE_ATT = ROOT + "/delattention";

	// 发起课程评论
	public final static String DISCUSSCOURSE = ROOT + "/discusscourse";

	// 回复某个课程评论
	public final static String COMMENTDISCUS = ROOT + "/commentdiscus";

	// ；列出关注(良师益友)
	public final static String LISTATTENTION = ROOT + "/listattention";

}
