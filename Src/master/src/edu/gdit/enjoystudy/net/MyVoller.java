package edu.gdit.enjoystudy.net;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.support.annotation.Nullable;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.utils.L;

/**
 * 自定义Volley
 * 
 * @author skytoup
 *
 */
public class MyVoller {

	private final static String TAG = "-->>MyVolley";
	private RequestQueue mRequestQueue;
	private OnGetDataed mOnGetDataed;
	private int mRequestCount;

	public MyVoller(Context context, OnGetDataed onGetDataed) {
		this.mOnGetDataed = onGetDataed;
		this.mRequestQueue = Volley.newRequestQueue(context);
		this.mRequestCount = 0;
	}
	
	/**
	 * 如果是像activitiy 没必要传两次
	 */
	public MyVoller(Context context) {
		mOnGetDataed=(OnGetDataed)context;
		this.mRequestQueue = Volley.newRequestQueue(context);
		this.mRequestCount = 0;
	}

	/**
	 * 
	 * 访问url获取Json，默认请求超时时间为25秒
	 *
	 * skytoup
	 *
	 * 日期：2014年8月19日-下午4:46:19
	 *
	 * @param type
	 *            请求类型，post、get
	 * @param tag
	 *            请求的标识
	 * @param url
	 *            请求的地址
	 * @param params
	 *            请求的参数
	 * @param isCache
	 *            是否缓存
	 */
	public void urlToJsonObject(int type, final String tag, String url,
			@Nullable Map<String, String> params, boolean isCache) {
		this.urlToJsonObject(type, tag, url, params, 25, isCache);
	}

	/**
	 * 
	 * 访问url获取Json，默认为POST请求
	 *
	 * skytoup
	 *
	 * 日期：2014年8月19日-下午4:46:31
	 *
	 * @param tag
	 *            请求的标识
	 * @param url
	 *            请求地址
	 * @param params
	 *            请求参数
	 * @param timeOut
	 *            请求超时时间
	 * @param isCache
	 *            是否缓存
	 */
	public void urlToJsonObject(final String tag, String url,
			@Nullable Map<String, String> params, int timeOut, boolean isCache) {
		this.urlToJsonObject(Request.Method.POST, tag, url, params, timeOut,
				isCache);
	}

	/**
	 * 
	 * 访问url获取Json，默认为POST请求，请求超时时间为25秒
	 *
	 * skytoup
	 *
	 * 日期：2014年8月19日-下午4:46:38
	 *
	 * @param tag
	 *            请求的标识
	 * @param url
	 *            请求地址
	 * @param params
	 *            请求参数
	 * @param isCache
	 *            是否缓存
	 */
	public void urlToJsonObject(final String tag, String url,
			@Nullable Map<String, String> params, boolean isCache) {
		this.urlToJsonObject(Request.Method.POST, tag, url, params, 25, isCache);
	}

	/**
	 * 
	 * 访问url获取Json
	 *
	 * skytoup
	 *
	 * 日期：2014年8月17日-下午11:07:42
	 *
	 * @param type
	 *            请求类型，post、get
	 * @param tag
	 *            请求的标识
	 * @param url
	 *            请求地址
	 * @param params
	 *            请求参数
	 * @param timeOut
	 *            请求超时时间
	 * @param isCache
	 *            是否缓存
	 */
	public synchronized void urlToJsonObject(int type, final String tag,
			final String url, @Nullable Map<String, String> params,
			int timeOut, final boolean isCache) {
		if (params == null) {
			params = new HashMap<String, String>();
		}
		L.i(TAG, "开始请求，url:" + url + ",请求的参数："
				+ new org.json.JSONObject(params));
		MyRequest request = new MyRequest(type, params, url, timeOut,
				new Response.Listener<String>() {

					@Override
					public void onResponse(String j) {
						int count = --MyVoller.this.mRequestCount;
						L.i(TAG, "访问成功，返回的JSON：" + j);
						JSONObject jsonObject = JSON.parseObject(j);

						int errorCode = jsonObject.getIntValue("errorcode");
						switch (errorCode) {
						case 0:// 成功
							if (isCache) {
								CacheDB.inster(url + tag, j);// 缓存
							}
							mOnGetDataed.onSuccess(tag, jsonObject, count);
							break;
						case 1:// 一般错误
						case 100:// 重新登录
						case 404:// 访问接口不存在
						case 1000:// 数据库错误
							String msg = jsonObject.getString("message");
							mOnGetDataed.onFail(tag, errorCode, msg, count);
							break;
						default:
							mOnGetDataed.onFail(tag, errorCode, "未知错误", count);
							break;
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						int count = --MyVoller.this.mRequestCount;
						// if (error instanceof TimeoutError) {
						// L.i(TAG, "访问失败，请求超时");
						// mOnGetDataed.onFail(tag, -1, "请求超时", count);
						// } else if (error instanceof NoConnectionError) {
						// L.i(TAG, "访问失败，无网络访问");
						// mOnGetDataed.onFail(tag, -2, "无网络访问", count);
						// }
						// String msg = error.getMessage();
						// if (msg == null || msg.equals("")) {
						// msg = "未知错误";
						// }
						error.printStackTrace();
						String msg = getErrorMessage(error);
						L.i(TAG, "访问失败，错误信息：" + msg);
						mOnGetDataed.onFail(tag, -1, msg, count);
					}
				});
		request.setTimeOut(timeOut);
		++this.mRequestCount;
		this.mRequestQueue.add(request);
	}

	/**
	 * 
	 * 停止所有请求
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-下午5:45:26
	 *
	 */
	public void stopAllRequest() {
		this.mRequestQueue.stop();
	}

	/**
	 * 请求完毕后回调的接口
	 * 
	 * @author skytoup
	 *
	 */
	public interface OnGetDataed {
		public void onSuccess(String tag, JSONObject jsonObject,
				int requestCount);

		public void onFail(String tag, int errorCode, String message,
				int requestCount);
	}

	private static String getErrorMessage(VolleyError error) {
		String result = null;
		if (error != null) {
			if (error.networkResponse != null) {
				switch (error.networkResponse.statusCode) {
				case 400:
					result = "请求失败";
					break;
				case 403:
					result = "请求被禁止";
					break;
				case 404:
					result = "HTTP没有找到";
					break;
				case 500:
					result = "服务器错误";
					break;
				case 502:
					result = "网关错误";
					break;
				default:
					result = "请求失败" + error.networkResponse.statusCode;
					break;
				}
			} else {
				String str = error.getMessage();
				if (str == null) {
					result = "请求超时";
				} else {
					if (str.startsWith("java.net.ConnectException:")) {
						result = "连接超时";
					} else if (str.startsWith("java.lang.RuntimeException:")) {
						result = "URL错误";
					} else if (str.startsWith("java.net.UnknownHostException:")) {
						result = "未知的HOST";
					} else if (str
							.startsWith("java.lang.IllegalArgumentException:")) {
						result = "提交参数不正确";
					} else if (str.startsWith("java.net.SocketException:")) {
						result = "连接失败";
					} else {
						result = str;
					}
				}
			}
		} else {
			result = "未知错误";
		}
		return result;
	}
}

/**
 * 自定义Volley请求
 * 
 * @author skytoup
 *
 */
class MyRequest extends StringRequest {

	private Map<String, String> mParams;
	private int mTime;

	public MyRequest(int type, Map<String, String> params, String url,
			int time, Listener<String> listener, ErrorListener errorListener) {
		super(type, url, listener, errorListener);
		this.mParams = params;
		this.setTimeOut(time);
	}

	@Override
	protected Map<String, String> getParams() {// 返回参数
		return mParams;
	}

	/**
	 * 
	 * 获取请求参数
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-下午4:28:38
	 *
	 * @return
	 */
	public Map<String, String> getRequestParams() {
		return mParams;
	}

	/**
	 * 
	 * 设置请求参数
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-下午4:28:54
	 *
	 * @param mParams
	 *            请求的参数
	 */
	public void setRequestParams(Map<String, String> params) {
		this.mParams = params;
	}

	/**
	 * 
	 * 获取请求超时时间
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-下午4:29:21
	 *
	 * @return
	 */
	public int getTimeOut() {
		return mTime;
	}

	/**
	 * 
	 * 设置请求超时时间
	 *
	 * skytoup
	 *
	 * 日期：2014年8月14日-下午4:29:36
	 *
	 * @param mTime
	 *            超时的时间（秒）
	 */
	public void setTimeOut(int time) {
		this.mTime = time;
		this.setRetryPolicy(new DefaultRetryPolicy(time * 1000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
	}

}