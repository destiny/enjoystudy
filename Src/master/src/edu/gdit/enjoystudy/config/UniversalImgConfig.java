/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-4   Create
 * 
 **/
package edu.gdit.enjoystudy.config;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import edu.gdit.enjoystudy.R;

/**
 * Description:统一 UniversalImageLoader 配置
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class UniversalImgConfig {

	public final static DisplayImageOptions displayImageOptions;

	static {
		displayImageOptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.img_onload)
				.showImageForEmptyUri(R.drawable.img_empty)
				.showImageOnFail(R.drawable.img_error).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				// .bitmapConfig(Bitmap.Config.RGB_565) 不知干啥
				.displayer(new RoundedBitmapDisplayer(20)).build();
	}

	public static class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				imageView.setImageBitmap(loadedImage);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

	public static class MyImageLoadingListener extends
			SimpleImageLoadingListener {
		private View mView;

		/**
		 * 
		 */
		public MyImageLoadingListener() {
		}

		MyImageLoadingListener(View view) {
			mView = view;
		}

		@Override
		public void onLoadingStarted(String imageUri, View view) {
			mView.setVisibility(View.VISIBLE);
		}

		@Override
		public void onLoadingFailed(String imageUri, View view,
				FailReason failReason) {
		}

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
		}

		@Override
		public void onLoadingCancelled(String imageUri, View view) {
		}

	}

	public static class MyImageLoadingProgressListener implements
			ImageLoadingProgressListener {

		private ProgressBar mProgressBar;

		/**
		 * 
		 */
		public MyImageLoadingProgressListener(ProgressBar progressBar) {
			mProgressBar = progressBar;
		}

		@Override
		public void onProgressUpdate(String imageUri, View view, int current,
				int total) {
			mProgressBar.setProgress(Math.round(100.0f * current / total));
		}

	}

}
