/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-5   Create
 * 
 **/
package edu.gdit.enjoystudy.app;

import java.util.UUID;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;
import android.view.WindowManager;

import com.baidu.frontia.FrontiaApplication;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import edu.gdit.enjoystudy.config.AppConfig;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.utils.StringUtils;
import edu.gdit.enjoystudy.vitamio.util.FileUtils;

/**
 * Description: 全局字段可以放在这里
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class App extends FrontiaApplication {

	/** OPlayer SD卡缓存路径 */
	public static final String OPLAYER_CACHE_BASE = Environment
			.getExternalStorageDirectory() + "/oplayer";
	/** 视频截图缓冲路径 */
	public static final String OPLAYER_VIDEO_THUMB = OPLAYER_CACHE_BASE
			+ "/thumb/";
	/** 首次扫描 */
	public static final String PREF_KEY_FIRST = "application_first";
	public static final String FROM_ME = "fromVitamioInitActivity";
	public static final String TAG = "-->>APP";
	private boolean login = false; // 登录状态
	private int loginUid = 0; // 登录用户的id
	private static int sceenWidth;// 屏幕宽
	private static int sceenHeight;// 屏幕高
	private static App mApplication;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate() {
		super.onCreate();
		mApplication = this;
		WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
		App.sceenWidth = manager.getDefaultDisplay().getWidth();
		App.sceenHeight = manager.getDefaultDisplay().getHeight();
		initVideo();
		initImageLoader(getApplicationContext());
		CacheDB.initDB(this);
	}

	private void initVideo() {
		// 创建缓存目录
		FileUtils.createIfNoExists(OPLAYER_CACHE_BASE);
		FileUtils.createIfNoExists(OPLAYER_VIDEO_THUMB);
	}

	public void setProperty(String key, String value) {
		AppConfig.getAppConfig(this).set(key, value);
	}

	public String getProperty(String key) {
		return AppConfig.getAppConfig(this).get(key);
	}

	public static void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you
		// may tune some of them,
		// or you can create default configuration by
		// ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				context).threadPriority(Thread.NORM_PRIORITY - 3)
				.denyCacheImageMultipleSizesInMemory()
				.diskCacheFileNameGenerator(new Md5FileNameGenerator())
				.diskCacheSize(50 * 1024 * 1024)
				.memoryCacheSize(20 * 1024 * 1024)
				// 50 Mb
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.writeDebugLogs() // Remove for release app
				.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}

	/**
	 * 判断当前版本是否兼容目标版本的方法
	 * 
	 * @param VersionCode
	 * @return
	 */
	public static boolean isMethodsCompat(int VersionCode) {
		int currentVersion = android.os.Build.VERSION.SDK_INT;
		return currentVersion >= VersionCode;
	}

	/** 销毁 */
	public void destory() {
		mApplication = null;
	}

	/**
	 * 获取App安装包信息
	 * 
	 * @return
	 */
	public PackageInfo getPackageInfo() {
		PackageInfo info = null;
		try {
			info = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace(System.err);
		}
		if (info == null)
			info = new PackageInfo();
		return info;
	}

	/**
	 * 获取App唯一标识
	 * 
	 * @return
	 */
	public String getAppId() {
		String uniqueID = getProperty(AppConfig.CONF_APP_UNIQUEID);
		if (StringUtils.isEmpty(uniqueID)) {
			uniqueID = UUID.randomUUID().toString();
			setProperty(AppConfig.CONF_APP_UNIQUEID, uniqueID);
		}
		return uniqueID;
	}

	/**
	 * 用户是否登录
	 * 
	 * @return
	 */
	public boolean isLogin() {
		return login;
	}

	/**
	 * 获取登录用户id
	 * 
	 * @return
	 */
	public int getLoginUid() {
		return this.loginUid;
	}

	/**
	 * 
	 * 获取屏幕宽度
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月13日-下午6:47:00
	 * 
	 * @return
	 */
	public static int getSceenWidth() {
		return sceenWidth;
	}

	/**
	 * 
	 * 获取屏幕高度
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月13日-下午6:47:13
	 * 
	 * @return
	 */
	public static int getSceenHeight() {
		return sceenHeight;
	}

	/**
	 * 描述：
	 * 
	 * @return
	 * @return Context
	 */
	public static Context getContext() {
		// TODO Auto-generated method stub
		return mApplication;
	}

	public static App getApplication() {
		return mApplication;
	}

}
