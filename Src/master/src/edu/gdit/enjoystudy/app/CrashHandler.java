package edu.gdit.enjoystudy.app;

import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Looper;

import com.alibaba.fastjson.JSONObject;

import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.utils.AppUtil;
import edu.gdit.enjoystudy.utils.FileUtil;

/**
 * 异常捕获
 * 
 * @author ping
 * @create 2014-5-22 下午9:58:27
 */

public class CrashHandler implements UncaughtExceptionHandler, OnGetDataed {

	public static final String TAG = "CrashHandler";
	private static CrashHandler INSTANCE = new CrashHandler();
	private static Activity mActivity;
	private Thread.UncaughtExceptionHandler mDefaultHandler;

	private CrashHandler() {

	}

	public static CrashHandler getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new CrashHandler();
		}
		return INSTANCE;
	}

	public void init(Activity activity) {
		this.mActivity = activity;
		mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(this);
		File folder = new File(mActivity.getCacheDir().getAbsolutePath()
				+ "/crash");
		if (!folder.exists()) {
			folder.mkdirs();
		}
		File[] files = folder.listFiles();
		MyVoller myVoller = new MyVoller(mActivity, this);
		for (int i = 0; i < files.length; i++) {
			if (i >= 3) {
				files[i].delete();
			} else {
				String repost = FileUtil.file2str(files[i]);
				repost = repost.replace("android", "_").replace("com.", "_")
						.replace(" ", "").replace("\n", "").replace("&", "");
				int length = repost.length() > 255 ? 255 : repost.length();
				repost = repost.substring(0, length);
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("appname",
						AppUtil.getPackageInfo(mActivity).packageName);
				params.put("version", AppUtil.getselfVersionName(mActivity));
				params.put("content", repost);
				params.put("device", "android"
						+ android.os.Build.VERSION.RELEASE + "("
						+ android.os.Build.MODEL + ")");
				myVoller.urlToJsonObject(files[i].getAbsolutePath(),
						Urls.POST_ERROR, params, false);
			}
		}
	}

	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		if (mDefaultHandler == null) {
			android.os.Process.killProcess(android.os.Process.myPid());
			return;
		}
		ex.printStackTrace();
		String repost = AppUtil.getCrashReport(mActivity, ex);
		String fileName = "crash-" + System.currentTimeMillis() + ".txt";
		FileUtil.save2File(repost, mActivity.getCacheDir().getAbsolutePath()
				+ "/crash" + File.separator, fileName);

		new Thread() {
			@Override
			public void run() {
				Looper.prepare();
				new AlertDialog.Builder(mActivity).setCancelable(false)
						.setMessage("程序出现异常，即将退出！")
						.setPositiveButton("确认", new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								System.exit(0);
							}
						}).create().show();
				;
				Looper.loop();
			}
		}.start();
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		new File(tag).delete();
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {

	}

}