package edu.gdit.enjoystudy.app;

import android.os.Environment;

/**
 * 程序常量
 * 
 * @author ping 2014-4-2 下午3:02:18
 */
public class Constant {
	static {

	}

	public final static boolean HASSDCARD = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
	public final static String SDCARDPATH = Environment.getExternalStorageDirectory().getAbsolutePath();

	 public final static String PUSHKEY = "04CcZbV5EQdrDQf9NxGXzl9m";// 百度推送apikey

}
