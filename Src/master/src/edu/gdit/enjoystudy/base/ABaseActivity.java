/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-7-30   Create
 * 
 **/
package edu.gdit.enjoystudy.base;

import android.app.Activity;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Description:一般Activity的基类
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public abstract class ABaseActivity extends Activity {

	// 得到ImageLoader 实例
	protected ImageLoader imageLoader = ImageLoader.getInstance();
}
