/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-5   Create
 * 
 **/
package edu.gdit.enjoystudy.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import edu.gdit.enjoystudy.app.App;
import edu.gdit.enjoystudy.interfaces.HandleActivityForResult;

/**
 * Description:
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public abstract class BaseFragment extends Fragment implements
		HandleActivityForResult {

	public App getApp() {
		return (App) getActivity().getApplication();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	/**
	 * 
	 * 当Fragment创建或者显示时调用
	 *
	 * skytoup
	 *
	 * 日期：2014年8月12日-下午9:09:47
	 *
	 */
	public abstract void onShow(ActionBarActivity activity);
}
