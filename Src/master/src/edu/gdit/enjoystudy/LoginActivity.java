package edu.gdit.enjoystudy;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;

import edu.gdit.enjoystudy.app.BroadcastController;
import edu.gdit.enjoystudy.app.Constant;
import edu.gdit.enjoystudy.base.ABaseActivity;
import edu.gdit.enjoystudy.config.AppConfig;
import edu.gdit.enjoystudy.entity.EntitySchool;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.ui.personal.FragmentPersonal;
import edu.gdit.enjoystudy.utils.DialogUtil;
import edu.gdit.enjoystudy.utils.IntentUtil;
import edu.gdit.enjoystudy.utils.MD5;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;
import edu.gdit.enjoystudy.view.PullDoorView;

/**
 * 登陆界面
 * 
 * @author skytoup
 * 
 */
public class LoginActivity extends ABaseActivity implements OnClickListener,
		OnGetDataed, DialogInterface.OnClickListener {

	private static final String TAG_LOGIN = "login";
	private static final String TAG_SCHOOLS = "schools";
	private MyVoller mVoller;
	private EditText mEdtUser;
	private EditText mEdtPsw;
	private TextView mBtnLogin;
	private Button mBtnForgetPsw;
	private TextView mTvSchool;
	private PullDoorView mTipsViewRoot;
	private ArrayList<EntitySchool> mSchools;
	private int mChooseItem;
	private ProgressDialog mDialog;
	private View mTipsTextView;
	private Animation mTipsAnimation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.getActionBar().hide();
		setContentView(R.layout.activity_login);
		mVoller = new MyVoller(this, this);
		mTvSchool = (TextView) findViewById(R.id.tvSchool);
		mEdtUser = (EditText) findViewById(R.id.edtUser);
		mEdtPsw = (EditText) findViewById(R.id.edtPsw);
		mBtnLogin = (TextView) findViewById(R.id.btnLogin);
		mBtnForgetPsw = (Button) findViewById(R.id.btnForgetPsw);
		mTipsViewRoot = (PullDoorView) findViewById(R.id.login_help_view);
		mTipsTextView = findViewById(R.id.close_tips);
		mBtnLogin.setOnClickListener(this);
		mBtnLogin.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
					IntentUtil
							.startActivity(LoginActivity.this, MainActivity.class);
					LoginActivity.this.overridePendingTransition(R.anim.fade_in,
							R.anim.fade_out);
					LoginActivity.this.finish();
				return false;
			}
		});
		mBtnForgetPsw.setOnClickListener(this);
		mTvSchool.setOnClickListener(this);
		this.mChooseItem = -1;
		String user = SPUtils.getPrefString(this, SPkey.USER, "");
		mEdtUser.setText(user);

		/* 默认数据 */
		this.mChooseItem = 0;
		mTvSchool.setText("广东科学技术职业学院");
		mEdtUser.setText("0110130148");
		mEdtPsw.setText("123456");
		/* 默认数据 */

		mSchools = CacheDB.getCacheArray(Urls.SCHOOLS + TAG_SCHOOLS,
				EntitySchool.class);
		if (mSchools == null) {
			T.showShort(this, "获取学校列表中...");
		}
		mTipsAnimation = AnimationUtils.loadAnimation(this, R.anim.connection);

		mTipsTextView.startAnimation(mTipsAnimation);
		getSchools();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mTipsViewRoot.scrollTo(0, 300);
		mTipsViewRoot.startBounceAnim(mTipsViewRoot.getScrollY(),
				-mTipsViewRoot.getScrollY(), 1000);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mTipsTextView != null && mTipsAnimation != null)
			mTipsTextView.clearAnimation();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mVoller.stopAllRequest();
	}

	// 获取学校列表
	public void getSchools() {
		mVoller.urlToJsonObject(TAG_SCHOOLS, Urls.SCHOOLS, null, true);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.tvSchool:// 选择学校
			if (mSchools == null) {
				T.showShort(this, "正在获取学校列表...");
			} else {
				int size = mSchools.size();
				String[] items = new String[size];
				for (int i = 0; i < size; i++) {
					items[i] = mSchools.get(i).getSchoolname();
				}
				DialogUtil.showSingleItemDialog(this, "选择学校", items,
						mChooseItem, this);
			}
			break;
		case R.id.btnLogin:// 登陆
			String strUser = mEdtUser.getText().toString();
			String strPsw = mEdtPsw.getText().toString();
			if (mChooseItem == -1) {
				T.showShort(this, "请先选择学校");
				return;
			}
			if (strUser.equals("")) {
				SPUtils.setPrefString(this, SPkey.TOKEN, "accesstoken_is_debug");

				IntentUtil
						.startActivity(LoginActivity.this, MainActivity.class);
				LoginActivity.this.overridePendingTransition(R.anim.fade_in,
						R.anim.fade_out);
				LoginActivity.this.finish();
				// T.showShort(this, "账号不能为空");
				return;
			} else if (strUser.length() < 10) {
				T.showShort(this, "账号长度不足10位");
				return;
			}
			SPUtils.setPrefString(this, SPkey.USER, strUser);
			if (strPsw.equals("")) {
				T.showShort(this, "密码不能为空");
				return;
			} else if (strPsw.length() < 6) {
				T.showShort(this, "密码长度不足6位");
				return;
			}
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("username", strUser);
			map.put("password", MD5.getPWDMD5(strUser, strPsw));
		//	map.put("schoolid", "" + mSchools.get(mChooseItem).getSchoolid());
			map.put("schoolid", "" + 123);//假数据

			map.put("device", "android");
			map.put("system", "" + android.os.Build.VERSION.RELEASE);
			if (mDialog == null) {
				mDialog = DialogUtil.createProgressDialog(this, "登陆中...");
			}
			mDialog.show();
			mVoller.urlToJsonObject(TAG_LOGIN, Urls.LOGIN, map, false);
			break;
		case R.id.btnForgetPsw:// 忘记密码
			T.showShort(this, "请联系管理员");
			break;
		}
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		if (tag.equals(TAG_SCHOOLS)) {
			JSONArray array = jsonObject.getJSONArray("data");
			mSchools = (ArrayList<EntitySchool>) JSON.parseArray(
					array.toJSONString(), EntitySchool.class);
			T.showShort(this, "获取最新学校列表成功");
		} else if (tag.equals(TAG_LOGIN)) {
			if(AppConfig.isDebug){
				SPUtils.setPrefString(this, SPkey.TOKEN, "accesstoken_is_debug");

			}else{
				JSONObject obj = jsonObject.getJSONObject("data");
				String accesstoken = obj.getString("accesstoken");
				SPUtils.setPrefString(this, SPkey.TOKEN, accesstoken);

				String userInfo = jsonObject.getJSONObject("userInfo")
						.toJSONString();
				CacheDB.inster(Urls.USER_INFO + FragmentPersonal.TAG, "{\"data\":"
						+ userInfo + "}");

				Intent intent = new Intent();
				intent.setAction(BroadcastController.ACTION_USERCHANGE);
				this.sendBroadcast(intent);

				// 重启百度推送
				PushManager.stopWork(getApplicationContext());
				PushManager.startWork(getApplicationContext(),
						PushConstants.LOGIN_TYPE_API_KEY, Constant.PUSHKEY);

			}
			
			T.showShort(this, "登陆成功");
			mDialog.dismiss();
			finish();
		}
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		if (tag.equals(TAG_SCHOOLS)) {
			getSchools();
		} else if (tag.equals(TAG_LOGIN)) {
			mDialog.dismiss();
			T.showShort(this, "登陆失败,失败原因：" + message);
		}
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		this.mChooseItem = which;
		mTvSchool.setText(mSchools.get(which).getSchoolname());
		dialog.dismiss();
	}
}
