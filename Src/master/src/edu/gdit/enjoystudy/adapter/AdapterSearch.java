package edu.gdit.enjoystudy.adapter;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.utils.HolderUtil;

/**
 * 搜索专业列表适配器
 * 
 * @author skytoup
 *
 */
public class AdapterSearch extends BaseAdapter {

	private ArrayList<String> data;
	private LayoutInflater inflater;

	public AdapterSearch(LayoutInflater inflater) {
		this.inflater = inflater;
	}

	public AdapterSearch(LayoutInflater inflater, ArrayList<String> data) {
		this(inflater);
		this.data = data;
	}

	public void setData(ArrayList<String> data) {
		this.data = data;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return data == null ? 0 : data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_search_btn, parent,
					false);
		}
		Button button = HolderUtil.getView(convertView, R.id.btn);
		button.setText(data.get(position));
		return convertView;
	}

}
