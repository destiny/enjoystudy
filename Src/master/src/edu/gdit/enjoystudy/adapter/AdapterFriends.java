package edu.gdit.enjoystudy.adapter;

import java.util.ArrayList;

import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.entity.EntityAttention;
import edu.gdit.enjoystudy.utils.HolderUtil;

/**
 * 良师益友适配器
 * 
 * @author skytoup
 * 
 */
public class AdapterFriends implements ExpandableListAdapter {
	// String[] st = new String[3];
	private ArrayList[] data = new ArrayList[2];// 0教师 1学生

	private LayoutInflater mInflater;

	public AdapterFriends(LayoutInflater inflater,
			ArrayList<EntityAttention> list) {
		this.mInflater = inflater;
		data[0] = new ArrayList<EntityAttention>();
		data[1] = new ArrayList<EntityAttention>();
		if (list == null) {
			return;
		}
		for (EntityAttention att : list) {
			if (att.getUlevel() < 10) {// 10老师
				data[0].add(att);
			} else {
				data[1].add(att);
			}
		}
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
	}

	@Override
	public int getGroupCount() {
		return 2;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return data[groupPosition].size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return data[groupPosition];
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return data[groupPosition].get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.friends_ground_item, null);
		}
		if (groupPosition == 1) {
			((TextView) convertView.findViewById(R.id.tvTitle)).setText("益友");
		}

		TextView tv = (TextView) convertView.findViewById(R.id.tvNum);
		tv.setText(data[groupPosition].size() + "");

		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.friends_item, parent,
					false);
		}
		EntityAttention atten = (EntityAttention) data[groupPosition]
				.get(childPosition);

		ImageView img = HolderUtil.getView(convertView, R.id.circleImageView1);
		ImageLoader.getInstance().displayImage(atten.getHead(), img);

		TextView name = HolderUtil.getView(convertView, R.id.name);
		name.setText(atten.getName());

		TextView content = HolderUtil.getView(convertView, R.id.content);
		content.setText(atten.getIntroduction());

		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return true;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public void onGroupExpanded(int groupPosition) {

	}

	@Override
	public void onGroupCollapsed(int groupPosition) {

	}

	@Override
	public long getCombinedChildId(long groupId, long childId) {
		return childId;
	}

	@Override
	public long getCombinedGroupId(long groupId) {
		return groupId;
	}

}
