package edu.gdit.enjoystudy.adapter;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.config.AppConfig;
import edu.gdit.enjoystudy.entity.EntityCourse;
import edu.gdit.enjoystudy.utils.HolderUtil;

public class AdapterHome extends BaseAdapter  implements OnItemClickListener{

	private LayoutInflater mInflater;
	private ArrayList<EntityCourse> datas;

	public AdapterHome(LayoutInflater inflater, ArrayList<EntityCourse> datas) {
		this.mInflater = inflater;
		this.datas = datas;
	}

	public AdapterHome(LayoutInflater inflater) {
		this.mInflater = inflater;
	}

	public void setDatas(ArrayList<EntityCourse> datas) {
		this.datas = datas;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() { 
		if(AppConfig.isDebug){
			return 15;
		}
		return datas == null ? 0 : datas.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.home_item, parent, false);
		}
		TextView tvTime = HolderUtil.getView(convertView, R.id.tvTime);
		TextView tvName = HolderUtil.getView(convertView, R.id.tvName);
		TextView tvDynamic = HolderUtil.getView(convertView, R.id.tvDynamic);
		TextView tvSeeNum = HolderUtil.getView(convertView, R.id.tvSeeNum);
		TextView tvCollectNum = HolderUtil.getView(convertView,
				R.id.tvCollectNum);
		// TextView tvMsgNum = HolderUtil.getView(convertView, R.id.tvMsgNum);
		ImageView imgImg = HolderUtil.getView(convertView, R.id.imgImg);
		ImageView imgNew = HolderUtil.getView(convertView, R.id.imgNew);
		if(AppConfig.isDebug){
			return convertView;
		}
		EntityCourse video = datas.get(position);
		tvTime.setText(video.getTime());
		tvName.setText(video.getName());
		tvDynamic.setText(video.getCoursedynamic());
		tvSeeNum.setText("" + video.getSeesum());
		tvCollectNum.setText("" + video.getCollectnum());
		// tvMsgNum.setText(video.get);
		if (video.isNew()) {
			imgNew.setVisibility(View.VISIBLE);
		} else {
			imgNew.setVisibility(View.GONE);
		}
		ImageLoader.getInstance().displayImage(video.getImageurl(), imgImg);
		return convertView;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
	}

}
