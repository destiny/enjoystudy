package edu.gdit.enjoystudy.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import edu.gdit.enjoystudy.R;

/**
 * 兴趣小组排名榜的适配器
 * 
 * @author skytoup
 *
 */
public class AdapterClassRank extends BaseAdapter {

	LayoutInflater inflater;

	public AdapterClassRank(LayoutInflater inflater) {
		this.inflater = inflater;
	}

	@Override
	public int getCount() {
		return 10;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.class_rank_item, parent,
					false);
		}
		return convertView;
	}

}
