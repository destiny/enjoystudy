package edu.gdit.enjoystudy.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.utils.DateUtil;
import edu.gdit.enjoystudy.utils.HolderUtil;

/**
 * 每日进度的适配器
 * 
 * @author skytoup
 *
 */
public class AdapterMyCourseProgress extends BaseAdapter {

	private LayoutInflater inflater;

	public AdapterMyCourseProgress(LayoutInflater inflater) {
		this.inflater = inflater;
	}

	@Override
	public int getCount() {
		return 10;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.my_course_progress_item,
					parent, false);
		}

		ImageView imgHor = HolderUtil.getView(convertView, R.id.imgHor);
		ImageView circleImageView = HolderUtil.getView(convertView,
				R.id.circleImageView);
		ImageView img = HolderUtil.getView(convertView, R.id.img);
		RelativeLayout viewImgOutside = HolderUtil.getView(convertView,
				R.id.viewImgOutside);
		TextView tvMsg = HolderUtil.getView(convertView, R.id.tvMsg);
		switch (position % 3) {
		case 0:
			imgHor.setImageResource(R.drawable.my_course_progress_item_time);
			viewImgOutside.setVisibility(View.GONE);
			tvMsg.setTextSize(20);
			tvMsg.setText(DateUtil.getSpecifiedDayBefore(DateUtil.getDate(),
					position == 0 ? 0 : position / 3));
			break;

		case 1:
			imgHor.setImageResource(R.drawable.my_course_progress_item_hor_1);
			viewImgOutside.setVisibility(View.VISIBLE);
			circleImageView.setVisibility(View.VISIBLE);
			img.setVisibility(View.GONE);
			circleImageView.setImageResource(R.drawable.img_onload);
			tvMsg.setTextSize(14);
			tvMsg.setText("今天学了Java_" + position);
			break;

		case 2:
			imgHor.setImageResource(R.drawable.my_course_progress_item_hor_2);
			viewImgOutside.setVisibility(View.VISIBLE);
			circleImageView.setVisibility(View.GONE);
			img.setVisibility(View.VISIBLE);
			img.setImageResource(R.drawable.home_actionbar_signin);
			tvMsg.setTextSize(14);
			tvMsg.setText(DateUtil.getSpecifiedDayBefore(DateUtil.getDate(),
					position / 3) + "签到了");
			break;
		}
		return convertView;
	}

}
