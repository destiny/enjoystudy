package edu.gdit.enjoystudy.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.utils.HolderUtil;

/**
 * 历程树的适配器
 * 
 * @author skytoup
 *
 */
public class AdapterPersonCourseTree extends BaseAdapter {

	private LayoutInflater inflater;

	public AdapterPersonCourseTree(LayoutInflater inflater) {
		this.inflater = inflater;
	}

	@Override
	public int getCount() {
		return 10;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.person_course_tree_item,
					parent, false);
		}
		View leftNode = HolderUtil.getView(convertView, R.id.viewLeftNode);
		View rightNode = HolderUtil.getView(convertView, R.id.viewRightNode);
		TextView tvTime = null;
		TextView tvMsg = null;
		ImageView img = null;
		if (position % 2 == 0) {
			leftNode.setVisibility(View.VISIBLE);
			rightNode.setVisibility(View.GONE);
			tvTime = HolderUtil.getView(convertView, R.id.tvLeftTime);
			tvMsg = HolderUtil.getView(convertView, R.id.tvLeftMsg);
			img = HolderUtil.getView(convertView, R.id.circleImageViewLeft);
		} else {
			leftNode.setVisibility(View.GONE);
			rightNode.setVisibility(View.VISIBLE);
			tvTime = HolderUtil.getView(convertView, R.id.tvRightTime);
			tvMsg = HolderUtil.getView(convertView, R.id.tvRightMsg);
			img = HolderUtil.getView(convertView, R.id.circleImageViewRight);
		}
		return convertView;
	}
}
