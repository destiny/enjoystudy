package edu.gdit.enjoystudy.adapter;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.entity.EntityVCommentSub;
import edu.gdit.enjoystudy.utils.HolderUtil;

public class CommentSubAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private ArrayList<EntityVCommentSub> list;
	
	
	public CommentSubAdapter(LayoutInflater inflater) {
		mInflater = inflater;
	}
	
	public void setdata(ArrayList<EntityVCommentSub> list) {
		this.list = list;
		notifyDataSetChanged();
	}
	
	
	@Override
	public int getCount() {
		return list==null?0:list.size();
	}

	@Override
	public Object getItem(int i) {
		return null;
	}

	@Override
	public long getItemId(int i) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView ==null) {
			convertView = mInflater.inflate(R.layout.friends_item, parent,false);			
		}
		EntityVCommentSub commsub = list.get(position);
		
		ImageView img = HolderUtil.getView(convertView, R.id.circleImageView1);
		ImageLoader.getInstance().displayImage(commsub.getHead(), img);
		
		TextView name = HolderUtil.getView(convertView, R.id.name);
		name.setText(commsub.getUsername());
		
		TextView content = HolderUtil.getView(convertView, R.id.content);
//		content.setText(commsub.getContent());
		if (commsub.getTouserid()!=-1) {
			commsub.setTousername(commsub.getTousername()==null?"吴同学":commsub.getTousername());
			commsub.setContent(commsub.getContent()==null?"无内容":commsub.getContent());
			
			content.setText("回复:" + commsub.getTousername() + "\n" + commsub.getContent());
		} else {
			content.setText(commsub.getContent());			
		}
		
		return convertView;
	}

}
