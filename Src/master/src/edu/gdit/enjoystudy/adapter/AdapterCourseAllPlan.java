package edu.gdit.enjoystudy.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.entity.EntityPlan;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.net.MyVoller;
import edu.gdit.enjoystudy.net.MyVoller.OnGetDataed;
import edu.gdit.enjoystudy.net.Urls;
import edu.gdit.enjoystudy.utils.DialogUtil;
import edu.gdit.enjoystudy.utils.HolderUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.utils.SPUtils;
import edu.gdit.enjoystudy.utils.SPkey;
import edu.gdit.enjoystudy.utils.T;

public class AdapterCourseAllPlan extends BaseAdapter implements OnGetDataed {

	private LayoutInflater mInflater;
	private ArrayList<EntityPlan> datas;
	private MyVoller voller;
	private Dialog dialog;

	public AdapterCourseAllPlan(LayoutInflater inflater,
			ArrayList<EntityPlan> datas) {
		voller = new MyVoller(inflater.getContext(), this);
		this.mInflater = inflater;
		this.datas = datas;
		dialog = DialogUtil.createProgressDialog(inflater.getContext(),
				"正在删除计划中...");
	}

	@Override
	public int getCount() {
		return datas == null ? 0 : datas.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.couser_item_all_plan,
					parent, false);
		}
		final EntityPlan plan = datas.get(position);
		TextView tvName = HolderUtil.getView(convertView, R.id.tvTitle);
		TextView tvStartDate = HolderUtil.getView(convertView, R.id.tv_date);
		TextView tvCountdown = HolderUtil
				.getView(convertView, R.id.tvCountdown);
		HolderUtil.getView(convertView, R.id.imgDel).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						HashMap<String, String> map = new HashMap<String, String>();
						if (L.isDebug) {
							map.put("accesstoken",
									"899489f74a6947ad3e32d960f2df506490e436342d4cb9c1");
						} else {
							map.put("accesstoken", SPUtils.getPrefString(
									mInflater.getContext(), SPkey.TOKEN, ""));
						}
						map.put("id", "" + plan.getPlanid());
						voller.urlToJsonObject("" + position, Urls.DEL_PLAN,
								map, false);
						dialog.show();
					}
				});
		tvName.setText(plan.getTheme());
		tvCountdown.setText(plan.getCountDown());
		tvStartDate.setText(plan.getStartDate());
		return convertView;
	}

	@Override
	public void onSuccess(String tag, JSONObject jsonObject, int requestCount) {
		int p = Integer.parseInt(tag);
		datas.remove(p);
		notifyDataSetChanged();
		saveDatas();
		T.showShort(mInflater.getContext(), jsonObject.getString("message"));
		DialogUtil.disShow(dialog);
	}

	@Override
	public void onFail(String tag, int errorCode, String message,
			int requestCount) {
		T.showShort(mInflater.getContext(), message);
		DialogUtil.disShow(dialog);
	}

	// 保存数据
	private void saveDatas() {
		CacheDB.inster(Urls.ALL_PLAN + EntityPlan.TAG,
				"{\"data\":" + JSON.toJSONString(datas) + "}");
	}
}
