package edu.gdit.enjoystudy.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.database.DataSetObserver;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lidroid.xutils.exception.HttpException;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.entity.EntityDownload;
import edu.gdit.enjoystudy.greenDAO.CacheDB;
import edu.gdit.enjoystudy.ui.download.DownloadService.DownloadBind;
import edu.gdit.enjoystudy.ui.download.DownloadService.OnDownload;
import edu.gdit.enjoystudy.utils.HolderUtil;

/**
 * 下载的适配器
 * 
 * @author skytoup
 * 
 */
public class AdapterDownload extends BaseExpandableListAdapter implements
		OnClickListener, OnDownload {

	private LayoutInflater mInflater;
	private TextView[] groundTv;
	ArrayList<EntityDownload> downloading;
	ArrayList<EntityDownload> downloaded;
	DownloadBind bind = null;
	ExpandableListView listView;
	private HashMap<EntityDownload, View> map;

	/**
	 * 设置服务的连接
	 * 
	 * @param bind
	 */
	public void setDownloadBind(DownloadBind bind) {
		this.bind = bind;
		bind.setOnDownload(this);
	}

	public AdapterDownload(LayoutInflater inflater, ExpandableListView listView) {
		this.mInflater = inflater;
		downloading = CacheDB.getCacheArray(EntityDownload.TAG_DOWNLOAD_ING,
				EntityDownload.class);
		downloaded = CacheDB.getCacheArray(EntityDownload.TAG_DOWNLOAD_ED,
				EntityDownload.class);
		if (downloading == null) {
			downloading = new ArrayList<EntityDownload>();
		}
		if (downloaded == null) {
			downloaded = new ArrayList<EntityDownload>();
		}
		this.listView = listView;
		map = new HashMap<EntityDownload, View>();
		groundTv = new TextView[2];
		groundTv[0] = (TextView) inflater.inflate(
				R.layout.download_ground_item, null);
		groundTv[1] = (TextView) inflater.inflate(
				R.layout.download_ground_item, null);
		groundTv[0].setText("正在下载的课程");
		groundTv[1].setText("已经下载的课程");
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
	}

	@Override
	public int getGroupCount() {
		return 2;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		int count = 0;
		if (groupPosition == 0) {
			count = downloading == null ? 0 : downloading.size();
		} else {
			count = downloaded == null ? 0 : downloaded.size();
		}
		return count;
	}

	@Override
	public Object getGroup(int groupPosition) {
		if (groupPosition == 0) {

			groundTv[0].setText("正在下载的课程");
		} else {
			groundTv[1].setText("已经下载的课程");
		}
		return groundTv[groupPosition];
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		return groundTv[groupPosition];
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		if (groupPosition == 0) {
			if (convertView == null
					|| convertView.findViewById(R.id.btnCancle) == null) {
				convertView = mInflater.inflate(R.layout.downloading_item,
						parent, false);
			}
			TextView tvName = HolderUtil.getView(convertView, R.id.tvName);
			final TextView tvProgress = HolderUtil.getView(convertView,
					R.id.tvProgress);
			final TextView tvSpeed = HolderUtil.getView(convertView,
					R.id.tvSpeed);
			final ProgressBar bar = HolderUtil.getView(convertView,
					R.id.progressBar);
			final Button btnControl = HolderUtil.getView(convertView,
					R.id.btnControl);
			Button btnCancle = HolderUtil.getView(convertView, R.id.btnCancle);
			final EntityDownload download = downloading.get(childPosition);
			map.put(download, convertView);
			if (bind == null || !bind.getIsDownload(download)) {
				btnControl.setText("开始");
			} else {
				btnControl.setText("暂停");
			}
			tvSpeed.setText("-");
			tvName.setText(download.getFileName());
			float p = download.getSize() == 0 ? 0 : ((float) download
					.getDownSize()) / download.getSize();
			bar.setMax(100);
			bar.setProgress((int) (p * 100));
			String d = Formatter.formatFileSize(mInflater.getContext(),
					download.getDownSize());
			String s = Formatter.formatFileSize(mInflater.getContext(),
					download.getSize());
			tvProgress.setText(d + "/" + s);
			btnControl.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!btnControl.getText().equals("暂停")) {
						btnControl.setText("暂停");
						bind.startDown(download);
					} else {
						btnControl.setText("开始");
						tvSpeed.setText("-");
						bind.pauseDown(download);
					}
					// notifyDataSetChanged();
				}
			});
			btnCancle.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					bind.cancleDown(download);
					downloading.remove(download);
					notifyDataSetChanged();
				}
			});
		} else {
			if (convertView == null
					|| convertView.findViewById(R.id.btnCancle) != null) {
				convertView = mInflater.inflate(R.layout.downloaded_item,
						parent, false);
			}
			TextView tvName = HolderUtil.getView(convertView, R.id.tvName);
			tvName.setText(downloaded.get(childPosition).getFileName());
		}
		convertView.setOnClickListener(this);
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return true;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {
	}

	@Override
	public long getCombinedChildId(long groupId, long childId) {
		return childId;
	}

	@Override
	public long getCombinedGroupId(long groupId) {
		return groupId;
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		downloading = CacheDB.getCacheArray(EntityDownload.TAG_DOWNLOAD_ING,
				EntityDownload.class);
		downloaded = CacheDB.getCacheArray(EntityDownload.TAG_DOWNLOAD_ED,
				EntityDownload.class);
		if (downloading == null) {
			downloading = new ArrayList<EntityDownload>();
		}
		if (downloaded == null) {
			downloaded = new ArrayList<EntityDownload>();
		}
		if (listView.isGroupExpanded(0)) {
			listView.collapseGroup(0);
			listView.expandGroup(0);
		}
		if (listView.isGroupExpanded(1)) {
			listView.collapseGroup(1);
			listView.expandGroup(1);
		}
	}

	@Override
	public void onClick(View v) {
		LinearLayout layout = (LinearLayout) v.findViewById(R.id.viewControl);
		layout.setVisibility(layout.getVisibility() == View.GONE ? View.VISIBLE
				: View.GONE);
	}

	@Override
	public void onSuccess(EntityDownload download) {
		downloaded.add(download);
		downloading.remove(download);
		notifyDataSetChanged();
	}

	@Override
	public void onFail(EntityDownload download, HttpException error, String msg) {
		Button view = (Button) map.get(download).findViewById(R.id.btnControl);
		view.setText("重试");
	}

	@Override
	public void onDownload(EntityDownload download, long total, long current,
			long speed) {
		View view = map.get(download);
		ProgressBar bar = (ProgressBar) view.findViewById(R.id.progressBar);
		TextView tvProgress = (TextView) view.findViewById(R.id.tvProgress);
		TextView tvSpeed = (TextView) view.findViewById(R.id.tvSpeed);
		float p = ((float) current) / total;
		bar.setMax(100);
		bar.setProgress((int) (p * 100));
		String d = Formatter.formatFileSize(mInflater.getContext(), current);
		String s = Formatter.formatFileSize(mInflater.getContext(), total);
		String sp = Formatter.formatFileSize(mInflater.getContext(), speed);
		tvProgress.setText(d + "/" + s);
		tvSpeed.setText(sp + "/s");
	}

}
