package edu.gdit.enjoystudy.adapter;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.config.UniversalImgConfig;
import edu.gdit.enjoystudy.config.UniversalImgConfig.AnimateFirstDisplayListener;
import edu.gdit.enjoystudy.entity.EntityTodayTask;
import edu.gdit.enjoystudy.utils.HolderUtil;

public class AdapterTask extends BaseAdapter {

	private LayoutInflater mInflater;
	private ArrayList<EntityTodayTask> datas;

	public AdapterTask(LayoutInflater inflater, ArrayList<EntityTodayTask> datas) {
		this.mInflater = inflater;
		this.datas = datas;
	}

	@Override
	public int getCount() {
		return datas == null ? 0 : datas.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.task_item, parent, false);
		}
		TextView tvCouserName = HolderUtil.getView(convertView,
				R.id.tvCouserName);
		TextView tvProgress = HolderUtil.getView(convertView, R.id.tvProgress);
		ImageView img = HolderUtil.getView(convertView, R.id.img);
		EntityTodayTask task = datas.get(position);
		tvCouserName.setText(task.getName());
		tvProgress.setText(task.getProgress() + "");
		ImageLoader imageLoader = ImageLoader.getInstance();
		imageLoader.displayImage(task.getPic(), img,
				UniversalImgConfig.displayImageOptions,
				new AnimateFirstDisplayListener());
		return convertView;
	}

}
