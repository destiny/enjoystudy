package edu.gdit.enjoystudy.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import edu.gdit.enjoystudy.R;

/**
 * 个人中心的勋章榜
 * 
 * @author skytoup
 *
 */
public class AdapterPersonMedals extends BaseAdapter {

	private LayoutInflater inflater;

	public AdapterPersonMedals(LayoutInflater inflater) {
		this.inflater = inflater;
	}

	@Override
	public int getCount() {
		return 10;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.person_medal_item, parent,
					false);
		}
		return convertView;
	}
}
