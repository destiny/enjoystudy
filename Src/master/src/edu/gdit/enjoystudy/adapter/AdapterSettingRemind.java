package edu.gdit.enjoystudy.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import edu.gdit.enjoystudy.R;
import edu.gdit.enjoystudy.utils.HolderUtil;
import edu.gdit.enjoystudy.view.ChooseWeekView;

/**
 * 设置提醒的适配器
 * 
 * @author skytoup
 *
 */
public class AdapterSettingRemind extends BaseAdapter {

	private LayoutInflater inflater;

	public AdapterSettingRemind(LayoutInflater inflater) {
		this.inflater = inflater;
	}

	@Override
	public int getCount() {
		return 10;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.setting_remind_item,
					parent, false);
		}
		ChooseWeekView chooseWeekView = HolderUtil.getView(convertView,
				R.id.chooseWeekView);
		chooseWeekView.setSelectable(false);
		return convertView;
	}

}
