package edu.gdit.enjoystudy;

import android.content.res.Configuration;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import edu.gdit.enjoystudy.app.CrashHandler;
import edu.gdit.enjoystudy.base.BaseActionBarActivity;
import edu.gdit.enjoystudy.base.BaseFragment;
import edu.gdit.enjoystudy.config.AppConfig;
import edu.gdit.enjoystudy.interfaces.DrawerMenuCallBack;
import edu.gdit.enjoystudy.ui.FragmentDemo;
import edu.gdit.enjoystudy.ui.FragmentDrawerMenu;
import edu.gdit.enjoystudy.ui.couser.FragmentCouser;
import edu.gdit.enjoystudy.ui.download.FragmentDownload;
import edu.gdit.enjoystudy.ui.friends.FragmentFriends;
import edu.gdit.enjoystudy.ui.home.FragmentHome;
import edu.gdit.enjoystudy.ui.myclass.FragmentClass;
import edu.gdit.enjoystudy.ui.personal.FragmentPersonal;
import edu.gdit.enjoystudy.ui.setting.FragmentSetting;
import edu.gdit.enjoystudy.ui.task.FragmentTask;
import edu.gdit.enjoystudy.utils.AppUtil;
import edu.gdit.enjoystudy.utils.L;
import edu.gdit.enjoystudy.view.CustomDrawerLayout;

/**
 * Description:主界面
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public class MainActivity extends BaseActionBarActivity implements
		DrawerMenuCallBack {

	static final String TAG_DRAWER_MENU = "drawer_menu";
	static final String TAG_CONTENT_HOME = "content_home";
	static final String TAG_CONTENT_CUORSER = "content_course";
	static final String TAG_CONTENT_CLASS = "content_class";
	static final String TAG_CONTENT_FRIENDS = "content_friends";
	static final String TAG_CONTENT_PERSONAL = "content_personal";
	static final String TAG_CONTENT_DOWNLOAD = "content_download";
	static final String TAG_CONTENT_SETTING = "content_setting";
	static final String TAG_CONTENT_TASK = "content_task";
	static final String TAG_CONTENT_DEMO = "content_demo";

	private CustomDrawerLayout mDrawerLayout;

	private ActionBar mActionBar;

	private FragmentManager mFragmentManager;

	private ActionBarDrawerToggle mDrawerToggle;

	private DoubleClickExitHelper mExitHelper;

	private Handler mHandler = new Handler();

	// 当前显示的界面标识
	private String mCurrentContentTag;

	private int mPostion = 0;

	/**
	 * 抽屉内容标识数组
	 */
	static final String CONTENTS[] = { TAG_CONTENT_HOME, // 0
			TAG_CONTENT_CUORSER, // 1
			TAG_CONTENT_TASK, // 2
			TAG_CONTENT_CLASS, // 3
			TAG_CONTENT_FRIENDS, // 4
			TAG_CONTENT_PERSONAL, // 5
			TAG_CONTENT_DOWNLOAD, // 6
			TAG_CONTENT_SETTING, // 7
			TAG_CONTENT_DEMO, // 8
	};

	static final String TITLES[] = { "首页", "我的课程", "今日任务", "我的班级", "良师益友",
			"个人中心", "下载管理", "设置", "测试" };

	/**
	 * fragment类名数组，与内容标识数组相对应
	 */
	static final String FRAGMENTS[] = { FragmentHome.class.getName(), // 0
			FragmentCouser.class.getName(), // 1
			FragmentTask.class.getName(), // 2
			FragmentClass.class.getName(), // 3
			FragmentFriends.class.getName(), // 4
			FragmentPersonal.class.getName(),// 5
			FragmentDownload.class.getName(),// 6
			FragmentSetting.class.getName(), // 7
			FragmentDemo.class.getName() // 8
	};
	private Fragment tagFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		CrashHandler.getInstance().init(this);
		mActionBar = getSupportActionBar();
		// 设置首页显示 那三横
		mActionBar.setDisplayHomeAsUpEnabled(true);
		// 设置左上角按钮可点击
		mActionBar.setHomeButtonEnabled(true);

		mActionBar.setDisplayShowCustomEnabled(false);
		mActionBar.setIcon(new BitmapDrawable());

		setContentView(R.layout.activity_main);
		// 获得退出执行类
		mExitHelper = new DoubleClickExitHelper(this);
		mFragmentManager = getSupportFragmentManager();
		mDrawerLayout = (CustomDrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerListener(new DrawerMenuListener());
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		// 拿到 DrawerToggle ，并给他设置背景
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, 0, 0);
		if (savedInstanceState == null) {
			FragmentTransaction ft = mFragmentManager.beginTransaction();
			ft.replace(R.id.drawer_menu, FragmentDrawerMenu.newInstance(),
					TAG_DRAWER_MENU).commit();
			transacionfragment(0);
			mActionBar.setTitle(TITLES[0]);
			mCurrentContentTag = TAG_CONTENT_HOME;
		}

	}

	@Override
	public void onResume() {
		super.onResume();
		mDrawerLayout.closeDrawers();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// MenuInflater inflater = getMenuInflater();
		// inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		int id = item.getItemId();
		switch (id) {
		case R.id.action_bar:
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 当设定的配置发生改变的时候，不会重新调用onCreate，而是调用本方法
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	/**
	 * 重写键按下方法，把事件交给mExitHelper处理
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// 判断菜单是否打开
			if (mDrawerLayout.isDrawerOpen(Gravity.START)) {
				mDrawerLayout.closeDrawers();
				return true;
			}
			return mExitHelper.onKeyDown(keyCode, event);
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * 显示内容 ,为了不卡，这里延时关闭
	 * 
	 * */
	public void showContent(int pos) {
		mPostion = pos;
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// 每次显示内容的时候，先关闭抽屉
				mDrawerLayout.closeDrawers();
			}
		}, 222);
		transacionfragment(pos);
	}

	/**
	 * 描述：开始转化fragment
	 * 
	 * @param pos
	 * @return void
	 */
	private void transacionfragment(int pos) {
		String tag = CONTENTS[pos];
		// 如果还是当前fragment，直接返回
		if (tag.equals(mCurrentContentTag)) {
			if (AppConfig.isDebug) {
				L.d("show content:" + tag);
			}
			return;
		}
		FragmentTransaction ft = mFragmentManager.beginTransaction();
		if (mCurrentContentTag != null) {
			Fragment fragment = mFragmentManager.findFragmentByTag(mCurrentContentTag);
			if (fragment != null) {
				ft.hide(fragment);
			}
		}
		tagFragment = mFragmentManager.findFragmentByTag(tag);
		if (tagFragment != null) {
			ft.show(tagFragment);
		} else {
			tagFragment = Fragment.instantiate(this, FRAGMENTS[pos]);
			ft.add(R.id.drawer_content, tagFragment, tag);
		}
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		ft.commit();
		((BaseFragment) tagFragment).onShow(this);
		mActionBar.setTitle(TITLES[pos]);
		mCurrentContentTag = tag;
	}

	/**
	 * Description:抽屉状态监听类
	 * 
	 * @author 林凌灵（最新修改者）
	 * @E-mail:ilove000@foxmail.com
	 * @version 1.0（最新版本号）
	 */
	private class DrawerMenuListener implements DrawerLayout.DrawerListener {

		@Override
		public void onDrawerOpened(View drawerView) {
			mDrawerToggle.onDrawerOpened(drawerView);
		}

		@Override
		public void onDrawerClosed(View drawerView) {
			mDrawerToggle.onDrawerClosed(drawerView);
			// transacionfragment(mPostion);
		}

		@Override
		public void onDrawerSlide(View drawerView, float slideOffset) {
			mDrawerToggle.onDrawerSlide(drawerView, slideOffset);
		}

		@Override
		public void onDrawerStateChanged(int newState) {
			mDrawerToggle.onDrawerStateChanged(newState);
		}
	}

	@Override
	public void onClickHome() {
		showContent(0);
	}

	@Override
	public void onClickCoursers() {
		showContent(1);
	}

	@Override
	public void onClickTask() {
		showContent(2);
	}

	@Override
	public void onClickClass() {
		showContent(3);
	}

	@Override
	public void onClickFriends() {
		showContent(4);
	}

	@Override
	public void onClickPresonal() {
		showContent(5);
	}

	@Override
	public void onClickDownload() {
		showContent(6);
	}

	@Override
	public void onClickSettring() {
		showContent(7);
	}

	@Override
	public void onClickDemo() {
		// showContent(8);
		AppUtil.exit();
	}

}
