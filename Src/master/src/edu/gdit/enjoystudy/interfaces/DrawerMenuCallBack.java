/*
 * Copyright (C) 2009-2014 gdit Inc.All Rights Reserved.
 * 
 * FileName：
 *
 * Description：简要描述本文件的内容
 * 
 * History：
 * 版本号	作者 	日期       简要介绍相关操作
 *  1.0     林凌灵   2014-8-8   Create
 * 
 **/
package edu.gdit.enjoystudy.interfaces;

/**
 * Description:菜单事件回调，只给fragment使用
 * 
 * @author 林凌灵（最新修改者）
 * @E-mail:ilove000@foxmail.com
 * @version 1.0（最新版本号）
 */
public interface DrawerMenuCallBack {
	
	/** 首页 0*/
	public void onClickHome();

	/** 我的课程 1 */
	public void onClickCoursers();

	/**  今日任务 2 */
	public void onClickTask();

	/**  我的班级 3 */
	public void onClickClass();

	/** 良师益友 4  */
	public void onClickFriends();
	
	/** 个人中心 5 */
	public void onClickPresonal();
	
	/** 下载管理 6 */
	public void onClickDownload();
	
	/** 设置 7*/
	public void onClickSettring();
	
	/** demo  8 */
	public void onClickDemo();

	
}
