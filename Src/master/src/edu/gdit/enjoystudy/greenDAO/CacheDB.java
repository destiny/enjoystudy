package edu.gdit.enjoystudy.greenDAO;

import java.util.ArrayList;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import de.greenrobot.dao.query.DeleteQuery;
import de.greenrobot.dao.query.QueryBuilder;
import edu.gdit.enjoystudy.greenDAO.CacheDao.Properties;
import edu.gdit.enjoystudy.greenDAO.DaoMaster.DevOpenHelper;
import edu.gdit.enjoystudy.utils.L;

public class CacheDB {

	private final static String TAG = "-->>Cache";
	private static CacheDao mCacheDao;// 数据库封装DAO

	/**
	 * 
	 * 初始化数据库
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月14日-下午9:18:20
	 * 
	 * @param context
	 *            上下文
	 */
	public static void initDB(Context context) {
		DevOpenHelper helper = new DaoMaster.DevOpenHelper(context,
				"caches-db", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		DaoMaster daoMaster = new DaoMaster(db);
		DaoSession daoSession = daoMaster.newSession();
		mCacheDao = daoSession.getCacheDao();
	}

	/**
	 * 
	 * 插入数据
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月14日-下午9:18:31
	 * 
	 * @param urlTag
	 *            url+TAG
	 * @param jsonData
	 */
	public static void inster(String urlTag, String jsonData) {
		Cache cache = new Cache(urlTag, jsonData);
		inster(cache);
	}

	/**
	 * 
	 * 插入数据
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月14日-下午9:18:51
	 * 
	 * @param cache
	 *            缓存数据
	 */
	public static void inster(Cache cache) {
		L.i(TAG, "准备缓存Key:" + cache.getUrlTag() + ",数据：" + cache.getJsonData());
		Cache oldCache = getCache(cache.getUrlTag());
		if (oldCache != null) {
			oldCache.setJsonData(cache.getJsonData());
			mCacheDao.update(oldCache);
		} else {
			mCacheDao.insert(cache);
		}
	}

	/**
	 * 
	 * 删除缓存
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月14日-下午9:19:18
	 * 
	 * @param urlTag
	 *            标识
	 */
	public static void delete(String urlTag) {
		QueryBuilder<Cache> qb = mCacheDao.queryBuilder();
		DeleteQuery<Cache> bd = qb.where(Properties.UrlTag.eq(urlTag))
				.buildDelete();
		bd.executeDeleteWithoutDetachingEntities();
	}

	/**
	 * 
	 * 获取缓存数据
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月15日-上午9:56:54
	 * 
	 * @param urlTag
	 *            标识
	 * @return 缓存的数据
	 */
	public static @Nullable
	Cache getCache(String urlTag) {
		QueryBuilder<Cache> qb = mCacheDao.queryBuilder();
		qb.where(Properties.UrlTag.eq(urlTag));
		if (qb.count() > 0) {
			Cache cache = qb.list().get(0);
			L.i(TAG, "缓存的key：" + urlTag + ",获取到的缓存：" + cache.getJsonData());
			return cache;
		} else {
			return null;
		}
	}

	/**
	 * 
	 * 获取缓存的字符串
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月15日-上午10:09:05
	 * 
	 * @param urlTag
	 *            标识
	 * @return data中的String
	 */
	public static @Nullable
	String getCacheData(String urlTag) {
		Cache cache = getCache(urlTag);
		if (cache != null) {
			String data = cache.getJsonData();
			return JSON.parseObject(data).getString("data");
		}
		return null;
	}

	/**
	 * 
	 * 获取缓存的JsonObject
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月15日-上午10:08:42
	 * 
	 * @param urlTag
	 *            标识
	 * @return JsonObject
	 */
	public static @Nullable
	JSONObject getCacheJsonObject(String urlTag) {
		String data = null;
		Cache cache = getCache(urlTag);
		if (cache != null) {
			data = cache.getJsonData();
			return JSON.parseObject(data).getJSONObject("data");
		} else {
			return null;
		}
	}

	/**
	 * 
	 * 获取缓存JsonArray
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月14日-下午9:19:36
	 * 
	 * @param urlTag
	 *            标识
	 * @return 缓存的JsonArray
	 */
	public static @Nullable
	JSONArray getCacheJsonArray(String urlTag) {
		Cache cache = getCache(urlTag);
		if (cache != null && cache.getJsonData() != null
				&& !cache.getJsonData().equals("")
				&& !cache.getJsonData().equals("null")) {
			String data = cache.getJsonData();
			return JSON.parseObject(data).getJSONArray("data");
		} else {
			return null;
		}
	}

	/**
	 * 
	 * 获取缓存的实体类
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月15日-上午10:08:01
	 * 
	 * @param urlTag
	 *            标识
	 * @param entityClass
	 *            实体类
	 * @return 实体类
	 */
	public static @Nullable
	<Entity> Entity getCacheObject(String urlTag, Class<Entity> entityClass) {
		JSONObject jsonObject = getCacheJsonObject(urlTag);
		if (jsonObject != null) {
			Entity entity = (Entity) JSON.parseObject(
					jsonObject.toJSONString(), entityClass);
			return entity;
		}
		return null;
	}

	/**
	 * 
	 * 获取缓存的实体类集合
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月14日-下午9:44:53
	 * 
	 * @param urlTag
	 *            标识
	 * @param entityClass
	 *            实体类
	 * @return 实体类集合
	 */
	public static @Nullable
	<Entity> ArrayList<Entity> getCacheArray(String urlTag,
			Class<Entity> entityClass) {
		JSONArray jsonArray = getCacheJsonArray(urlTag);
		if (jsonArray != null) {
			int size = jsonArray.size();
			if (size == 0) {
				return null;
			}
			ArrayList<Entity> list = (ArrayList<Entity>) JSON.parseArray(
					jsonArray.toJSONString(), entityClass);
			return list;
		}
		return null;
	}

	/**
	 * 
	 * 删除所有缓存数据
	 * 
	 * skytoup
	 * 
	 * 日期：2014年8月14日-下午9:20:52
	 * 
	 */
	public static void clearAllCache() {
		mCacheDao.deleteAll();
	}

}
