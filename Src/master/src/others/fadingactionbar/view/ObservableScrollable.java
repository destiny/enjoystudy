package others.fadingactionbar.view;


public interface ObservableScrollable {
    void setOnScrollChangedCallback(OnScrollChangedCallback callback);
}
