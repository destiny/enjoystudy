package others.fadingactionbar.view;

public interface OnScrollChangedCallback {
    void onScroll(int l, int t);
}
