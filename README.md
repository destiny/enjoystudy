#最爱学
本项目是针对高校学生的一款以“微课”学习为主的（MOOC）移动端学习平台。本次参赛我们展示的是本项目的Android端，其界面设计和操作体验依照谷歌最新推出的Material Design，具备良好的用户体验，实现多人在线学习、远程学习、师生互动、趣味学习等功能。我们项目的目标是利用本应用实现符合信息化时代的创新教学模式，并能正真投入到真实教学环境中使用。

##一句话
基于Vitamio的视频播放器实践




## 主要库
### [Vitamio](https://github.com/yixia/VitamioBundle)
Vitamio is an open multimedia framework for Android and iOS, with full and real hardware accelerated decoder and renderer.



## 废话不多说，上图
![](image/login_page.png)

![](image/home_page.png)

![](image/task_page.png)

![](image/play_page.png)

![](image/profile_page.png)

![](image/sider_page.png)

![](image/corser_page.png)


